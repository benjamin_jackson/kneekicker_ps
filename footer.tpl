{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<footer id="footer"  class="container">
						<div class="row">
						<section id="block_contact_infos" class="footer-block col-xs-12 col-sm-4">
							<div>
						        <h4>Contact Us</h4>
						        <ul class="toggle-footer" style="">
						            <li>
						            	Call: <span>02380 334555</span>
						            </li>
						            <li>
						            	Email: <span><a href="mailto:enquiries@kneekicker.co.uk">enquiries@kneekicker.co.uk</a></span>
						            </li>
									<li>
										KneeKicker, KneeKicker Sales Counter <br />
										Unit 1 Hollybrook Road <br />
										Southampton <br />
										SO16 6RB
									</li>
						        </ul>
						    </div>
						</section>
						<section id="block_contact_infos" class="footer-block col-xs-12 col-sm-4">
							<div>
						        <h4>Company Information</h4>
						        <ul class="toggle-footer" style="">
						            <li><a href="/content/4-about-us">About Us</a></li>
						            <li><a href="/blog">Blog</a></li>
									<li><a href="/help_inspiration">Inspiration</a></li>
									<li>Delivery Information</li>
						        </ul>
						    </div>
						</section>
						{$HOOK_FOOTER}
							<section id="block_contact_infos" class="footer-block col-xs-12 col-sm-4">
								<div>
									<h4>Secure Online Shopping</h4>
									 <div class="Secure_Payments_Logo">
						                <img src="{$tpl_uri}/img/ico-padlock.svg" style="width: 50px;">
						                <img src="{$tpl_uri}/img/card-types-row.png" />
						            </div>
								</div>
							</section>
						</div>
					</footer>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
	</body>
</html>