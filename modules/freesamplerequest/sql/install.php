<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'quotes_product` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`id_quote` varchar(100) NOT NULL,
	`id_shop` int(11) unsigned NOT NULL DEFAULT "1",
	`id_shop_group` int(11) unsigned NOT NULL DEFAULT "1",
	`id_lang` int(11) unsigned NOT NULL DEFAULT "1",
	`id_product` int(10) unsigned NOT NULL,
	`id_product_attribute` int(10) unsigned NOT NULL,
	`id_currency` int(10) unsigned NOT NULL,
	`id_guest` int(11) unsigned NOT NULL DEFAULT "0",
	`id_customer` int(11) unsigned NOT NULL DEFAULT "0",
	`quantity` int(10) unsigned NOT NULL DEFAULT "0",
	`date_add` datetime NOT NULL,
	`date_upd` datetime NOT NULL ,
	PRIMARY KEY (`id`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'quotes_bargains` (
	`id_bargain` int(11) NOT NULL AUTO_INCREMENT,
	`id_quote` int(11) NOT NULL,
	`bargain_whos` varchar(100) NOT NULL,
	`bargain_text` text NOT NULL,
	`date_add` datetime NOT NULL,
	PRIMARY KEY (`id_bargain`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'quotes` (
	`id_quote` int(11) NOT NULL AUTO_INCREMENT,
	`id_cart` int(11) NOT NULL,
	`reference` varchar(250) NOT NULL,
	`quote_name` varchar(250) NOT NULL,
	`burgain_price` int(20) NOT NULL,
	`id_shop` int(11) NOT NULL,
	`id_shop_group` int(11) NOT NULL,
	`id_lang` int(11) NOT NULL,
	`id_currency` int(11) NOT NULL,
	`id_customer` int(11) NOT NULL,
	`products` text NOT NULL,
	`date_add` datetime NOT NULL,
	`submited` tinyint(1) NOT NULL,
	`id_quote_state` int(11) NOT NULL,
	PRIMARY KEY (`id_quote`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'quote_state` (
	  `id_quote_state` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `send_email` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
	  `color` varchar(32) DEFAULT NULL,
	  `default` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
	  `deleted` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
	  PRIMARY KEY (`id_quote_state`)
	) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'quote_state` (`id_quote_state`, `send_email`, `color`, `default`, `deleted`) VALUES
	(1, 1, \'#4169E1\', 1, 0),
	(2, 1, \'#FF8C00\', 0, 0),
	(3, 1, \'#32CD32\', 0, 0);';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'quote_state_lang` (
	  `id_quote_state` int(10) unsigned NOT NULL,
	  `id_lang` int(10) unsigned NOT NULL,
	  `name` varchar(64) NOT NULL,
	  PRIMARY KEY (`id_quote_state`,`id_lang`)
	) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'quotes_history` (
				  `id_quote_history` int(11) NOT NULL AUTO_INCREMENT,
				  `id_employee` int(11) NOT NULL,
				  `id_quote` int(11) NOT NULL,
				  `id_quote_state` int(11) NOT NULL,
				  `date_add` datetime NOT NULL,
				  PRIMARY KEY (`id_quote_history`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$languages = Language::getLanguages(true, false, true);
foreach ($languages as $language)
{
	$sql[] = 'INSERT INTO `'._DB_PREFIX_.'quote_state_lang` (`id_quote_state`, `id_lang`, `name`) VALUES
		(1, '.(int)$language.', \'Received\'),
		(2, '.(int)$language.', \'Accepted\'),
		(3, '.(int)$language.', \'Sent\');';
}

foreach ($sql as $query)
	if (Db::getInstance()->execute($query) == false)
		return false;
