<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
	exit;

class QuotesObj
{

	public function getQuotesByCustomer($id_customer)
	{
		if (!$id_customer)
			return false;

		$sql = 'SELECT q.*
				FROM `'._DB_PREFIX_.'quotes` q
				LEFT JOIN `'._DB_PREFIX_.'quotes_bargains` b
				ON q.`burgain_price` = b.`id_bargain`
				WHERE q.`id_customer` = '.pSQL($id_customer).' ORDER BY q.`id_quote` DESC';
		return Db::getInstance()->executeS($sql);
		//return $sql;
	}

	public function getQuoteByCustomerAndCart($id_customer, $id_cart)
	{
		if (!$id_customer)
			return false;

		$sql = 'SELECT q.*
				FROM `'._DB_PREFIX_.'quotes` q
				LEFT JOIN `'._DB_PREFIX_.'quotes_bargains` b
				ON q.`burgain_price` = b.`id_bargain`
				WHERE q.`id_customer` = '.(int)$id_customer.' AND q.`id_cart` = '.pSQL($id_cart).' AND q.`submited` < 2';
		return Db::getInstance()->executeS($sql);
		//return $sql;
	}

	public function getQuoteInfo($id_quote = false)
	{
		if (!$id_quote)
			return false;

		$sql = 'SELECT * FROM `'._DB_PREFIX_.'quotes` WHERE `id_quote` = '.pSQL($id_quote);

		return Db::getInstance()->executeS($sql);
	}

	public function getCartByQuoteId($id_quote = false)
	{
		if (!$id_quote)
			return false;

		$sql = 'SELECT `id_cart` FROM `'._DB_PREFIX_.'quotes` WHERE `id_quote` = '.pSQL($id_quote);

		$result = Db::getInstance()->executeS($sql);

		if ($result)
			return $result[0]['id_cart'];

		return true;
	}

	public function renameQuote($id_quote = false, $quote_name)
	{
		if (!$id_quote)
			return false;

		$sql = 'UPDATE `'._DB_PREFIX_.'quotes` SET
					`quote_name` = "'.pSQL($quote_name).'"
						WHERE `id_quote`='.pSQL($id_quote);

		return Db::getInstance()->execute($sql);
	}

	public function getBargains($id_quote = false)
	{
		if (!$id_quote)
			return false;
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'quotes_bargains` WHERE `id_quote`='.pSQL($id_quote).' ORDER BY `id_bargain` DESC';

		return Db::getInstance()->executeS($sql);
	}

	public function getHistories($id_quote = false)
	{
		if (!$id_quote)
			return false;
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'quotes_history` WHERE `id_quote`='.pSQL($id_quote).' ORDER BY `date_add` DESC';

		return Db::getInstance()->executeS($sql);
	}

	public function addQuoteBargain($id_quote = false, $text, $whos = 'customer')
	{
		if (!$id_quote)
			return false;

		$date_add = date('Y-m-d H:i:s', time());
		$sql = 'INSERT INTO `'._DB_PREFIX_.'quotes_bargains` SET
					`id_quote` = '.pSQL($id_quote).',
					`bargain_whos` = "'.pSQL($whos).'",
					`bargain_text` = "'.pSQL($text).'",
					`date_add` = "'.pSQL($date_add).'"';

		$result = Db::getInstance()->execute($sql);

		$id_bargain = Db::getInstance()->Insert_ID();

		$sql = 'UPDATE `'._DB_PREFIX_.'quotes` SET `submited` = 2 WHERE `id_quote`='.pSQL($id_quote);
		Db::getInstance()->execute($sql);

		if ($result)
		{
			$sql = 'UPDATE `'._DB_PREFIX_.'quotes` SET `burgain_price` = '.pSQL($id_bargain).' WHERE `id_quote`='.pSQL($id_quote);
			if (Db::getInstance()->execute($sql))
				return true;
		}

		return $result;
	}

	public function deleteBargain($id_bargain = false)
	{
		if (!$id_bargain)
			return false;
		$sql = 'DELETE FROM `'._DB_PREFIX_.'quotes_bargains` WHERE `id_bargain`='.pSQL($id_bargain);

		return Db::getInstance()->execute($sql);
	}

	public function submitTransformQuote($id_quote)
	{
		if (!$id_quote)
			return false;
		$sql = 'UPDATE `'._DB_PREFIX_.'quotes` SET
				`submited` = '.ASK_FOR_A_QUOTE_SUBMITED_STATUS.'
					WHERE `id_quote`='.pSQL($id_quote);
		if (Db::getInstance()->execute($sql))
			return true;

		return false;
	}

}
