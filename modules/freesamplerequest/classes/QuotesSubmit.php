<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'freesamplerequest/classes/QuotesTools.php');

class QuotesSubmitCore extends ObjectModel
{

	public $id_quote;
	public $id_cart;
	public $reference;
	public $quote_name;
	public $id_shop;
	public $id_shop_group;
	public $id_lang;
	public $id_currency;
	public $id_customer;
	public $products;
	public $burgain_price;
	public $date_add;
	public $submited;

	/**
	 * Hold id of the current quote state
	 *
	 * @var int $id_quote_state
	 */
	public $id_quote_state;

	public static $definition = array(
		'table' => 'quotes',
		'primary' => 'id_quote',
		'fields' => array(
			'id_cart' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
			'quote_name' => array( 'type' => self::TYPE_STRING, 'validate' => 'isAnything' ),
			'reference' => array( 'type' => self::TYPE_STRING, 'validate' => 'isAnything' ),
			'id_shop' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
			'id_shop_group' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
			'id_lang' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
			'id_currency' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
			'id_customer' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
			'products' => array( 'type' => self::TYPE_STRING, 'validate' => 'isAnything' ),
			'burgain_price' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
			'date_add' => array( 'type' => self::TYPE_DATE, 'validate' => 'isDateFormat' ),
			'submited' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
			'id_quote_state' => array( 'type' => self::TYPE_INT, 'validate' => 'isUnsignedId' ),
		),
	);

	public function __construct($id_quote = null, $id_lang = null)
	{
		parent::__construct($id_quote);

		$this->context = Context::getContext();
		if (!is_null($id_lang))
			$this->id_lang = (Language::getLanguage($id_lang) !== false) ? (int)$id_lang : (int)Configuration::get('PS_LANG_DEFAULT');
		$this->id_shop = (int)$this->context->shop->id;
		$this->id_shop_group = (int)$this->context->shop->id_shop_group;

		if ($this->context->customer)
			$this->id_customer = (int)$this->context->customer->id;

		$this->date_add = date('Y-m-d H:i:s', time());
		if (empty($this->reference))
			$this->reference = Tools::strtoupper(Tools::passwdGen(9, 'NO_NUMERIC'));
		$this->submited = 0;
	}

	public function add($autodate = true, $null_values = false)
	{
		$return = parent::add($autodate, $null_values);
		return $return;
	}

	public function update($null_values = false)
	{
		$return = parent::update($null_values);
		return $return;
	}

	public function delete()
	{
		if (!Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'quotes` WHERE `id_quote` = '.pSQL($this->id_quote)))
			return false;

		return parent::delete();
	}

	public function deleteQuoteById($id_quote = false, $id_customer = false)
	{
		if ($id_quote && $id_customer)
		{
			if (Db::getInstance()->execute(
				'DELETE FROM `'._DB_PREFIX_.'quotes`
				WHERE `id_quote` = '.pSQL($id_quote).'
				AND `id_customer` = '.pSQL($id_customer)
			))
			{
				if (Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'quotes_bargains` WHERE `id_quote` = '.pSQL($id_quote)))
					return true;
			}
			return false;
		}
		else
			return false;
	}

	public function getAllQuotes()
	{
		$quotes = array();
		$result = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'quotes`');
		if (empty($result))
			return array();

		foreach ($result as $row)
		{
			$quote = array();
			$quote['id_quote'] = $row['id_quote'];
			$quote['quote_name'] = $row['quote_name'];
			$quote['id_shop'] = $row['id_shop'];
			$quote['id_shop_group'] = $row['id_shop_group'];
			$quote['id_lang'] = $row['id_lang'];
			$customer = new Customer($row['id_customer']);
			$quote['customer'] = array(
				'id' => $customer->id,
				'name' => $customer->firstname.' '.$customer->lastname,
				'link' => 'index.php?tab=AdminCustomers&addcustomer&id_customer='.$customer->id.'&token='.Tools::getAdminTokenLite('AdminCustomers'),
			);
			$quote['products'] = unserialize($row['products']);
			$quote['date_add'] = $row['date_add'];
			$quote['submited'] = $row['submited'];

			// start add new filed id_state
			$quote['id_quote_state'] = $row['id_quote_state'];
			// end add new field id_state

			$quotes[] = $quote;
		}

		return $quotes;
	}

	public function getQuoteById($id_quote, $id_customer)
	{
		$result = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'quotes`
						WHERE `id_quote` = '.pSQL($id_quote).'
						AND `id_customer` = '.pSQL($id_customer));
		if (empty($result))
			return array();

		$customer = new Customer($id_customer);
		$out = array();
		$out['customer'] = array(
			'id' => $customer->id,
			'name' => $customer->firstname.' '.$customer->lastname,
			'gender' => $customer->id_gender,
			'email' => $customer->email,
			'birthday' => $customer->birthday,
			'addresses' => $customer->getAddresses($this->context->language->id),
			'date_add' => $customer->date_add,
			'link' => 'index.php?tab=AdminCustomers&addcustomer&id_customer='.$customer->id.'&token='.Tools::getAdminTokenLite('AdminCustomers'),
		);

		$product_arr = array();
		$products = unserialize($result[0]['products']);
		$price = 0;
		foreach ($products as $item)
		{
			$itemp = new Product($item['id'], true, $this->context->language->id);
			$attr = new Attribute($item['id_attribute'], $this->context->language->id);
			if ($item['id_attribute'] != 0)
			{
				$image_id = getProductAttributeImage($itemp->id, $item['id_attribute'], $this->context->language->id);
				if (!$image_id)
				{
					$image_id = $itemp->getCover($itemp->id);
					$image_id = $image_id['id_image'];
				}
			}
			else
			{
				$image_id = $itemp->getCover($itemp->id);
				$image_id = $image_id['id_image'];
			}

			if (Configuration::get('AFQ_PRICE_GROUP') == 1)
			{
				$id_address = null;

				// start get id_address from cart associated to quote
				if (isset($result[0]['id_cart']) && $result[0]['id_cart'] > 0)
				{
					$cart = new Cart($result[0]['id_cart']);
					if (isset($cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')})
						&& $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')} > 0)
						$id_address = $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
				}
				// end get id_address from cart associated to quote

				// start get first id_address from all customer's addresses
				if (empty($id_address))
				{
					$addresses = $customer->getAddresses($this->context->language->id);
					if (!empty($addresses) && isset($addresses[0]))
						$id_address = $addresses[0]['id_address'];
				}
				// end get first id_address from all customer's addresses

				$specific_price_output = array();

				$unit_price = self::getPriceStatic(
					$itemp->id,
					true,
					$item['id_attribute'],
					6,
					null,
					false,
					true,
					$item['quantity'],
					false,
					$id_customer,
					null,
					$id_address,
					$specific_price_output,
					false,
					true,
					null,
					true
				);
			}
			else
				$unit_price = $itemp->getPriceStatic($itemp->id, true, $item['id_attribute'], 6);

			// get first image type as cart_default
			$image_types = ImageType::getImagesTypes();
			$product_arr[] = array(
				'id' => $itemp->id,
				'attr' => $attr->name,
				'name' => $itemp->name,
				'image' => $this->context->link->getImageLink($itemp->link_rewrite[$this->context->language->id], $image_id, $image_types[0]['name']),
				'link' => $this->context->link->getProductLink($itemp, $itemp->link_rewrite,
						$itemp->category, null, null, $itemp->id_shop, $item['id_attribute']),
				'quantity' => $item['quantity'],
				'unit_price' => Tools::displayPrice(
						Tools::ps_round($unit_price, 2),
						$this->context->currency),
				'total' => Tools::displayPrice(
						Tools::ps_round(($unit_price * (int)$item['quantity']), 2),
						$this->context->currency),
			);
			$price = $price + ((float)$unit_price * (int)$item['quantity']);
		}
		$out['quote_total'] = array(
			'quote_static' => $price,
			'quote_normal' => Tools::displayPrice(Tools::ps_round($price, 2), $this->context->currency)
		);

		$out['products'] = $product_arr;
		$out[] = $result[0];
		return $out;
	}

	/**
	* Get product price
	*
	* @param integer $id_product Product id
	* @param boolean $usetax With taxes or not (optional)
	* @param integer $id_product_attribute Product attribute id (optional).
	* 	If set to false, do not apply the combination price impact. NULL does apply the default combination price impact.
	* @param integer $decimals Number of decimals (optional)
	* @param integer $divisor Useful when paying many time without fees (optional)
	* @param boolean $only_reduc Returns only the reduction amount
	* @param boolean $usereduc Set if the returned amount will include reduction
	* @param integer $quantity Required for quantity discount application (default value: 1)
	* @param boolean $forceAssociatedTax DEPRECATED - NOT USED Force to apply the associated tax. Only works when the parameter $usetax is true
	* @param integer $id_customer Customer ID (for customer group reduction)
	* @param integer $id_cart Cart ID. Required when the cookie is not accessible (e.g., inside a payment module, a cron task...)
	* @param integer $id_address Customer address ID. Required for price (tax included) calculation regarding the guest localization
	* @param variable_reference $specificPriceOutput.
	* 	If a specific price applies regarding the previous parameters, this variable is filled with the corresponding SpecificPrice object
	* @param boolean $with_ecotax insert ecotax in price output.
	* @return float Product price
	*/
	public static function getPriceStatic($id_product, $usetax = true, $id_product_attribute = null, $decimals = 6, $divisor = null,
		$only_reduc = false, $usereduc = true, $quantity = 1, $force_associated_tax = false, $id_customer = null, $id_cart = null,
		$id_address = null, &$specific_price_output = null, $with_ecotax = true, $use_group_reduction = true, Context $context = null,
		$use_customer_price = true)
	{
		unset($force_associated_tax);

		if (!$context)
			$context = Context::getContext();

		$cur_cart = $context->cart;

		if ($divisor !== null)
			Tools::displayParameterAsDeprecated('divisor');

		if (!Validate::isBool($usetax) || !Validate::isUnsignedId($id_product))
			die(Tools::displayError());

		// Initializations
		$id_group = (int)Group::getCurrent()->id;

		// If there is cart in context or if the specified id_cart is different from the context cart id
		if (!is_object($cur_cart) || (Validate::isUnsignedInt($id_cart) && $id_cart && $cur_cart->id != $id_cart))
		{
			/*
			* When a user (e.g., guest, customer, Google...) is on PrestaShop, he has already its cart as the global (see /init.php)
			* When a non-user calls directly this method (e.g., payment module...) is on PrestaShop, he does not have already it BUT knows the cart ID
			* When called from the back office, cart ID can be inexistant
			*/
			if (!$id_cart && !isset($context->employee))
				die(Tools::displayError());
			$cur_cart = new Cart($id_cart);
			// Store cart in context to avoid multiple instantiations in BO
			if (!Validate::isLoadedObject($context->cart))
				$context->cart = $cur_cart;
		}

		$cart_quantity = 0;
		if ((int)$id_cart)
		{
			$cache_id = 'Product::getPriceStatic_'.(int)$id_product.'-'.(int)$id_cart;
			if (!Cache::isStored($cache_id) || ($cart_quantity = Cache::retrieve($cache_id) != (int)$quantity))
			{
				$sql = 'SELECT SUM(`quantity`)
				FROM `'._DB_PREFIX_.'cart_product`
				WHERE `id_product` = '.(int)$id_product.'
				AND `id_cart` = '.(int)$id_cart;
				$cart_quantity = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
				Cache::store($cache_id, $cart_quantity);
			}
			$cart_quantity = Cache::retrieve($cache_id);
		}

		$id_currency = (int)Validate::isLoadedObject($context->currency) ? $context->currency->id : Configuration::get('PS_CURRENCY_DEFAULT');

		// retrieve address informations
		$id_country = (int)$context->country->id;
		$id_state = 0;
		$zipcode = 0;

		if (!$id_address && Validate::isLoadedObject($cur_cart))
			$id_address = $cur_cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};

		if ($id_address)
		{
			$address_infos = Address::getCountryAndState($id_address);
			if ($address_infos['id_country'])
			{
				$id_country = (int)$address_infos['id_country'];
				$id_state = (int)$address_infos['id_state'];
				$zipcode = $address_infos['postcode'];
			}
		}
		else if (isset($context->customer->geoloc_id_country))
		{
			$id_country = (int)$context->customer->geoloc_id_country;
			$id_state = (int)$context->customer->id_state;
			$zipcode = (int)$context->customer->postcode;
		}

		if (Tax::excludeTaxeOption())
			$usetax = false;

		if ($usetax != false
			&& !empty($address_infos['vat_number'])
			&& $address_infos['id_country'] != Configuration::get('VATNUMBER_COUNTRY')
			&& Configuration::get('VATNUMBER_MANAGEMENT'))
			$usetax = false;

		if (is_null($id_customer) && Validate::isLoadedObject($context->customer))
			$id_customer = $context->customer->id;

		if (isset($id_customer) && $id_customer > 0)
		{
			$customer = new Customer($id_customer);
			$id_group = $customer->id_default_group;
		}

		$specific_price_output = array();

		return Product::priceCalculation(
			$context->shop->id,
			$id_product,
			$id_product_attribute,
			$id_country,
			$id_state,
			$zipcode,
			$id_currency,
			$id_group,
			$quantity,
			$usetax,
			$decimals,
			$only_reduc,
			$usereduc,
			$with_ecotax,
			$specific_price_output,
			$use_group_reduction,
			$id_customer,
			$use_customer_price,
			$id_cart,
			$cart_quantity
		);
	}

}
