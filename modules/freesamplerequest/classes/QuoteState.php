<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
	exit;

class QuoteState extends ObjectModel
{
	/** @var string Name */
	public $name;

	/** @var bool Send an e-mail to customer ? */
	public $send_email;

	/** @var string Display state in the specified color */
	public $color;

	/** @var bool Default */
	public $default;

	/** @var bool True if carrier has been deleted (staying in database as deleted) */
	public $deleted = 0;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'quote_state',
		'primary' => 'id_quote_state',
		'multilang' => true,
		'fields' => array(
			'send_email' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'color' => 		array('type' => self::TYPE_STRING, 'validate' => 'isColor'),
			'default' =>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'deleted' =>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),

			/* Lang fields */
			'name' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 64),
		),
	);

	/**
	* Get all available quote statuses
	*
	* @param int $id_lang Language id for status name
	* @return array Quote statuses
	*/
	public static function getQuoteStates($id_lang, $deleted = false)
	{
		$cache_id = 'QuoteState::getQuoteStates_'.(int)$id_lang;
		if (!Cache::isStored($cache_id))
		{
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT *
				FROM `'._DB_PREFIX_.'quote_state` qs
				LEFT JOIN `'._DB_PREFIX_.'quote_state_lang` qsl ON (qs.`id_quote_state` = qsl.`id_quote_state` AND qsl.`id_lang` = '.(int)$id_lang.')
				'.(($deleted) ? ('') : ('WHERE `deleted` = 0')).'
				ORDER BY `name` ASC');
			Cache::store($cache_id, $result);
			return $result;
		}
		return Cache::retrieve($cache_id);
	}

	/**
	 * Get default quote state
	 *
	 * @param int $id_lang
	 * @return QuoteState $state
	 */
	public static function getDefaultQuoteState($id_lang)
	{
		$sql = 'SELECT `id_quote_state`
				FROM `'._DB_PREFIX_.'quote_state`
				WHERE `default` = 1';
		$id_quote_state = (int)Db::getInstance()->getValue($sql);

		$quote_state = new QuoteState($id_quote_state, $id_lang);

		return $quote_state;
	}

	/**
	 * Set default quote state as default
	 *
	 * @param int $id_quote_state
	 * @return bool
	 */
	public static function setAsDefault($id_quote_state)
	{
		$quote_state = new QuoteState($id_quote_state);
		if (Validate::isLoadedObject($quote_state))
		{
			$sql = 'UPDATE `'._DB_PREFIX_.'quote_state`
					SET `default` = 0';
			Db::getInstance()->execute($sql);

			$quote_state->default = 1;

			return $quote_state->save();
		}
		else
			return false;
	}

	/**
	 * Toggle set_email quote state as default
	 *
	 * @param int $id_quote_state
	 * @return bool
	 */
	public static function toggleSendEmail($id_quote_state)
	{
		$quote_state = new QuoteState($id_quote_state);
		if (Validate::isLoadedObject($quote_state))
		{
			if ($quote_state->send_email == 1)
				$quote_state->send_email = 0;
			else
				$quote_state->send_email = 1;

			return $quote_state->save();
		}
		else
			return false;
	}
}