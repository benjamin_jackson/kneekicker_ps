FREE SAMPLE REQUEST
--------------
Module for PrestaShop
--------------


-----
INTRODUCTION
==============

First  of  all,  thank  you  for  purchasing  our  module!

We’ve  done  our  best  to  make  this  module  as  functional  and  as  stable  as  possible,  but  as  each  server , shop  and theme  is  different, and your Prestashop may contain  deeper edits than  we expect, small incompatibilities  or  issues  may  appear .  And do remember,  that  we can test  only on a default Prestashop  enviroment,  and  if  the  case,  please  allow  us  to  try  to  debug!

So,  before  placing  it  on  your  production  website,  it  is  adviced  to  test  it  on  a  preproduction enviroment  to  verify  that  it  works  out  of  the  box,  to  see  if  corresponds  to  all  your  needs  and  that  you understand  its  behaviour .

If  problems,  you  are  adviced  to  get  in  contact  with  us  as  soon  as  possible  (see  section  below),  and we  will  help  and/or  debug.  For  the  fastest  results,  please  provide  us  with  FTP  and  Back  Office  access to  your  shop  (URL, user  and  password),  and  with  the  detailed  explanation  of  your  issue  that  will  help  us 
reproduce  it  (Please  note,  that  both  accounts  should  be  temporary  ones  so  they  can  be  deleted  after  our intervention).  And  do  not  forget  to  include  the  date  and  your  unique  purchase  ID!

**Disclaimer:**

This  module  is  delivered  as  Open-Source.  All  rights  are  the  exclusive  belonging  of  Presta  FABRIQUE. You can modify it  to  adapt it  to  your exact need. You are granted for  a license  of usage on a single web site, so you are not allowed to install it on several sites (if  you need several licenses,  please get in contact with us, and we could give you a discount). Y ou are not allowed to redistribute it, modified or not, 
whatever  the  way  of  redistribution.


CONTACT
==============

You  have  several  ways  that  you  can  use  to  get  in  contact  with  us  and  for  after  sales  support:

1)  if  you  purchased  on  PrestaShop  Addons,  please  use  the  option  *Contact  the  developer* , available  on  the  page  of  the  module,  but  first  please  login  with  the  user  under  you  made  the purchase  from.

2) if  you purchased on our website (Presta-shop-modules.com), head there, login,  go to the Contact  page,  and  fill  up  the  form.Both of the above will normally validate you as a buyer , but if you prefere, you can always send a direct  email  to  *prestafabrique@gmail.com*


PRODUCT PRESENTATION
==============

Free Sample Request module for PrestaShop makes it possible for clients to request free samples of a products. This is a must have for B2B commerce, specially where you are dealing with products where the touch and feel of a material is just as important as it’s look, like:

- cosmetics or health care - smell and softness of a cream
- clothing industry - size and quality of a t-shirt and it’s print
- car industry - leather types and colors for car interior
- furniture - leather texture and coloring for office chairs, couches and other items
- pavement industry - concrete roughness and durability

... and the list can go on and on. You name it!

If this is what you're after, this is the module you need: squeeze out the maximum from your B2B shop, product presentation or catalog mode website with a sample request module!

**Front  office  features**

- future clients can touch and feel the final product
- My Samples option is added to My Account list
- Samples sidebox - aka "Samples Cart”
- animated add to samples cart effect
- quantity can be forced or set to one, regardless of the normal buy form
- price can be turned off on module level
- attributes, colors and reference is also saved
- email is sent to both client and admin with request details
- messaging system  for client and admin
- multiple and dynamic statuses for request - 3 by default
- one page login / register (just as one page checkout) with fast register (OPC)
- category permission (request button is present only in selected categories)
- product type permission (request button is present only if product is not sold online)
- guest checkout option
- terms and conditions can be added and made mandatory

**Back  office  options**

- separate menupoint for submits is automatically added under Orders tab
- admin can add statuses
- admin can change status of a request
- instant submit can be enabled / disabled for logged in clients
- guest checkout can be enabled / disabled
- Terms and Conditions can be made mandatory
- category management
- receiver email address management
- forced quantity settings


INSTALLATION
==============

To  install  the  Free Sample Request  module,  please  follow  these  steps:

1.  Login  to  your  Back  Office  if  you  have  not  already  done  so.
2.  Go  to  the  Modules  tab.
3.  Click  the  text  which  says,  "Add  a  new  module”
4.  Click  the  "Browse..."  button  next  to  the  Module  File  field
5.   Select  the  file  which  you  downloaded  from  Prestashop  Addons  or  our  website.  It  should  be  called "freesamplerequest.zip".
6.  Click  the  "Upload  this  module"  button.
6.  You should see the "Free Sample Request" module in the list of modules. Click the "Install" button next  to  this  module  to  install  it.
8.  Configure  it  to  your  needs.

PS: make sure that *Ask for a Quote* or *Ask for a Quote PRO* modules are not installed, because these three share the same database structure, being one product family, derived from each other! The module will alert you if this is the case and will not install itself, but for safety, you should be aware of this!


*Thank you!*
*the Presta FABRIQUE team*