{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Free Sample Request' mod='freesamplerequest'}{/capture}
<div class="block">
    <h4 class="title_block">
        {l s='Your free sample request list' mod='freesamplerequest'}
    </h4>
</div>
<div id="quotes-cart-wrapper">
{if isset($products) && count($products) > 0}
    <div id="quotes-cart-wrapper-products">
      		{if file_exists("$tpl_path/quote_product_list.tpl")}
				{include file="$tpl_path/quote_product_list.tpl"}
			{else}
				{include file="$tpl_path_local/quote_product_list.tpl"}
			{/if}
    </div>

    {if isset($isLogged) && $isLogged == 1 && count($products) > 0}
        <div class="clearfix row">
            <form action="{$link->getModuleLink('freesamplerequest', 'SampleCart', array(), true)|escape:'html':'UTF-8'}" method="post" class="std">
                <div class="form-group clearfix">
                    <label for="quote_name" class="col-sm-2 control-label">{l s='Request name' mod='freesamplerequest'}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="quote_name" name="quote_name">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="bargain_message" class="col-sm-2 control-label">{l s='Message' mod='freesamplerequest'}</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="bargain_message" id="bargain_message" rows="3"></textarea>
                    </div>
                </div>
            </form>
        </div>
        <div class="clearfix">
            {if isset($terms_page) && $terms_page}
                <div class="alert alert-warning terms_alert">
                    {l s='You must confirm terms and conditions!' mod='freesamplerequest'}
                </div>
                <form action="{$link->getModuleLink('freesamplerequest', 'SampleCart', array(), true)|escape:'html':'UTF-8'}" method="post" class="std col-sm-12">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="terms_conditions" id="terms_conditions"> {l s='Confirm' mod='freesamplerequest'}
                            <a class="terms_link" href="{$link->getCMSLink($terms_page)|escape:'htmlall':'UTF-8'}" target="_blank">{l s='terms and conditions' mod='freesamplerequest'}</a>
                        </label>
                    </div>
                </form>
            {/if}
            <form action="{$link->getModuleLink('freesamplerequest', 'SampleCart', array(), true)|escape:'html':'UTF-8'}" method="post" class="std col-sm-5">
                {if $MESSAGING_ENABLED != 1}
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="contact_via_phone" id="contact_via_phone"> {l s='Contact me via telephone instead of email' mod='freesamplerequest'}
                        </label>
                    </div>
                    <div class="required form-group show_phone_field">
                        <label for="contact_phone">{l s='Enter your phone number' mod='freesamplerequest'}</label>
                        <input type="text" class="text form-control validate" name="contact_phone" id="contact_phone" data-validate="isPhoneNumber" value="{if isset($guestInformations) && isset($guestInformations.phone_mobile) && $guestInformations.phone_mobile}{$guestInformations.phone_mobile|escape:'html':'UTF-8'}{else}{$post.phone_mobile|escape:'html':'UTF-8'}{/if}" />
                    </div>
                {/if}
            </form>
            <a class="btn btn-success submit_quote" href="javascript:void(0);" title="{l s='Submit now' mod='freesamplerequest'}">
                <span data-wait_text="{l s='Please wait...' mod='freesamplerequest'}">
                    {l s='Submit now' mod='freesamplerequest'}
                    <i class="icon-chevron-right right"></i>
                </span>
            </a>
        </div>
    {else}

      	{if file_exists("$tpl_path/quotes_new_account.tpl")}
			{include file="$tpl_path/quotes_new_account.tpl"}
		{else}
			{include file="$tpl_path_local/quotes_new_account.tpl"}
		{/if}
    {/if}
    <div {if isset($userRegistry) && $userRegistry==1}style="display: block;"{/if} id="quote_account_saved" class="alert alert-success">
        {l s='Account information saved successfully' mod='freesamplerequest'}
    </div>
{else}
    {if isset($guest_mail_send)}
        <p class="alert alert-success">{l s='Sample list was sent to your e-mail. Thank you. We will contact you soon' mod='freesamplerequest'}</p>
    {else}
        <p class="alert alert-warning">{l s='No samples' mod='freesamplerequest'}</p>
    {/if}
{/if}
</div>

{strip}
    {addJsDef authenticationUrl=$link->getPageLink("authentication", true)|escape:'quotes':'UTF-8'}
    {addJsDefL name=txtThereis}{l s='There is' mod='freesamplerequest' js=1|escape:'html':'UTF-8'}{/addJsDefL}
    {addJsDefL name=txtErrors}{l s='Error(s)' mod='freesamplerequest' js=1|escape:'html':'UTF-8'}{/addJsDefL}
    {addJsDef quoteCartUrl=$link->getModuleLink('freesamplerequest', 'SampleCart', array(), true)|escape:'html':'UTF-8'}
    {addJsDef guestCheckoutEnabled=$PS_GUEST_QUOTES_ENABLED|intval}
    {addJsDef isGuest=$isGuest|intval}
    {addJsDef addressEnabled=$ADDRESS_ENABLED|intval}
    {addJsDef messagingEnabled=$MESSAGING_ENABLED|intval}
    {addJsDef terms_page=$terms_page|intval}
    {addJsDef guest_open=$guest_open|intval}
{/strip}