{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="order-detail-content" class="table_block table-responsive">
    <table id="quotes_cart_summary" class="table table-bordered ">
        <thead>
        <tr>
            <th class="quotes_cart_product first_item">{l s='Product' mod='freesamplerequest'}</th>
            <th class="quotes_cart_description item">{l s='Description' mod='freesamplerequest'}</th>
            {if $MAIN_PRICE}<th class="quotes_cart_unit item">{l s='Unit price' mod='freesamplerequest'}</th>{/if}
            <th class="quotes_cart_quantity item">{l s='Qty' mod='freesamplerequest'}</th>
            {if $MAIN_PRICE}<th class="quotes_cart_total item">{l s='Total' mod='freesamplerequest'}</th>{/if}
            <th class="quotes_cart_delete last_item"></th>
        </tr>
        </thead>
        <tbody>
        {foreach $products as $key=>$product}
			{if is_numeric($key)}
                <tr id="product_{$product.id|escape:'htmlall':'UTF-8':'intval'}_{$product.id_attribute|escape:'htmlall':'UTF-8':'intval'}">
                    <td class="quotes_cart_product">
                        <a href="{$product.link|escape:'html':'UTF-8'}">
                            <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'cart_default')|escape:'html':'UTF-8'}" alt="{$product.title|escape:'html':'UTF-8'}" />
                        </a>
                    </td>
                    <td class="quotes_cart_description">
                        <p class="product-name"><a href="{$product.link|escape:'html':'UTF-8'}">{$product.title|escape:'html':'UTF-8'}</a></p>
						<small>{l s='SKU' mod='freesamplerequest'}: {$product.reference|escape:'html':'UTF-8'}</small><br />
						<small>{$product.combinations|escape:'html':'UTF-8'}</small>
                    </td>
                    {if $MAIN_PRICE}
                        <td class="quotes_cart_unit">
                            {$product.unit_price|escape:'html':'UTF-8'}
                        </td>
                    {/if}
                    <td class="quotes_cart_quantity">
                    	{if $force_quantity == 0}
	                        <div class="row">
	                            <div class="col-lg-8">
	                                <input size="3" maxlength="6" rel="{$product.id|escape:'htmlall':'UTF-8':'intval'}_{$product.id_attribute|escape:'htmlall':'UTF-8':'intval'}" type="text" onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" autocomplete="off" class="cart_quantity_input form-control grey" value="{$product.quantity|escape:'htmlall':'UTF-8':'intval'}"  name="quantity_{$product.id|escape:'htmlall':'UTF-8':'intval'}_{$product.id_attribute|escape:'htmlall':'UTF-8':'intval'}" />
	                            </div>
	                            <div class="col-lg-2">
	                                <div class="quantity-block">
	                                    <a href="javascript:void(0);" class="quote-plus-button btn btn-default" rel="{$product.id|escape:'htmlall':'UTF-8':'intval'}_{$product.id_attribute|escape:'htmlall':'UTF-8':'intval'}"><i class="icon-chevron-up"></i></a>
	                                    <a href="javascript:void(0);" class="quote-minus-button btn btn-default" rel="{$product.id|escape:'htmlall':'UTF-8':'intval'}_{$product.id_attribute|escape:'htmlall':'UTF-8':'intval'}"><i class="icon-chevron-down"></i></a>
	                                </div>
	                            </div>
	                        </div>
                        {else}
                        	{$product.quantity|escape:'htmlall':'UTF-8':'intval'}
                        {/if}
                    </td>
                    {if $MAIN_PRICE}
                        <td class="quotes_cart_total">
                            {$product.total_price|escape:'html':'UTF-8'}
                        </td>
                    {/if}
                    <td class="quotes_cart_delete">
                        <a href="javascript:void(0);" rel="{$product.id|escape:'htmlall':'UTF-8':'intval'}_{$product.id_attribute|escape:'htmlall':'UTF-8':'intval'}" class="remove_quote"><i class="icon-remove"></i></a>
                    </td>
                </tr>
            {/if}
        {/foreach}
        {if $MAIN_PRICE}
            <tr class="quote_row_total">
                <td colspan="4" align="right"><h5>{l s='Cart total:' mod='freesamplerequest'}</h5></td>
                <td colspan="2" class="total_cart">{$cart.total|escape:'html':'UTF-8'}</td>
            </tr>
        {/if}
        </tbody>
    </table>
</div>