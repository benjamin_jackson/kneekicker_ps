{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Your requests' mod='freesamplerequest'}{/capture}
<h1 class="page-heading bottom-indent">{l s='Request hitory' mod='freesamplerequest'}</h1>
<p class="info-title">{l s='Here are the free sample requests you\'ve submited since your account was created.' mod='freesamplerequest'}</p>

<div class="block-center" id="block-quotes">
    {if $quotes && count($quotes)}
		<button aria-controls="legendPanel" aria-expanded="false" data-target="#legendPanel" data-toggle="collapse" type="button" class="btn btn-primary collapsed">{l s='Open request legend' mod='freesamplerequest'}</button>
		<div id="legendPanel" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><i class="icon-remove color-red btn"></i> {l s='No action taken' mod='freesamplerequest'}</li>
                <li class="list-group-item"><i class="icon-comments btn"></i> {l s='Under discussion' mod='freesamplerequest'}</li>
                <li class="list-group-item"><i class="icon-pencil btn"></i> {l s='Click to edit request name' mod='freesamplerequest'}</li>
            </ul>
        </div>

        <table id="quotes-list" class="table table-bordered footab">
            <thead>
            <tr>
                <th class="first_item">{l s='Reference' mod='freesamplerequest'}</th>
                <th class="item">{l s='Request name' mod='freesamplerequest'}</th>
                <th class="item">{l s='Date' mod='freesamplerequest'}</th>
                {if $MAIN_PRICE}
                    <th class="item">{l s='Total price' mod='freesamplerequest'}</th>
                {/if}
                <th class="item">{l s='Status' mod='freesamplerequest'}</th>
                <th class="item text-center">{l s='State' mod='freesamplerequest'}</th>
                <th class="last_item">{l s='Details' mod='freesamplerequest'}</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$quotes item=quote}
                <tr class="item quote_{$quote.id_quote|escape:'htmlall':'UTF-8':'intval'}">
                    <td>{$quote.reference|escape:'html':'UTF-8'}</td>
                    <td data-value="{$quote.id_quote|escape:'html':'UTF-8'}" class="quote_name"><i class="icon-pencil"></i>{$quote.quote_name|escape:'html':'UTF-8'}</td>
                    <td data-value="{$quote.date_add|regex_replace:"/[\-\:\ ]/":""|escape:'htmlall':'UTF-8'}" class="">
                        {dateFormat date=$quote.date_add full=0}
                    </td>
                    {if $MAIN_PRICE}
                        <td>
                            <span class="price">
                                {$quote.price|escape:'html':'UTF-8'}
                            </span>
                        </td>
                    {/if}
                    <td>
                        <span class="label" style="background-color:{$quote.state.color|escape:'html':'UTF-8'};color:{if Tools::getBrightness($quote.state.color) < 128}white{else}#383838{/if}">
                        	{$quote.state.name|escape:'html':'UTF-8'}
                        </span>
                    </td>
                    <td class="text-center">
						{if $quote.submited == 1}
							<i class="icon-ok-circle color-green"></i>
						{elseif $quote.submited == 0}
							<i class="icon-remove color-red"></i>
						{elseif $quote.submited == 2}
							<i class="icon-comments"></i>
						{else}
							<i class="icon-mail-forward color-green2"></i>
						{/if}
					</td>
                    <td class="table_link">
                        <a class="show_quote_details" data-id="{$quote.id_quote|escape:'html':'UTF-8'}" href="{$link->getModuleLink('freesamplerequest', 'SubmitedSampleCarts', array(), true)|escape:'htmlall':'UTF-8'}"><i class="icon-eye-open"></i> {l s='view' mod='freesamplerequest'}</a>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <div id="block-order-detail" class="unvisible">&nbsp;</div>
    {else}
        <p class="alert alert-warning">{l s='You have not requested any samples yet.' mod='freesamplerequest'}</p>
    {/if}
</div>

<ul class="footer_links clearfix">
    <li>
        <a class="btn btn-default button button-small" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        <span>
            <i class="icon-chevron-left"></i> {l s='Back to Your Account' mod='freesamplerequest'}
        </span>
        </a>
    </li>
    <li>
        <a class="btn btn-default button button-small" href="{$base_dir|escape:'html':'UTF-8'}">
            <span><i class="icon-chevron-left"></i> {l s='Home' mod='freesamplerequest'}</span>
        </a>
    </li>
</ul>

{strip}
    {addJsDef submitedQuotes=$link->getModuleLink('freesamplerequest', 'SubmitedSampleCarts', array(), true)}
    {addJsDefL name=your_msg}{l s='Your message:' mod='freesamplerequest' js=1|escape:'html':'UTF-8'}{/addJsDefL}
    {addJsDefL name=added}{l s='Added:' mod='freesamplerequest' js=1|escape:'html':'UTF-8'}{/addJsDefL}
{/strip}