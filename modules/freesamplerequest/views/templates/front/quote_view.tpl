{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<h1 class="page-heading bottom-indent">{l s='Request information' mod='freesamplerequest'}</h1>

<p><a href="{$link->getModuleLink('freesamplerequest', 'SubmitedSampleCarts', array(), true)|escape:'html':'UTF-8'}" id="show_quote_products_info">&raquo; {l s='Click to hide/show products info' mod='freesamplerequest'}</a></p>

<table id="quote_products_info" class="table table-bordered">
    <tr>
        <th class="quotes_cart_product first_item">{l s='Product' mod='freesamplerequest'}</th>
        <th class="quotes_cart_description item">{l s='Name' mod='freesamplerequest'}</th>
        {if $MAIN_PRICE}
            <th class="quotes_cart_unit item">{l s='Unit price' mod='freesamplerequest'}</th>
        {/if}
        <th class="quotes_cart_quantity item">{l s='Qty' mod='freesamplerequest'}</th>
        {if $MAIN_PRICE}
            <th class="quotes_cart_total item">{l s='Total' mod='freesamplerequest'}</th>
        {/if}
    </tr>
    {foreach from=$quote.products item=product}
        <tr id="product_{$product.id|intval}_{$product.id_attribute|intval}">
            <td class="quotes_cart_product">
                <a href="{$product.link|escape:'html':'UTF-8'}">
                    <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'cart_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:'html':'UTF-8'}" />
                </a>
            </td>
            <td class="quotes_cart_description">
                <p class="product-name"><a href="{$product.link|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></p>
				<small>{l s='SKU' mod='freesamplerequest'}: {$product.reference|escape:'html':'UTF-8'}</small><br />
				<small>{$product.combinations|escape:'html':'UTF-8'}</small>
            </td>
            {if $MAIN_PRICE}
                <td class="quotes_cart_unit">
                    {$product.price|escape:'html':'UTF-8'}
                </td>
            {/if}
            <td class="quotes_cart_quantity">
                {$product.quantity|intval}
            </td>
            {if $MAIN_PRICE}
                <td class="quotes_cart_total">
                    {$product.price_total|escape:'html':'UTF-8'}
                </td>
            {/if}

        </tr>
    {/foreach}
</table>

{if $MESSAGING_ENABLED}
    <form action="{$link->getModuleLink('freesamplerequest', 'SubmitedSampleCarts', array(), true)|escape:'html':'UTF-8'}" method="post" id="client_bargain_txt" class="std">
        <input type="hidden" id="id_quote" name="id_quote" value="{$id_quote|escape:'html':'UTF-8'}" />
        <input type="hidden" name="action" value="addClientBargain" />
        <fieldset>
            <div class="box">
                <div id="success_bargain_message" class="alert alert-success">
                    {l s='Your message was sent' mod='freesamplerequest'}
                </div>
                <div id="errors_bargain_message" class="alert alert-danger"></div>
                <h3 class="page-subheading">{l s='New message' mod='freesamplerequest'}</h3>
                <div class="form-group is_customer_param">
                    <textarea class="form-control" name="bargain_text" id="bargain_text" cols="26" rows="3"></textarea>
                </div>
                <button type="submit" name="addClientBargain" id="addClientBargain" class="btn btn-default button button-medium"><span>{l s='Send' mod='freesamplerequest'}<i class="icon-chevron-right right"></i></span></button>
            </div>
        </fieldset>
    </form>
{/if}

<h1 class="page-heading bottom-indent">{l s='Activity' mod='freesamplerequest'}</h1>


<ul class="bargains_list">
    {if $detail_messages && count($detail_messages) > 0}
        {foreach from=$detail_messages item=detail_message}
        	{if isset($detail_message.id_bargain)}
	            {if $detail_message.bargain_whos == 'customer'}
	                <li class="customer_bargain clearfix">
	                    <div class="row">
	                        <div class="bargain_heading clearfix">
	                            <div class="date col-xs-9">
	                                <p class="bargain_whos">{l s='Your message:' mod='freesamplerequest'}</p>
	                            </div>
	                            <div class="date col-xs-3">
	                                <strong>{l s='Added:' mod='freesamplerequest'}</strong> {$detail_message.date_add|escape:'html':'UTF-8'}
	                            </div>
	                        </div>
	                        <div class="bargain_message col-xs-12 box">{$detail_message.bargain_text|escape:'html':'UTF-8'}</div>
	                    </div>
	                </li>
	            {else}
	                <li class="admin_bargain clearfix">
	                    <div class="row">
	                        <div class="bargain_heading clearfix">
	                            <div class="date col-xs-9">
	                                <p class="bargain_whos">{l s='Admin message:' mod='freesamplerequest'}</p>
	                            </div>
	                            <div class="date col-xs-3">
	                                <strong>{l s='Added:' mod='freesamplerequest'}</strong> {$detail_message.date_add|escape:'html':'UTF-8'}
	                            </div>
	                        </div>
	                        {if $detail_message.bargain_text}
	                            <div class="bargain_message col-xs-12 box">{$detail_message.bargain_text|escape:'html':'UTF-8'}</div>
	                        {/if}
	                    </div>
	                </li>
	            {/if}
            {/if}
            {if isset($detail_message.id_quote_history)}
            	<li class="admin_bargain clearfix">
	                <div class="row">
	                    <div class="date col-xs-9">
	                        {l s='Admin changed the status of the request to:' mod='freesamplerequest'}
	                        <span class="label" style="background-color:{$detail_message.state->color|escape:'html':'UTF-8'};color:{if Tools::getBrightness($detail_message.state->color) < 128}white{else}#383838{/if}">
	                        {$detail_message.state->name|escape:'html':'UTF-8'}
	                        </span>
	                    </div>
	                    <div class="date col-xs-3 pull-right">
	                        {$detail_message.date_add|escape:'html':'UTF-8'}
	                    </div>
	                </div>
	            </li>
            {/if}
        {/foreach}
    {else}
        <p class="alert alert-warning bargains_list_warning">{l s='There are is no activity history for this request' mod='freesamplerequest'}</p>
    {/if}
</ul>