{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script type="text/javascript">
    var confirmDelete   = "{l s='Are you sure you want delete?' mod='freesamplerequest'}";
    var adminQuotesUrl = "{$index|escape:'quotes':'UTF-8'}";
</script>
<div class="row panel">
    <h3><i class="icon-hand-right"></i> {l s='Request:' mod='freesamplerequest'} #{$quote[0]['id_quote']|intval}</h3>
    <br/>
    {if isset($smarty.get.qconf) && $smarty.get.qconf == 1}
    	<div class="alert alert-success">
	        {l s='Message sent' mod='freesamplerequest'}
	    </div>
    {else if $quote[0]['submited'] == 3}
    	<div class="alert alert-success">
        	{l s='Transformed to order' mod='freesamplerequest'}
    	</div>
    {/if}
    <div class="col-lg-12 panel admin-panel">
        <h3><i class="icon-user"></i> {l s='Requisites' mod='freesamplerequest'}</h3>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <table class="table">
                            <tr>
                                <td>{l s='Customer Name:' mod='freesamplerequest'}</td>
                                <td><strong>{$quote.customer.name|escape:'html':'UTF-8'}</strong></td>
                            </tr>
                            <tr>
                                <td>{l s='Gender:' mod='freesamplerequest'}</td>
                                <td><strong>{if $quote.customer.gender == 1}<i class="icon-male"></i>{elseif $quote.customer.gender == 2}<i class="icon-female"></i>{else}{l s='Not selected' mod='freesamplerequest'}{/if}</strong></td>
                            </tr>
                            <tr>
                                <td>{l s='Email:' mod='freesamplerequest'}</td>
                                <td><strong><a href="mailto:{$quote.customer.email|escape:'html':'UTF-8'}">{$quote.customer.email|escape:'html':'UTF-8'}</a></strong></td>
                            </tr>
                            <tr>
                                <td>{l s='Birthday:' mod='freesamplerequest'}</td>
                                <td><strong>{if $quote.customer.birthday == '0000-00-00'}{l s='Not specified' mod='freesamplerequest'}{else}{$quote.customer.birthday|escape:'html':'UTF-8'}{/if}</strong></td>
                            </tr>
                            <tr>
                                <td>{l s='Registration date:' mod='freesamplerequest'}</td>
                                <td><strong>{$quote.customer.date_add|escape:'html':'UTF-8'}</strong></td>
                            </tr>
                        </table>
                        <br/>
                        <div class="text-right">
                            <a target="_blank" href="{$link->getAdminLink("AdminCustomers", true)|escape:'html':'UTF-8'}&id_customer={$quote.customer.id|intval}&updatecustomer" class="btn btn-default"><i class="icon-edit"></i> {l s='Edit' mod='freesamplerequest'}</a>
                        </div>
                        
                        <!-- start change status form -->
                        <br/>
						<div class="tab-pane active" id="status">
							<h4 class="visible-print">{l s='Status' mod='freesamplerequest'}</h4>							
							<form action="{$index|escape:'html':'UTF-8'}" method="post" class="form-horizontal well hidden-print">
								<input type="hidden" name="id_quote" value="{$id_quote|intval}"/>
            					<input type="hidden" name="id_customer" value="{$id_customer|intval}">
								<div class="row">
									<div class="col-lg-9">
										<select id="id_quote_state" class="chosen form-control" name="id_quote_state">
										{foreach from=$states item=state}
											<option value="{$state['id_quote_state']|intval}"{if $state['id_quote_state'] == $quote[0]['id_quote_state']} selected="selected" disabled="disabled"{/if}>{$state['name']|escape}</option>
										{/foreach}
										</select>
										<input type="hidden" name="id_quote" value="{$quote[0]['id_quote']|intval}" />
									</div>
									<div class="col-lg-3">
										<button type="submit" name="submitState" class="btn btn-primary">
											{l s='Update status' mod='freesamplerequest'}
										</button>
									</div>
								</div>
							</form>
						</div>
                        <!-- end change status form -->
                        
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-7">
                        {if count($quote.customer.addresses) > 0}
                            {foreach $quote.customer.addresses as $address}
                                <div class="panel panel-default">
                                    <div class="panel-heading">{$address.alias|escape:'html':'UTF-8'}</div>
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr>
                                                <td>{l s='Company' mod='freesamplerequest'}</td>
                                                <td><strong>{if !empty($address.company)}{$address.company|escape:'html':'UTF-8'}{else}{l s='Not specified' mod='freesamplerequest'}{/if}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>{l s='First name, Last name' mod='freesamplerequest'}</td>
                                                <td><strong>{$address.firstname|escape:'htmlall':'UTF-8'} {$address.lastname|escape:'html':'UTF-8'}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>{l s='Region' mod='freesamplerequest'}</td>
                                                <td><strong>{$address.country|escape:'htmlall':'UTF-8'}, {$address.state|escape:'html':'UTF-8'}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>{l s='Address' mod='freesamplerequest'}</td>
                                                <td><strong>{$address.address1|escape:'htmlall':'UTF-8'} {$address.address2|escape:'html':'UTF-8'} {$address.city|escape:'html':'UTF-8'}, {$address.postcode|escape:'html':'UTF-8'} </strong></td>
                                            </tr>
                                            <tr>
                                                <td>{l s='Phone' mod='freesamplerequest'}</td>
                                                <td><strong>{if !empty($address.phone)}{$address.phone|escape:'html':'UTF-8'}{else}{l s='Not specified' mod='freesamplerequest'}{/if}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>{l s='Phone mobile' mod='freesamplerequest'}</td>
                                                <td><strong>{if !empty($address.phone_mobile)}{$address.phone_mobile|escape:'html':'UTF-8'}{else}{l s='Not specified' mod='freesamplerequest'}{/if}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>{l s='Creation date' mod='freesamplerequest'}</td>
                                                <td><strong>{$address.date_add|escape:'html':'UTF-8'}</strong></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            {/foreach}
                        {else}
                            <div class="alert alert-warning">
                                {$quote.customer.name|escape:'html':'UTF-8'} {l s='has not registered any addresses yet' mod='freesamplerequest'}
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 panel">
        <h3><i class="icon-list-ul"></i> {l s='Products list' mod='freesamplerequest'}</h3>
        <table class="table">
            <thead>
            <tr>
                <td>{l s='ID' mod='freesamplerequest'}</td>
                <td>{l s='Name' mod='freesamplerequest'}</td>
                <td>{l s='Unit price' mod='freesamplerequest'}</td>
                <td>{l s='Quantity' mod='freesamplerequest'}</td>
                <td>{l s='Total' mod='freesamplerequest'}</td>
            </tr>
            </thead>
            {foreach $quote.products as $product}
                <tr>
                    <td>{$product.id|intval}</td>
                    <td>
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{$product.link|escape:'html':'UTF-8'}" target="_blank">
                                    <div class="col-lg-2">
                                        <img src="{$product.image|escape:'html':'UTF-8'}" class="img-responsive" width="50" height="50" alt="{$product.name|escape:'html':'UTF-8'}" />
                                    </div>
                                    <div>{$product.name|escape:'html':'UTF-8'}</div>
                                    <small>{$product.attr|escape:'html':'UTF-8'}</small>
                                </a>
                            </div>
                        </div>
                    </td>
                    <td>{$product.unit_price|escape:'html':'UTF-8'}</td>
                    <td>{$product.quantity|intval}</td>
                    <td>{$product.total|escape:'html':'UTF-8'}</td>
                </tr>
            {/foreach}
            <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><h4>{l s='Total products:' mod='freesamplerequest'}</h4></td>
                <td><h4>{$quote.quote_total.quote_normal|escape:'html':'UTF-8'}<h4></td>
            </tr>
            </tfoot>
        </table>
    </div>

    <div class="col-lg-12 panel">
        <h3><i class="icon-user"></i> {l s='Send message to customer' mod='freesamplerequest'}</h3>
		<form id="bargain_form" class="defaultForm form-horizontal AdminCustomers" action="{$index|escape:'html':'UTF-8'}" method="post">
            <input type="hidden" name="id_quote" value="{$id_quote|intval}"/>
            <input type="hidden" name="id_customer" value="{$id_customer|intval}">
                <div class="form-wrapper">
                    <div class="form-group">
                        <label class="control-label col-lg-3 required">{l s='Your comment' mod='freesamplerequest'}</label>
                        <div class="col-lg-6">
                            <textarea name="bargain_text" class="textarea-autosize" style=""></textarea>
                        </div>
						<div class="form-group col-lg-2">
							<button type="submit" id="addClientBargain" name="addClientBargain" class="btn btn-default pull-right">
								<i class="process-icon-save"></i> {l s='Send' mod='freesamplerequest'}
							</button>
						</div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.form-wrapper -->
        </form>

    </div>

    <div class="col-lg-12 panel">
        <h3><i class="icon-list-ul"></i> {l s='Message history' mod='freesamplerequest'}</h3>
        {if $bargains && count($bargains) > 0}
            <ul class="bargains_list">
                {foreach from=$bargains item=bargain}
                    {if $bargain.bargain_whos == 'customer'}
                        <li class="customer_bargain clearfix">
                            <div class="row">
                                <div class="bargain_heading clearfix">
                                    <div class="date col-xs-9">
                                        <p class="bargain_whos">{$quote.customer.name|escape:'html':'UTF-8'} {l s='bargain:' mod='freesamplerequest'}</p>
                                    </div>
                                    <div class="date col-xs-3">
                                        <strong>{l s='Added:' mod='freesamplerequest'}</strong> {$bargain.date_add|escape:'html':'UTF-8'}
                                    </div>
                                </div>
                                <div class="bargain_message col-xs-12 box">{$bargain.bargain_text|escape:'html':'UTF-8'}</div>
                            </div>
                        </li>
                    {else}
                        <li class="admin_bargain clearfix">
                            <div class="row">
                                <div class="bargain_heading clearfix">
                                    <div class="date col-xs-9">
                                        <p class="bargain_whos">{l s='Administrator bargain message:' mod='freesamplerequest'}</p>
                                    </div>
                                    <div class="date col-xs-3">
                                        <strong>{l s='Added:' mod='freesamplerequest'}</strong> {$bargain.date_add|escape:'html':'UTF-8'}
                                    </div>
                                </div>
                                <div class="bargain_message col-xs-12 box">{$bargain.bargain_text|escape:'html':'UTF-8'}</div>
                                
                                <div class="col-lg-1 bargain_action">
                                    <form  action="{$index|escape:'html':'UTF-8'}" method="post" class="burgainSubmitForm std">
                                        <a data-action="deleteBargain" data-id="{$bargain.id_bargain|intval}" class="btn btn-default deleteBargainOffer">
                                            <i class="icon-trash"></i><span> {l s='Delete' mod='freesamplerequest'}</span>
                                        </a>
                                    </form>
                                </div>
                            </div>
                        </li>
                    {/if}
                {/foreach}
            </ul>
        {else}
            <p class="alert alert-warning">{l s='There are no messages yet' mod='freesamplerequest'}</p>
        {/if}
    </div>


    <div class="panel-footer">
        <button onclick="javascript:history.go(-1);" class="btn btn-default pull-right"><i
                    class="icon-chevron-left"></i> {l s='Back' mod='freesamplerequest'}</button>
    </div>
