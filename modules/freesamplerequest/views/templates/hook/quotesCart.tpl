{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Quotes cart -->
<script type="text/javascript">
    var quotesCartTop = true;
</script>

{if $AFQ_HIDE_TOP_CART == 0}
	{if $active_overlay == 0}
	    <div class="clearfix col-sm-3 quotesOuterBox block">
	        <div class="quotes_cart">
	            <a href="{$quotesCart|escape:'html':'UTF-8'}" rel="nofollow" id="quotes-cart-link">
	                <b>{l s='Samples' mod='freesamplerequest'}</b>
	                <span class="ajax_quote_quantity{if $quoteTotalProducts == 0} unvisible{/if}">{$quoteTotalProducts|intval}</span>
	                <span class="ajax_quote_product_txt{if $quoteTotalProducts != 1} unvisible{/if}">{l s='Product' mod='freesamplerequest'}</span>
	                <span class="ajax_quote_product_txt_s{if $quoteTotalProducts < 2} unvisible{/if}">{l s='Products' mod='freesamplerequest'}</span>
	                <span class="ajax_quote_no_product{if $quoteTotalProducts > 0} unvisible{/if}">{l s='(empty)' mod='freesamplerequest'}</span>
	            </a>
	            <div class="col-sm-12 quotes_cart_block exclusive" id="box-body" style="display:none;">
	                <div class="block_content">
	                    <div class="row product-list" id="product-list">
	                    	{include file="$item_tpl_dir./product-cart-item.tpl"}
	                    </div>
	                    <p class="cart-buttons">
	                            {if $logged && $enable_submit && !$terms_page}
	                                <a id="button_order_cart" class="btn btn-default button button-small submit_quote" href="javascript:void(0);" title="{l s='Submit request' mod='freesamplerequest'}" rel="nofollow">
	                                <span data-wait_text="{l s='Please wait...' mod='freesamplerequest'}">
	                                    {l s='Submit now' mod='freesamplerequest'}<i class="icon-chevron-right right"></i>
	                                </span>
	                                </a>
	                            {/if}
	                        <a id="button_order_cart" class="btn btn-default button button-small" href="{$quotesCart|escape:'html':'UTF-8'}" title="{l s='Submit request' mod='freesamplerequest'}" rel="nofollow">
	                            <span>
	                                {l s='View list' mod='freesamplerequest'}<i class="icon-chevron-right right"></i>
	                            </span>
	                        </a>
	
	                    </p>
	                </div>
	            </div>
	        </div>
	    </div>
	
	
	{elseif $active_overlay == 1}
		<div id="quotes_layer_cart">
			<div class="clearfix">
	            <h2><i class="icon-ok-circle"></i> {l s='Product successfully added to your sample list' mod='freesamplerequest'}</h2>
				<div class="quotes_layer_cart_product col-xs-12 col-md-6">
					<span class="cross" title="{l s='Close window' mod='freesamplerequest'}"></span>
	                {if $popup_product}
	                    <div class="product-image-container layer_cart_img">
	                        <img class="layer_cart_img img-responsive" src="{$link->getImageLink($popup_product.link_rewrite, $popup_product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$popup_product.name|escape:'html':'UTF-8'}" title="{$popup_product.name|escape:'html':'UTF-8'}">
	                    </div>
	                    <div class="layer_cart_product_info">
	                        <span class="product-name">{$popup_product.name|escape:'html':'UTF-8'}</span>
	                        <div>
	                            <strong class="dark">{l s='Quantity' mod='freesamplerequest'}</strong>
	                            <span id="layer_cart_product_quantity">{$popup_product.quantity|intval}</span>
	                        </div>
							{if $MAIN_PRICE}
	                        <div>
	                            <strong class="dark">{l s='Total' mod='freesamplerequest'}:</strong>
	                            <span id="layer_cart_product_price">{$popup_product.price_total|escape:'html':'UTF-8'}</span>
	                        </div>
							{/if}
	                    </div>
	                {/if}
				</div>
				<div class="quotes_layer_cart_cart col-xs-12 col-md-6">
					<br/>
					<hr/>
					<div class="button-container">
						<span class="continue btn btn-default button exclusive-medium" title="{l s='Continue shopping' mod='freesamplerequest'}">
							<span>
								<i class="icon-chevron-left left"></i>{l s='Continue browsing' mod='freesamplerequest'}
							</span>
						</span>
						{if $enablePopSubmit && $logged && !$terms_page}
							<a id="button_order_cart" class="btn btn-default button button-medium submit_quote" href="javascript:void(0);" title="{l s='Submit quote' mod='freesamplerequest'}" rel="nofollow">
								<span data-wait_text="{l s='Please wait...' mod='freesamplerequest'}">
									{l s='Submit now' mod='freesamplerequest'}<i class="icon-chevron-right right"></i>
								</span>
							</a>
						{else}
							<a class="btn btn-default button button-medium"	href="{$link->getModuleLink('freesamplerequest','SampleCart', array(), true)|escape:'html':'UTF-8'}" title="{l s='Proceed to checkout' mod='freesamplerequest'}" rel="nofollow">
								<span>
									{l s='Proceed to checkout' mod='freesamplerequest'}<i class="icon-chevron-right right"></i>
								</span>
							</a>
						{/if}
					</div>
					<hr/>
				</div>
			</div>
			<div class="crossseling"></div>
		</div> <!-- #layer_cart -->
		<div class="quotes_layer_cart_overlay"></div>
	
	{/if}
{/if}
