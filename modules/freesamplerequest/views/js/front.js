/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

function setHiddenShow(elem_id) {
	document.getElementById(elem_id).value = 1;
}

if (contentOnly == 'undefined')
	var contentOnly = false;
if (catalogMode == 'undefined')
	var catalogMode = false;
if (quotesCart == 'undefined')
	var quotesCart = false;

function stripslashes(str) {
	str=str.replace(/\\'/g,'\'');
	str=str.replace(/\\"/g,'"');
	str=str.replace(/\\0/g,'\0');
	str=str.replace(/\\\\/g,'\\');
	return str;
}

var cartActions= {
	reloadContent : function(response) {
		$('#product-list').empty();
		$('#product-list').html(response.products);
		// insert cart header
		$('#quotes-cart-link').empty();
		$('#quotes-cart-link').html(response.header);
	},
	reloadContentPopup : function(response) {
		$('#columns').append(response.popup);
		$('#quotes_layer_cart').css('display', 'block');
		var $scroll = $(window).scrollTop() - 200;
		$scroll = $scroll + 'px';
		$('#quotes_layer_cart').css('top', $scroll);

		$('.quotes_layer_cart_overlay').css('display', 'block');
		$('.quotes_layer_cart_overlay').css('width', '100%');
		$('.quotes_layer_cart_overlay').css('height', '100%');
	}
}

$(document).ready(function(){
	// Quote details and bargains
	$('.show_quote_details').on('click', function() {

		var $id_quote = $(this).data('id');

		$.ajax({
			url: submitedQuotes,
			method:'post',
			data:
			{
				action : 'showQuoteDetails',
				id_quote : $id_quote
			},
			dataType:'json',
			success: function(data) {
				$('.ajax_item').fadeOut(500);
				$('.ajax_item').remove();

				$('#quotes-list .quote_' + $id_quote).after('<tr class="item ajax_item" style="display: none"><td colspan="7">' + data.details + '</td></tr>');

				$('#quotes-list .ajax_item').fadeIn(500);
			}
		});
		return false;
	});

	//$('#submit-added').fancybox();
	// Change quote name
	$( ".quote_name" ).on( "click", function() {
		var $thisQuoteName = $(this);
		var $oldName = $thisQuoteName.text();
		if($oldName == ''){
			var $myInput = $($thisQuoteName).find('.changed_name');
		}else {
			$($thisQuoteName).html('<input class="changed_name" type="text" value="' + $oldName + '">');
			var $myInput = $($thisQuoteName).find('.changed_name');
		}
		$myInput.focus();
		$myInput.on( "blur", function() {
			var $id_quote = $thisQuoteName.data('value');
			var $quoteName = $myInput.val();
			$.ajax({
				url: submitedQuotes,
				method:'post',
				data:
				{
					quoteRename: 'rename',
					id_quote : $id_quote,
					quoteName : $quoteName
				},
				dataType:'json',
				success: function(data) {
					if(data.hasError){
						alert(data.message);
						$thisQuoteName.html('<i class="icon-pencil"></i>' + $oldName);
					}
					if(data.renamed){
						$thisQuoteName.html('<i class="icon-pencil"></i>' + data.renamed);
					}
				}
			});
		});
	});

	// Show quote products
	$('body').on('click', '#show_quote_products_info', function (e) {
		e.preventDefault();
		$('#quote_products_info').toggle('slow');
	});

	// Add client bargain message
	$('body').on('click', '#addClientBargain', function() {
		$.ajax({
			url: submitedQuotes,
			method:'post',
			data: $('#client_bargain_txt').serialize(),
			dataType:'json',
			success: function(data) {
				if(data.errors) {
					$.each(data.errors, function (key, value) {
						$('#errors_bargain_message').append('<p>' + key + ": " + value + '</p>');
					});
					$('#errors_bargain_message').fadeIn(500);
				}
				else {
					$('#errors_bargain_message').css('display','none');

					var $out_msg = '<li class="customer_bargain clearfix"><div class="row"><div class="bargain_heading clearfix"><div class="date col-xs-9"><p class="bargain_whos">';
					$out_msg += your_msg;
					$out_msg += '</p></div><div class="date col-xs-3"><strong>';
					$out_msg += added;
					$out_msg += ' </strong>';
					$out_msg += getDateTime();
					$out_msg +='</div></div><div class="bargain_message col-xs-12 box">';
					$out_msg += $('#bargain_text').val();
					$out_msg += '</div></div></li>';
					$('.bargains_list .bargains_list_warning').fadeOut(500);
					$('.bargains_list').prepend($out_msg);

					$('#bargain_text').val('');

					$('#success_bargain_message').fadeIn(500);
					setTimeout(function() {
						$('#success_bargain_message').fadeOut(500);
					}, 2000);
				}
			}
		});
		return false;
	});

	//input item quantity cart change
	$('body').on('change', '.cart_quantity_input', function() {
		if(quotesCart == false || quotesCart == 'undefined' || !quotesCart) {
			alert('Quotes cart is undefined. Please hook it on this page to submit the quote and to do other actions');
			return false;
		}
		var input = $(this);
		$.ajax({
			url: quotesCart,
			method:'post',
			data: 'action=recount&method=set&item_id='+input.attr('rel')+'&value='+input.val(),
			dataType:'json',
			success: function(response) {
				if(response.hasError == false) {
					$('#quotes-cart-wrapper-products').empty();
					$('#quotes-cart-wrapper-products').html(response.data);

					// insert cart header
					$('#quotes-cart-link').empty();
					$('#quotes-cart-link').html(response.header);

					$('#product-list').empty();
					$('#product-list').html(response.products);
				}
				else
					alert(response.data.message);
			}
		});
	});
	// minus item quote cart
	$('body').on('click', '.quote-plus-button', function() {
		if(quotesCart == false || quotesCart == 'undefined' || !quotesCart) {
			alert('Quotes cart is undefined. Please hook it on this page to submit the quote and to do other actions');
			return false;
		}
		var current = $(this).closest('.quotes_cart_quantity').find('.cart_quantity_input');
		if($('#order-detail-content').find('.overlay').length == 0)
			$('#order-detail-content').append('<div class="overlay-wrapper"><div class="overlay"></div></div>');
		var button = $(this);
		$.ajax({
			url: quotesCart,
			method:'post',
			data: 'action=recount&method=add&item_id='+button.attr('rel')+'&value='+current.val()+'&button=1',
			dataType:'json',
			success: function(response) {
				if(response.hasError == false) {
					$('#quotes-cart-wrapper-products').empty();
					$('#quotes-cart-wrapper-products').html(response.data);

					// insert cart header
					$('#quotes-cart-link').empty();
					$('#quotes-cart-link').html(response.header);

					$('#product-list').empty();
					$('#product-list').html(response.products);
				}
				else
					alert(response.data.message);
			}
		});
	});
	// plus item quote cart
	$('body').on('click', '.quote-minus-button', function() {
		if(quotesCart == false || quotesCart == 'undefined' || !quotesCart) {
			alert('Quotes cart is undefined. Please hook it on this page to submit the quote and to do other actions');
			return false;
		}
		var current = $(this).closest('.quotes_cart_quantity').find('.cart_quantity_input');
		if(current.val() != 1) {
			if($('#order-detail-content').find('.overlay').length == 0)
				$('#order-detail-content').append('<div class="overlay-wrapper"><div class="overlay"></div></div>');
			var button = $(this);
			$.ajax({
				url: quotesCart,
				method:'post',
				data: 'action=recount&method=remove&item_id='+button.attr('rel')+'&value='+current.val()+'&button=1',
				dataType:'json',
				success: function(response) {
					if(response.hasError == false) {
						$('#quotes-cart-wrapper-products').empty();
						$('#quotes-cart-wrapper-products').html(response.data);

						// insert cart header
						$('#quotes-cart-link').empty();
						$('#quotes-cart-link').html(response.header);

						$('#product-list').empty();
						$('#product-list').html(response.products);
					}
					else
						alert(response.data.message);
				}
			});
		}
	});

	$('#contact_via_phone').click(function() {
		if ($(this).is(':checked'))
			$('.show_phone_field').show();
		else
			$('.show_phone_field').hide();
	});

	$('body').on('click', '.submit_quote', function() {
		if(quotesCart == false || quotesCart == 'undefined' || !quotesCart) {
			alert('Quotes cart is undefined. Please hook it on this page to submit the quote and to do other actions');
			return false;
		}
		if(terms_page) {
			if (!($('#terms_conditions').is(':checked'))){
				$('.terms_alert').show();
				return false;
			}
		}

		var $contact_via = 0;
		if(messagingEnabled == 1){
			if ($('#contact_via_mail').is(':checked')) {
				$contact_via = 'mail';
			}
		} else {
			if ($('#contact_via_phone').is(':checked')) {
				$contact_via = 'phone&contact_phone=' + $('#contact_phone').val();
			}
		}

		var $quote_name = $('#quote_name').val();
		if (!$quote_name)
			$quote_name = 0;
		var $bargain_message = $('#bargain_message').val();
		if (!$bargain_message)
			$bargain_message = 0;
		
		$(this).find('span').text($(this).find('span').attr('data-wait_text'));
		$(this).find('span').addClass('blink');
		
		$.ajax({
			url: quotesCart,
			method:'post',
			data: 'action=submit&contact_via='+$contact_via+'&quote_name='+$quote_name+'&bargain_message='+$bargain_message,
			dataType:'json',
			success: function(response) {
				if(response.hasError == false) {
					window.location = response.redirectUrl;
				}else {
					alert(quotesCartEmpty);
				}
			}
		});
		return false;
	});
	$('body').on('click', '.remove_quote', function(){
		if(quotesCart == false || quotesCart == 'undefined' || !quotesCart) {
			alert('Quotes cart is undefined. Please hook it on this page to submit the quote and to do other actions');
			return false;
		}
		var elem = $(this);
		$.ajax({
			url: quotesCart,
			method: 'post',
			data: 'action=delete_from_cart&item_id='+ $(this).attr('rel'),
			dataType: 'json',
			success: function(response) {
				if(response.hasError == false) {
					$('#quotes-cart-wrapper-products').empty();
					$('#quotes-cart-wrapper-products').html(response.data);

					// insert cart header
					$('#quotes-cart-link').empty();
					$('#quotes-cart-link').html(response.header);

					$('#product-list').empty();
					$('#product-list').html(response.products);
				}
				else
					alert(response.data.message);
			}
		});
	});

	$('body').on('click','.fly_to_quote_cart_button', function(){

		if(quotesCart == false || quotesCart == 'undefined' || !quotesCart) {
			alert('Quotes cart is undefined. Please hook it on this page to submit the quote and to do other actions');
			return false;
		}

		var this_element = $(this);
		var product_list = 0;
		if (this_element.closest('form.quote_ask_form').find('.product_list_opt').val() == 1)
			product_list = 1;

		if (product_list != 1)
			this_element.closest('form.quote_ask_form').find('.ipa').val($('#idCombination').val());

		var filteredQuantity = $('#filtered_quantity').val();

		if(catalogMode == false) {
			if(filteredQuantity != 1)
				if (product_list != 1)
					this_element.closest('form.quote_ask_form').find('.pqty').val(parseInt($('#quantity_wanted').val()));
		}

		if (!contentOnly && $('#quotes-cart-link').length > 0) {
			var score_x = $('#quotes-cart-link').offset().left;
			var score_y = $('#quotes-cart-link').offset().top;

			if (product_list != 1)
			{
				var image = $('#bigpic').attr('src');
				if (!image)
					var image = $('#image-block img').attr('src');
			}
			else
				var image = this_element.closest('.product-container').find('.product_img_link img').attr('src');

			// fly to cart animation
			var top =  this_element.offset().top - 150;
			var left = this_element.offset().left;
			var class_name = 'basket_add_indicator_' + new Date().getTime();
		}

		$.ajax({
			url: quotesCart,
			method:'post',
			data: this_element.closest('form.quote_ask_form').serialize(),
			dataType:'json',
			success: function(response) {
				if (afqHideTopCart == 1 || typeof quotesCartTop == 'undefined') {
					location.href = quotesCart;
					return false;
				}
				
				if (contentOnly) {
					window.parent.cartActions.reloadContent(response);
					parent.$.fancybox.close();
				}
				else {
					$("body").append('<img src="' + image + '" style="width: 150px;height:150px;position:absolute;z-index: 99999;opacity:0;left:' + left + 'px;top:' + top + 'px" class="' + class_name + '" alt="" />');

					$('.' + class_name).animate({"opacity": "1"}, 600, function () {
						$('.' + class_name).animate({
							'left': score_x,
							'top': score_y,
							'width': '20px',
							'height': '20px',
							'opacity': '0.2'
						}, 800, function () {
							$(this).remove();
							/*if(!$('#box-body').hasClass('expanded'))
							 $('#box-body').addClass('expanded');*/
							// insert cart content
							cartActions.reloadContent(response);
						});
					});
				}
			}
		});
		return false;
	});

	//close popup events
	$(document).on('click', '#quotes_layer_cart .cross, #quotes_layer_cart .continue, .quotes_layer_cart_overlay', function(e){
		e.preventDefault();
		$('.quotes_layer_cart_overlay').hide(function(){
			$('.quotes_layer_cart_overlay').remove();
		});

		$('#quotes_layer_cart').fadeOut('fast', function(){
			$(this).remove();
		});
	});
	$('#columns #quotes_layer_cart, #columns .quotes_layer_cart_overlay').detach().prependTo('#columns');
	//add product to quotes with popup
	$('body').on('click','.ajax_add_to_quote_cart_button', function(){
		if(quotesCart == false || quotesCart == 'undefined' || !quotesCart) {
			alert('Quotes cart is undefined. Please hook it on this page to submit the quote and to do other actions');
			return false;
		}
		var this_element = $(this);
		var product_list = 0;
		if (this_element.closest('form.quote_ask_form').find('.product_list_opt').val() == 1)
			product_list = 1;

		if (product_list != 1)
			this_element.closest('form.quote_ask_form').find('.ipa').val($('#idCombination').val());

		var filteredQuantity = $('#filtered_quantity').val();

		// if(catalogMode == false) {
		// 	if(filteredQuantity != 1)
		// 		if (product_list != 1)
		// 			this_element.closest('form.quote_ask_form').find('.pqty').val(parseInt($('#quantity_wanted').val()));
		// }

		$.ajax({
			url: quotesCart,
			method:'post',
			data: this_element.closest('form.quote_ask_form').serialize(),
			dataType:'json',
			success: function(response) {
				
				if (afqHideTopCart == 1 || typeof quotesCartTop == 'undefined') {
					location.href = quotesCart;
					return false;
				}

				$.ajax({
					url: quotesCart,
					method:'post',
					data: this_element.closest('form.quote_ask_form').serialize()+'&showpop&action=popup',
					dataType:'json',
					success: function(response) {
						//console.log(response);
						if(!contentOnly) {
							cartActions.reloadContentPopup(response);
						}else {
							parent.$.fancybox.close();
							window.parent.cartActions.reloadContentPopup(response);
						}
					}
				});
				if(contentOnly)
					window.parent.cartActions.reloadContent(response);
				else
					cartActions.reloadContent(response);
			}
		});
		return false;
	});

	$('body').on('click', '.remove-quote', function() {
		if(quotesCart == false || quotesCart == 'undefined' || !quotesCart) {
			alert('Quotes cart is undefined. Please hook it on this page to submit the quote and to do other actions');
			return false;
		}
		var item = $(this).attr('rel');
		var item_a = $(this);
		$.ajax({
			url: quotesCart,
			method:'post',
			data: 'action=delete&item_id='+item,
			dataType:'json',
			success: function(response) {
				item_a.closest('dt').fadeOut('slow', function(){
					item_a.closest('dt').remove();
				});
				$('#product-list').empty();
				$('#product-list').html(response.products);

				$('#quotes-cart-link').empty();
				$('#quotes-cart-link').html(response.header);

				$('#quotes-cart-wrapper-products').empty();
				$('#quotes-cart-wrapper-products').html(response.data);
			}
		});
	});

	// quotes cart actions
	var cart_block = new showCart('#header .quotesOuterBox .quotes_cart_block');
	var cart_parent_block = new showCart('#header .quotesOuterBox .quotes_cart');

	$("#header .quotesOuterBox .quotes_cart a:first").hover(
		function(){
			$("#header .quotesOuterBox .quotes_cart_block").stop(true, true).slideDown(450);
		},
		function(){
			setTimeout(function(){
				if (!cart_parent_block.isHoveringOver() && !cart_block.isHoveringOver()) {
					$("#header .quotesOuterBox .quotes_cart_block").stop(true, true).slideUp(450);
					if($('#box-body').hasClass('expanded'))
						$('#box-body').removeClass('expanded');
				}

			}, 200);
		}
	);

	$("#header .quotesOuterBox .quotes_cart .quotes_cart_block").hover(
		function(){
		},
		function(){
			setTimeout(function(){
				if (!cart_parent_block.isHoveringOver()) {
					$("#header .quotesOuterBox .quotes_cart_block").stop(true, true).slideUp(450);
					if($('#box-body').hasClass('expanded'))
						$('#box-body').removeClass('expanded');
				}
			}, 200);
		}
	);
});
function showCart(selector)
{
	this.hovering = false;
	var self = this;

	this.isHoveringOver = function(){
		return self.hovering;
	}

	$(selector).hover(function(){
		self.hovering = true;
	}, function(){
		self.hovering = false;
	})
}

function getDateTime() {
	var now     = new Date();
	var year    = now.getFullYear();
	var month   = now.getMonth()+1;
	var day     = now.getDate();
	var hour    = now.getHours();
	var minute  = now.getMinutes();
	var second  = now.getSeconds();
	if(month.toString().length == 1) {
		var month = '0'+month;
	}
	if(day.toString().length == 1) {
		var day = '0'+day;
	}
	if(hour.toString().length == 1) {
		var hour = '0'+hour;
	}
	if(minute.toString().length == 1) {
		var minute = '0'+minute;
	}
	if(second.toString().length == 1) {
		var second = '0'+second;
	}
	var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;
	return dateTime;
}