/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

AfcContent = {
	init: function () {		
		if ($('#MAIN_PRODUCT_PAGE_off').is(':checked')) {
			AfcContent.hideProductPageOptions();
		}
		if ($('#MAIN_PRODUCT_LIST_off').is(':checked')) {
			AfcContent.hideProductListOptions();
		}
		if ($('#AFQ_MAX_QUANTITY_off').is(':checked')) {
			AfcContent.hideMaxQuantityOptions();
		}
		if ($('#AFQ_FORCE_QUANTITY_off').is(':checked')) {
			AfcContent.hideForceQuantityOptions();
		}
		
		this.initListeners();
	},
	
	initListeners: function () {
		$('#MAIN_PRODUCT_PAGE_on').change(function () {
			if ($('#MAIN_PRODUCT_PAGE_on').is(':checked')) {
				AfcContent.showProductPageOptions();
			}
		});
		$('#MAIN_PRODUCT_PAGE_off').change(function () {
			if ($('#MAIN_PRODUCT_PAGE_off').is(':checked')) {
				AfcContent.hideProductPageOptions();
			}
		});
		
		$('#MAIN_PRODUCT_LIST_on').change(function () {
			if ($('#MAIN_PRODUCT_LIST_on').is(':checked')) {
				AfcContent.showProductListOptions();
			}
		});
		$('#MAIN_PRODUCT_LIST_off').change(function () {
			if ($('#MAIN_PRODUCT_LIST_off').is(':checked')) {
				AfcContent.hideProductListOptions();
			}
		});
		
		$('#AFQ_MAX_QUANTITY_on').change(function () {
			if ($('#AFQ_MAX_QUANTITY_on').is(':checked')) {
				AfcContent.showMaxQuantityOptions();
			}
		});
		$('#AFQ_MAX_QUANTITY_off').change(function () {
			if ($('#AFQ_MAX_QUANTITY_off').is(':checked')) {
				AfcContent.hideMaxQuantityOptions();
			}
		});
		
		$('#AFQ_FORCE_QUANTITY_on').change(function () {
			if ($('#AFQ_FORCE_QUANTITY_on').is(':checked')) {
				AfcContent.showForceQuantityOptions();
			}
		});
		$('#AFQ_FORCE_QUANTITY_off').change(function () {
			if ($('#AFQ_FORCE_QUANTITY_off').is(':checked')) {
				AfcContent.hideForceQuantityOptions();
			}
		});
	},
	
	showProductPageOptions: function () {
		$('#HOOK_PRODUCT_LEFT_on').closest('.form-group').slideDown('slow');
		$('#HOOK_PRODUCT_RIGHT_on').closest('.form-group').slideDown('slow');
		$('#HOOK_PRODUCT_BUTTON_on').closest('.form-group').slideDown('slow');
		$('#MAIN_QUANTITY_FIELDS_on').closest('.form-group').slideDown('slow');
	},
	
	hideProductPageOptions: function () {
		$('#HOOK_PRODUCT_LEFT_on').closest('.form-group').slideUp('slow');
		$('#HOOK_PRODUCT_RIGHT_on').closest('.form-group').slideUp('slow');
		$('#HOOK_PRODUCT_BUTTON_on').closest('.form-group').slideUp('slow');
		$('#MAIN_QUANTITY_FIELDS_on').closest('.form-group').slideUp('slow');
	},
	
	showProductListOptions: function () {
		$('#MAIN_QUANTITY_LIST_on').closest('.form-group').slideDown('slow');
	},
	
	hideProductListOptions: function () {
		$('#MAIN_QUANTITY_LIST_on').closest('.form-group').slideUp('slow');
	},
	
	showMaxQuantityOptions: function () {
		$('#AFQ_MAX_QUANTITY_VALUE').closest('.form-group').slideDown('slow');
	},
	
	hideMaxQuantityOptions: function () {
		$('#AFQ_MAX_QUANTITY_VALUE').closest('.form-group').slideUp('slow');
	},
	
	showForceQuantityOptions: function () {
		$('#AFQ_FORCE_QUANTITY_VALUE').closest('.form-group').slideDown('slow');
	},
	
	hideForceQuantityOptions: function () {
		$('#AFQ_FORCE_QUANTITY_VALUE').closest('.form-group').slideUp('slow');
	}
}

$(document).ready(function () {
	AfcContent.init();
});