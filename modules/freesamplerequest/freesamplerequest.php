<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
	exit;

define('ASK_FOR_A_QUOTE_SUBMITED_STATUS', 3);

class Freesamplerequest extends Module
{

	protected $config_form = false;

	public function __construct()
	{
		$this->name = 'freesamplerequest';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Predict Marketing';
		$this->module_key = 'af78b0f48dac495fcd848349d1286557';
		$this->need_instance = 1;
		$this->controllers = array( 'SampleCart', 'SubmitedSampleCarts' );
		/**
		 * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
		*/
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Free Sample Request Module');
		$this->description = $this->l('Clients can request free samples of your products');
	}

	/**
	 * Don't forget to create update methods if needed:
	 *
	 */
	public function install()
	{
		$askforaquote = Module::getInstanceByName('askforaquote');
		if ($askforaquote instanceof Module)
		{
			if ($askforaquote->active)
			{
				$this->_errors[] = $this->l('Please uninstall "Ask For a Quote" before continue this installation.');
				return false;
			}
		}
		$askforaquotepro = Module::getInstanceByName('askforaquotepro');
		if ($askforaquotepro instanceof Module)
		{
			if ($askforaquotepro->active)
			{
				$this->_errors[] = $this->l('Please uninstall "Ask For a Quote PRO" before continue this installation.');
				return false;
			}
		}

		include (dirname(__file__).'/sql/install.php');

		/* create new tab on backoffice Customers - Quotes */
		$tab = new Tab();
		foreach (Language::getLanguages() as $language)
			$tab->name[$language['id_lang']] = 'Free Sample Request';

		$tab->id_parent = 10;
		$tab->class_name = 'AdminFreesamplerequests'; // Current admin quotes controller
		$tab->module = $this->name; // module name and folder
		$tab->position = Tab::getNewLastPosition($tab->id_parent);

		/* parent tab id */
		$tab->save(); // saving your tab
		//Select all categories to be checked
		$categories = Category::getCategories($this->context->language->id, true, false);
		$categories_ids = array();
		foreach ($categories as $categorie)
			$categories_ids[] = $categorie['id_category'];
		$categories_ids = implode(',', $categories_ids);

		// in future for mail
		// languages
//		$mail_dir = dirname( __FILE__).'/mails/oo';
//		mkdir($mail_dir, 0777, true);

		Configuration::updateValue('MODULE_TAB_ID', $tab->id); // saving tab ID to remove it when uninstall
		Configuration::updateValue('MAIN_STATE', '1'); // Main module status
		Configuration::updateValue('MAIN_QUANTITY_FIELDS', '0'); // Quantity fields product
		Configuration::updateValue('MAIN_QUANTITY_LIST', '0'); // Quantity fields product
		Configuration::updateValue('MAIN_PRICE', '0');
		Configuration::updateValue('MAIN_ANIMATE', '1'); // Quantity fields trigger
		Configuration::updateValue('MAIN_TERMS_AND_COND', '0'); // Quantity fields trigger
		Configuration::updateValue('MAIN_CMS_PAGE', '0'); // Quantity fields trigger
		Configuration::updateValue('PS_GUEST_QUOTES_ENABLED', '0');
		Configuration::updateValue('ADDRESS_ENABLED', '0');
		Configuration::updateValue('MESSAGING_ENABLED', '1');
		Configuration::updateValue('MAIN_PRODUCT_STATUS', '0');
		Configuration::updateValue('MAIN_POP_SUBMIT', '1');
		Configuration::updateValue('MAIN_PRODUCT_PAGE', '1');
		Configuration::updateValue('MAIN_PRODUCT_LIST', '1');
		Configuration::updateValue('MAIN_MAILS', Configuration::get('PS_SHOP_EMAIL'));
		Configuration::updateValue('CATEGORY_BOX', $categories_ids);
		Configuration::updateValue('MAIN_HACK_THEME', '1');
		Configuration::updateValue('HOOK_PRODUCT_LEFT', '0');
		Configuration::updateValue('HOOK_PRODUCT_RIGHT', '0');
		Configuration::updateValue('HOOK_PRODUCT_BUTTON', '1');
		Configuration::updateValue('CATEGORY_RECURSIVE', '0');
		Configuration::updateValue('LIMIT_QUOTED_PRODUCTS_STATUS', '0');
		Configuration::updateValue('LIMIT_QUOTED_PRODUCTS_QUANTITY', '3');
		Configuration::updateValue('AFQ_PRICE_GROUP', '0'); // Take price groups into consideration
		Configuration::updateValue('AFQ_MAX_QUANTITY', '0'); // Max amount bool what a client can ask from a product
		Configuration::updateValue('AFQ_MAX_QUANTITY_VALUE', '0'); // Max amount value what a client can ask from a product
		Configuration::updateValue('AFQ_FORCE_QUANTITY', '0'); // Force quantity bool
		Configuration::updateValue('AFQ_FORCE_QUANTITY_VALUE', '0'); // Force quantity value
		Configuration::updateValue('AFQ_HIDE_TOP_CART', '0'); // Hide top quote cart

		return parent::install() && $this->registerHook('header') &&
			$this->registerHook('extraRight') &&
			$this->registerHook('extraLeft') &&
			$this->registerHook('myAccountBlock') &&
			$this->registerHook('CustomerAccount') &&
			$this->registerHook('top') &&
			$this->registerHook('Header') &&
			$this->registerHook('displayProductListFunctionalButtons') &&
			$this->registerHook('displayMyAccountBlockfooter') &&
			$this->registerHook('displayProductButtons') &&
			$this->registerHook('displayBackOfficeHeader') &&
			$this->registerHook('ActionOrderStatusUpdate');
	}

	public function uninstall()
	{
		$this->deleteTables();

		$tab = new Tab(Configuration::get('MODULE_TAB_ID'));
		$tab->delete();

		return parent::uninstall() &&
			Configuration::deleteByName('MAIN_STATE') &&
			Configuration::deleteByName('MODULE_TAB_ID') &&
			Configuration::deleteByName('MAIN_QUANTITY_FIELDS') &&
			Configuration::deleteByName('MAIN_QUANTITY_LIST') &&
			Configuration::deleteByName('MAIN_PRICE') &&
			Configuration::deleteByName('MAIN_ANIMATE') &&
			Configuration::deleteByName('MAIN_TERMS_AND_COND') &&
			Configuration::deleteByName('MAIN_CMS_PAGE') &&
			Configuration::deleteByName('PS_GUEST_QUOTES_ENABLED') &&
			Configuration::deleteByName('ADDRESS_ENABLED') &&
			Configuration::deleteByName('MESSAGING_ENABLED') &&
			Configuration::deleteByName('MAIN_PRODUCT_STATUS') &&
			Configuration::deleteByName('MAIN_PRODUCT_PAGE') &&
			Configuration::deleteByName('MAIN_PRODUCT_LIST') &&
			Configuration::deleteByName('MAIN_POP_SUBMIT') &&
			Configuration::deleteByName('MAIN_MAILS') &&
			Configuration::deleteByName('MESSAGING_ENABLED') &&
			Configuration::deleteByName('CATEGORY_BOX') &&
			Configuration::deleteByName('MAIN_HACK_THEME') &&
			Configuration::deleteByName('HOOK_PRODUCT_LEFT') &&
			Configuration::deleteByName('HOOK_PRODUCT_RIGHT') &&
			Configuration::deleteByName('HOOK_PRODUCT_BUTTON') &&
			Configuration::deleteByName('CATEGORY_RECURSIVE') &&
			Configuration::deleteByName('LIMIT_QUOTED_PRODUCTS_STATUS') &&
			Configuration::deleteByName('LIMIT_QUOTED_PRODUCTS_QUANTITY') &&
			Configuration::deleteByName('AFQ_PRICE_GROUP') &&
			Configuration::deleteByName('AFQ_MAX_QUANTITY') &&
			Configuration::deleteByName('AFQ_MAX_QUANTITY_VALUE') &&
			Configuration::deleteByName('AFQ_FORCE_QUANTITY') &&
			Configuration::deleteByName('AFQ_FORCE_QUANTITY_VALUE') &&
			Configuration::deleteByName('AFQ_HIDE_TOP_CART');
	}

	private function deleteTables()
	{
		return Db::getInstance()->execute('DROP TABLE IF EXISTS
			`'._DB_PREFIX_.'quote_state`,
			`'._DB_PREFIX_.'quote_state_lang`,
			`'._DB_PREFIX_.'quotes_product`,
			`'._DB_PREFIX_.'quotes_bargains`,
			`'._DB_PREFIX_.'quotes_history`,
			`'._DB_PREFIX_.'quotes`');
	}

	/**
	 * Load the configuration form
	 */
	public function getContent()
	{
		include_once (_PS_MODULE_DIR_.'freesamplerequest/classes/QuoteState.php');

		$output = '<div class="askforquotes_configuration">';
		/**
		 * If values have been submitted in the form, process.
		 */
		if (Tools::getValue('submitMainSettings'))
		{
			Configuration::updateValue('MAIN_STATE', Tools::getValue('MAIN_STATE'));
			Configuration::updateValue('MAIN_QUANTITY_FIELDS', Tools::getValue('MAIN_QUANTITY_FIELDS'));
			Configuration::updateValue('MAIN_QUANTITY_LIST', Tools::getValue('MAIN_QUANTITY_LIST'));
			Configuration::updateValue('MAIN_PRICE', Tools::getValue('MAIN_PRICE'));
			Configuration::updateValue('MAIN_ANIMATE', Tools::getValue('MAIN_ANIMATE'));
			Configuration::updateValue('MAIN_TERMS_AND_COND', Tools::getValue('MAIN_TERMS_AND_COND'));
			Configuration::updateValue('MAIN_CMS_PAGE', Tools::getValue('MAIN_CMS_PAGE'));
			Configuration::updateValue('PS_GUEST_QUOTES_ENABLED', Tools::getValue('PS_GUEST_QUOTES_ENABLED'));
			Configuration::updateValue('ADDRESS_ENABLED', Tools::getValue('ADDRESS_ENABLED'));
			Configuration::updateValue('MESSAGING_ENABLED', Tools::getValue('MESSAGING_ENABLED'));
			Configuration::updateValue('MAIN_PRODUCT_STATUS', Tools::getValue('MAIN_PRODUCT_STATUS'));
			Configuration::updateValue('MAIN_PRODUCT_PAGE', Tools::getValue('MAIN_PRODUCT_PAGE'));
			Configuration::updateValue('MAIN_PRODUCT_LIST', Tools::getValue('MAIN_PRODUCT_LIST'));
			Configuration::updateValue('MAIN_POP_SUBMIT', Tools::getValue('MAIN_POP_SUBMIT'));
			Configuration::updateValue('CATEGORY_BOX', '1,'.implode(',', Tools::getValue('CATEGORY_BOX')));
			Configuration::updateValue('MAIN_HACK_THEME', Tools::getValue('MAIN_HACK_THEME'));
			Configuration::updateValue('HOOK_PRODUCT_LEFT', Tools::getValue('HOOK_PRODUCT_LEFT'));
			Configuration::updateValue('HOOK_PRODUCT_RIGHT', Tools::getValue('HOOK_PRODUCT_RIGHT'));
			Configuration::updateValue('HOOK_PRODUCT_BUTTON', Tools::getValue('HOOK_PRODUCT_BUTTON'));
			Configuration::updateValue('CATEGORY_RECURSIVE', Tools::getValue('CATEGORY_RECURSIVE'));
			Configuration::updateValue('LIMIT_QUOTED_PRODUCTS_STATUS', Tools::getValue('LIMIT_QUOTED_PRODUCTS_STATUS'));
			Configuration::updateValue('LIMIT_QUOTED_PRODUCTS_QUANTITY', Tools::getValue('LIMIT_QUOTED_PRODUCTS_QUANTITY'));
			Configuration::updateValue('AFQ_PRICE_GROUP', Tools::getValue('AFQ_PRICE_GROUP'));
			Configuration::updateValue('AFQ_MAX_QUANTITY', Tools::getValue('AFQ_MAX_QUANTITY'));
			Configuration::updateValue('AFQ_MAX_QUANTITY_VALUE', Tools::getValue('AFQ_MAX_QUANTITY_VALUE'));
			Configuration::updateValue('AFQ_FORCE_QUANTITY', Tools::getValue('AFQ_FORCE_QUANTITY'));
			Configuration::updateValue('AFQ_FORCE_QUANTITY_VALUE', Tools::getValue('AFQ_FORCE_QUANTITY_VALUE'));
			Configuration::updateValue('AFQ_HIDE_TOP_CART', Tools::getValue('AFQ_HIDE_TOP_CART'));

			$mails = explode(',', Tools::getValue('MAIN_MAILS'));
			$mail_errors = 0;

			foreach ($mails as $key => $mail)
			{
				if (!Validate::isEmail(trim($mail)))
					$mail_errors = 1;
				else
					$mails[$key] = trim($mail);
			}
			if (!$mail_errors)
			{
				$mails = implode(', ', $mails);
				Configuration::updateValue('MAIN_MAILS', $mails);
			}
			else
				$output .= $this->displayError($this->l('Wrong email format. Please try again'));

			$output .= $this->displayConfirmation($this->l('Settings updated'));
		}
		if (Tools::isSubmit('defaultfreesamplerequeststatuses'))
		{
			if ($this->updateDefaultState(Tools::getValue('id_quote_state')))
				$output .= $this->displayConfirmation($this->l('Settings updated'));
			else
				$output .= $this->displayError($this->l('Update default failed. Please try again'));
		}
		if (Tools::isSubmit('sendEmailfreesamplerequeststatuses'))
		{
			if ($this->updateSendEmailState(Tools::getValue('id_quote_state')))
				$output .= $this->displayConfirmation($this->l('Settings updated'));
			else
				$output .= $this->displayError($this->l('Update send email failed. Please try again'));
		}
		if (Tools::isSubmit('submitAddQuoteStateSettings'))
		{
			if ($this->addQuoteState())
				$output .= $this->displayConfirmation($this->l('Request state added'));
			else
			{
				$output .= $this->displayError($this->l('An error occurred while creating an object.'));
				$addaskforaquotepro_error = true;
			}
		}
		if (Tools::isSubmit('deletefreesamplerequeststatuses'))
		{
			if ($this->deleteQuoteState(Tools::getValue('id_quote_state')))
				$output .= $this->displayConfirmation($this->l('Request state deleted'));
			else
				$output .= $this->displayError($this->l('An error occurred while deleting an object.'));
		}
		if (Tools::isSubmit('submitUpdateQuoteStateSettings'))
		{
			if ($this->updateQuoteState())
				$output .= $this->displayConfirmation($this->l('Request state updated'));
			else
			{
				$output .= $this->displayError($this->l('An error occurred while updating an object.'));
				$updateaskforaquotepros_error = true;
			}
		}

		$this->context->smarty->assign('module_dir', $this->_path);

		if (Tools::isSubmit('addfreesamplerequest') || isset($addaskforaquotepro_error))
			$output .= $this->renderAddQuoteStateForm();
		elseif (Tools::isSubmit('updatefreesamplerequeststatuses') || isset($updateaskforaquotepros_error))
			$output .= $this->renderUpdateQuoteStateForm();
		else
		{
			$output .= $this->renderForm();
			$output .= $this->renderList();
		}

		$output .= '</div>';

		$this->context->controller->addJs($this->getPathUri().'views/js/content.js');

		return $output;
	}

	/**
	 * Update default value of the quote state
	 *
	 * @return bool
	 */
	protected function updateDefaultState($id_quote_state)
	{
		return QuoteState::setAsDefault($id_quote_state);
	}

	/**
	 * Update send email value of the quote state
	 *
	 * @return bool
	 */
	protected function updateSendEmailState($id_quote_state)
	{
		return QuoteState::toggleSendEmail($id_quote_state);
	}

	/**
	 * Add quote state process
	 */
	protected function addQuoteState()
	{
		$quote_state = new QuoteState();

		$this->copyFromPost($quote_state, 'quote_state');
		try
		{
			if ($quote_state->add())
			{
				if ($quote_state->default == 1)
					QuoteState::setAsDefault($quote_state->id);

				return true;
			}
			else
				return false;
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	/**
	 * Update quote state process
	 */
	protected function updateQuoteState()
	{
		$quote_state = new QuoteState();

		$this->copyFromPost($quote_state, 'quote_state');
		try {
			$quote_state->id = Tools::getValue('id_quote_state');
			$quote_state->id_quote_state = Tools::getValue('id_quote_state');

			if ($quote_state->update())
			{
				if ($quote_state->default == 1)
					QuoteState::setAsDefault($quote_state->id);

				return true;
			}
			else
				return false;
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	/**
	 * Delete quote state
	 *
	 * @param int $id_quote_state
	 * @return bool
	 */
	protected function deleteQuoteState($id_quote_state)
	{
		$quote_state = new QuoteState($id_quote_state);
		if (Validate::isLoadedObject($quote_state))
		{
			if ($quote_state->default == 0)
			{
				$quote_state->deleted = 1;
				$quote_state->save();

				return true;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Copy data values from $_POST to object
	 *
	 * @param ObjectModel &$object Object
	 * @param string $table Object table
	 */
	protected function copyFromPost(&$object, $table)
	{
		$languages = Language::getLanguages();

		/* Classical fields */
		foreach ($_POST as $key => $value)
			if (array_key_exists($key, $object) && $key != 'id_'.$table)
			{
				/* Do not take care of password field if empty */
				if ($key == 'passwd' && Tools::getValue('id_'.$table) && empty($value))
					continue;
				/* Automatically encrypt password in MD5 */
				if ($key == 'passwd' && !empty($value))
					$value = Tools::encrypt($value);
				$object->{$key} = $value;
			}

		/* Multilingual fields */
		$class_vars = get_class_vars(get_class($object));
		$fields = array();
		if (isset($class_vars['definition']['fields']))
			$fields = $class_vars['definition']['fields'];

		foreach ($fields as $field => $params)
			if (array_key_exists('lang', $params) && $params['lang'])
				foreach ($languages as $language)
				{
					$id_lang = $language['id_lang'];
					if (Tools::isSubmit($field.'_'.(int)$id_lang))
						$object->{$field}[(int)$id_lang] = Tools::getValue($field.'_'.(int)$id_lang);
				}
	}

	/**
	 * Return field value if possible (both classical and multilingual fields)
	 *
	 * Case 1 : Return value if present in $_POST / $_GET
	 * Case 2 : Return object value
	 *
	 * @param ObjectModel $obj Object
	 * @param string $key Field name
	 * @param int|null $id_lang Language id (optional)
	 * @return string
	 */
	public function getFieldValue($obj, $key, $id_lang = null)
	{
		if ($id_lang)
			$default_value = (isset($obj->id) && $obj->id && isset($obj->{$key}[$id_lang])) ? $obj->{$key}[$id_lang] : false;
		else
			$default_value = isset($obj->{$key}) ? $obj->{$key} : false;

		return Tools::getValue($key.($id_lang ? '_'.$id_lang : ''), $default_value);
	}

	/**
	 * Create the form that will be displayed in the configuration of your module.
	 */
	protected function renderForm()
	{
		$options = $this->generateCMS();

		$fields_form1 = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('General'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'switch',
						'label' => $this->l('Enable new sample requests'),
						'name' => 'MAIN_STATE',
						'desc' => $this->l('If disabled, no new submits are possible, but history is visible for both admin and clients'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Enabled') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('Disabled') ),
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Allow prices'),
						'name' => 'MAIN_PRICE',
						'desc' => $this->l('Display or not prices on customers overview page (admin always have them)'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Show') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('Hide') ),
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Take price groups into consideration'),
						'name' => 'AFQ_PRICE_GROUP',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Animate product to fly to cart (else popup option)'),
						'name' => 'MAIN_ANIMATE',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enable Submit now for logged in clients'),
						'name' => 'MAIN_POP_SUBMIT',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enable Guest checkout'),
						'name' => 'PS_GUEST_QUOTES_ENABLED',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Required terms and conditions'),
						'name' => 'MAIN_TERMS_AND_COND',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'select',
						'label' => $this->l('Select CMS Page with Terms and Conditions'),
						'id' => 'cms_page_select',
						'name' => 'MAIN_CMS_PAGE',
						'options' => array(
							'query' => $options,
							'id' => 'id',
							'name' => 'name' ),
						'identifier' => 'id',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Delivery address is required'),
						'name' => 'ADDRESS_ENABLED',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('User messaging'),
						'name' => 'MESSAGING_ENABLED',
						'desc' => $this->l('Allows the user to send messages to admin'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Filtered by product status'),
						'name' => 'MAIN_PRODUCT_STATUS',
						'desc' => $this->l('Present only if product not available for order'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Button on product page'),
						'name' => 'MAIN_PRODUCT_PAGE',
						'desc' => $this->l('If disabled, none of the below hooks will show'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Quantity field on product page'),
						'name' => 'MAIN_QUANTITY_FIELDS',
						'desc' => $this->l('If catalog mode only'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Show') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('Hide') ),
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Display between Useful links'),
						'name' => 'HOOK_PRODUCT_LEFT',
						'desc' => $this->l('hook in displayLeftColumnProduct'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Enabled') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('Disabled') ),
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Display above useful links'),
						'name' => 'HOOK_PRODUCT_RIGHT',
						'desc' => $this->l('hook in displayRightColumnProduct'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Enabled') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('Disabled') ),
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Display below the Cart button'),
						'name' => 'HOOK_PRODUCT_BUTTON',
						'desc' => $this->l('hook in displayProductButtons'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Enabled') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('Disabled') ),
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Button on product list'),
						'name' => 'MAIN_PRODUCT_LIST',
						'desc' => $this->l('Please note that the hook is disabled in homefeatured and index page'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Quantity field on product list'),
						'name' => 'MAIN_QUANTITY_LIST',
						'desc' => $this->l('If catalog mode only'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Show') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('Hide') ),
						),
					),
					/* array(
					  'type' => 'switch',
					  'label' => $this->l( 'Limit quotable quantitiy status' ),
					  'name' => 'LIMIT_QUOTED_PRODUCTS_STATUS',
					  'values' => array(
					  array(
					  'id' => 'on',
					  'value' => 1,
					  'label' => $this->l( 'Yes' ) ),
					  array(
					  'id' => 'off',
					  'value' => 0,
					  'label' => $this->l( 'No' ) ),
					  )
					  ),
					  array(
					  'type' => 'text',
					  'label' => $this->l( 'Limit quotable quantitiy:' ),
					  'name' => 'LIMIT_QUOTED_PRODUCTS_QUANTITY',
					  'desc' => $this->l( 'Enter the number of products you want to limit when requesting a quote.' ),
					  'value' => 3
						),*/
					array(
						'type' => 'switch',
						'label' => $this->l('Reset CSS'),
						'name' => 'MAIN_HACK_THEME',
						'desc' => $this->l('Will apply some basic fixes for default theme to fit the request cart in header'),
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'text',
						'label' => $this->l('Email addresses:'),
						'name' => 'MAIN_MAILS',
						'desc' => $this->l('Enter the email addresses separated by a comma (",") where you need submitted request notifications to be sent')
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Max quantity'),
						'name' => 'AFQ_MAX_QUANTITY',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'text',
						'label' => $this->l('Max quantity value'),
						'name' => 'AFQ_MAX_QUANTITY_VALUE',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Force quantity'),
						'name' => 'AFQ_FORCE_QUANTITY',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'text',
						'label' => $this->l('Force quantity value'),
						'name' => 'AFQ_FORCE_QUANTITY_VALUE',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Hide top cart'),
						'name' => 'AFQ_HIDE_TOP_CART',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
				),
				'bottom' => '<script type="text/javascript">showBlock(element);hideBlock(element);</script>',
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$fields_form2 = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Category permissions'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'switch',
						'label' => $this->l('Category permission recursive to products'),
						'name' => 'CATEGORY_RECURSIVE',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'categories',
						'name' => 'CATEGORY_BOX',
						'tree' => array(
							'id' => 'associated-categories-tree',
							'title' => $this->l('Filter on category base'),
							'use_search' => 1,
							'use_checkbox' => 1,
							'selected_categories' => explode(',', Configuration::get('CATEGORY_BOX'))
						)
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
				Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitMainSettings';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
				'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFormValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id );

		return $helper->generateForm(array( $fields_form1, $fields_form2 ));
	}

	/**
	 * Create form to add quote state
	 */
	protected function renderAddQuoteStateForm()
	{
		$fields_form1 = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Request status'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Status name'),
						'name' => 'name',
						'lang' => true,
						'required' => true,
						'hint' => array(
							$this->l('Request status (e.g. \'Pending\').'),
							$this->l('Invalid characters: numbers and').' !<>,;?=+()@#"{}_$%:'
						)
					),
					array(
						'type' => 'color',
						'label' => $this->l('Color'),
						'name' => 'color',
						'hint' => $this->l('Status will be highlighted in this color. HTML colors only.').' "lightblue", "#CC6600")'
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Send email'),
						'name' => 'send_email',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Default'),
						'name' => 'default',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
				Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitAddQuoteStateSettings';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
				'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getQuoteStateFormValues(new QuoteState(), array( $fields_form1 )),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id );

		return $helper->generateForm(array( $fields_form1 ));
	}

	/**
	 * Create form to add quote state
	 */
	protected function renderUpdateQuoteStateForm()
	{
		$fields_form1 = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Request status'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'hidden',
						'name' => 'id_quote_state',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Status name'),
						'name' => 'name',
						'lang' => true,
						'required' => true,
						'hint' => array(
							$this->l('Request status (e.g. \'Pending\').'),
							$this->l('Invalid characters: numbers and').' !<>,;?=+()@#"{}_$%:'
						)
					),
					array(
						'type' => 'color',
						'label' => $this->l('Color'),
						'name' => 'color',
						'hint' => $this->l('Status will be highlighted in this color. HTML colors only.').' "lightblue", "#CC6600")'
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Send email'),
						'name' => 'send_email',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Default'),
						'name' => 'default',
						'values' => array(
							array(
								'id' => 'on',
								'value' => 1,
								'label' => $this->l('Yes') ),
							array(
								'id' => 'off',
								'value' => 0,
								'label' => $this->l('No') ),
						)
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$quote_state = new QuoteState((int)Tools::getValue('id_quote_state'));

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
				Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitUpdateQuoteStateSettings';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
				'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getQuoteStateFormValues($quote_state, array( $fields_form1 )),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id );

		return $helper->generateForm(array( $fields_form1 ));
	}

	/**
	 * Get object values for form
	 */
	protected function getQuoteStateFormValues($obj, $fields_form)
	{
		$field_values = array();

		$languages = Language::getLanguages(true);

		foreach ($fields_form as $fieldset)
			if (isset($fieldset['form']['input']))
				foreach ($fieldset['form']['input'] as $input)
					if (!isset($field_values[$input['name']]))
						if (isset($input['type']) && $input['type'] == 'shop')
						{
							if ($obj->id)
							{
								$result = Shop::getShopById((int)$obj->id, $this->identifier, $this->table);
								foreach ($result as $row)
									$field_values['shop'][$row['id_'.$input['type']]][] = $row['id_shop'];
							}
						}
						elseif (isset($input['lang']) && $input['lang'])
							foreach ($languages as $language)
							{
								$field_value = $this->getFieldValue($obj, $input['name'], $language['id_lang']);
								if (empty($field_value))
								{
									if (isset($input['default_value']) && is_array($input['default_value']) && isset($input['default_value'][$language['id_lang']]))
										$field_value = $input['default_value'][$language['id_lang']];
									elseif (isset($input['default_value']))
										$field_value = $input['default_value'];
								}
								$field_values[$input['name']][$language['id_lang']] = $field_value;
							}
						else
						{
							$field_value = $this->getFieldValue($obj, $input['name']);
							if ($field_value === false && isset($input['default_value']))
								$field_value = $input['default_value'];
							$field_values[$input['name']] = $field_value;
						}

		return $field_values;
	}

	public function renderList()
	{
		$this->fields_list = array(
			'id_quote_state' => array(
				'title' => $this->l('ID'),
				'align' => 'text-center',
				'class' => 'fixed-width-xs'
			),
			'name' => array(
				'title' => $this->l('Name'),
				'width' => 'auto',
				'color' => 'color'
			),
			'send_email' => array(
				'title' => $this->l('Send email to customer'),
				'align' => 'text-center',
				'active' => 'sendEmail',
				'type' => 'bool',
				'ajax' => false,
				'orderby' => false,
				'class' => 'fixed-width-sm'
			),
			'default' => array(
				'title' => $this->l('Default'),
				'align' => 'text-center',
				'active' => 'default',
				'type' => 'bool',
				'ajax' => false,
				'orderby' => false,
				'class' => 'fixed-width-sm'
			),
		);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->simple_header = false;
		$helper->identifier = 'id_quote_state';
		$helper->actions = array('edit', 'delete');
		$helper->show_toolbar = true;
		$helper->imageType = 'jpg';
		$helper->toolbar_btn['new'] = array(
			'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
			'desc' => $this->l('Add new')
		);

		$helper->title = $this->l('Statuses');
		$helper->table = $this->name.'statuses';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

		$content = $this->getListContent($this->context->language->id);

		return $helper->generateList($content, $this->fields_list);
	}

	protected function getListContent($id_lang = null)
	{
		if (is_null($id_lang))
			$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$sql = 'SELECT qs.`id_quote_state`, qsl.`name`, qs.`color`, qs.`send_email`, qs.`default`
				FROM `'._DB_PREFIX_.'quote_state` qs
				LEFT JOIN `'._DB_PREFIX_.'quote_state_lang` qsl ON (qs.`id_quote_state` = qsl.`id_quote_state`)
				WHERE qsl.`id_lang` = '.(int)$id_lang.'
				AND qs.`deleted` = 0';

		$content = Db::getInstance()->executeS($sql);

		return $content;
	}

	/**
	 * Set values for the inputs.
	 */
	protected function getConfigFormValues()
	{
		return array(
			'MAIN_STATE' => Configuration::get('MAIN_STATE'),
			'MAIN_QUANTITY_FIELDS' => Configuration::get('MAIN_QUANTITY_FIELDS'),
			'MAIN_QUANTITY_LIST' => Configuration::get('MAIN_QUANTITY_LIST'),
			'MAIN_PRICE' => Configuration::get('MAIN_PRICE'),
			'MAIN_ANIMATE' => Configuration::get('MAIN_ANIMATE'),
			'MAIN_TERMS_AND_COND' => Configuration::get('MAIN_TERMS_AND_COND'),
			'MAIN_CMS_PAGE' => Configuration::get('MAIN_CMS_PAGE'),
			'PS_GUEST_QUOTES_ENABLED' => Configuration::get('PS_GUEST_QUOTES_ENABLED'),
			'ADDRESS_ENABLED' => Configuration::get('ADDRESS_ENABLED'),
			'MESSAGING_ENABLED' => Configuration::get('MESSAGING_ENABLED'),
			'CATEGORY_BOX' => explode(',', Configuration::get('CATEGORY_BOX')),
			'MAIN_PRODUCT_STATUS' => Configuration::get('MAIN_PRODUCT_STATUS'),
			'MAIN_PRODUCT_PAGE' => Configuration::get('MAIN_PRODUCT_PAGE'),
			'MAIN_POP_SUBMIT' => Configuration::get('MAIN_POP_SUBMIT'),
			'MAIN_PRODUCT_LIST' => Configuration::get('MAIN_PRODUCT_LIST'),
			'MAIN_MAILS' => Configuration::get('MAIN_MAILS'),
			'MAIN_HACK_THEME' => Configuration::get('MAIN_HACK_THEME'),
			'HOOK_PRODUCT_BUTTON' => Configuration::get('HOOK_PRODUCT_BUTTON'),
			'HOOK_PRODUCT_LEFT' => Configuration::get('HOOK_PRODUCT_LEFT'),
			'HOOK_PRODUCT_RIGHT' => Configuration::get('HOOK_PRODUCT_RIGHT'),
			'CATEGORY_RECURSIVE' => Configuration::get('CATEGORY_RECURSIVE'),
			'LIMIT_QUOTED_PRODUCTS_STATUS' => Configuration::get('LIMIT_QUOTED_PRODUCTS_STATUS'),
			'LIMIT_QUOTED_PRODUCTS_QUANTITY' => Configuration::get('LIMIT_QUOTED_PRODUCTS_QUANTITY'),
			'AFQ_PRICE_GROUP' => Configuration::get('AFQ_PRICE_GROUP'),
			'AFQ_MAX_QUANTITY' => Configuration::get('AFQ_MAX_QUANTITY'),
			'AFQ_MAX_QUANTITY_VALUE' => Configuration::get('AFQ_MAX_QUANTITY_VALUE'),
			'AFQ_FORCE_QUANTITY' => Configuration::get('AFQ_FORCE_QUANTITY'),
			'AFQ_FORCE_QUANTITY_VALUE' => Configuration::get('AFQ_FORCE_QUANTITY_VALUE'),
			'AFQ_HIDE_TOP_CART' => Configuration::get('AFQ_HIDE_TOP_CART'));
	}

	/**
	 * Add the CSS & JavaScript files you want to be loaded in the BO.
	 */
	public function hookdisplayBackOfficeHeader()
	{
		$this->context->controller->addJquery();
		$this->context->controller->addJS($this->_path.'views/js/back.js');
		$this->context->controller->addCSS($this->_path.'views/css/back.css');
	}

	/**
	 * Add the CSS & JavaScript files you want to be added on the FO.
	 */
	public function hookHeader()
	{
		$this->context->controller->addJS($this->_path.'/views/js/front.js');
		$this->context->controller->addCSS($this->_path.'/views/css/front.css');
		if (Configuration::get('MAIN_HACK_THEME'))
			$this->context->controller->addCSS($this->_path.'/views/css/reset.css');
		if (Configuration::get('AFQ_FORCE_QUANTITY'))
			$this->context->controller->addCSS($this->_path.'/views/css/front_force_quantity.css');

		// start variable needed in case customer unhook top manually
		$this->context->smarty->assign(
			'actionAddQuotes',
			$this->context->link->getModuleLink($this->name, 'SampleCart', array(), Tools::usingSecureMode())
		);

		$this->context->smarty->assign('quotesCart', $this->context->link->getModuleLink($this->name, 'SampleCart', array(), Tools::usingSecureMode()));

		if (Configuration::get('MAIN_TERMS_AND_COND'))
			$this->context->smarty->assign('terms_page', Configuration::get('MAIN_CMS_PAGE'));
		else
			$this->context->smarty->assign('terms_page', 0);

		$this->context->smarty->assign('MESSAGING_ENABLED', Configuration::get('MESSAGING_ENABLED'));
		$this->context->smarty->assign('AFQ_HIDE_TOP_CART', Configuration::get('AFQ_HIDE_TOP_CART'));
		// end variable needed in case customer unhook top manually

		if (Configuration::get('MAIN_STATE'))
			return $this->display(__file__, 'header.tpl');
	}

	public function hookTop()
	{
		//load model
		include_once (_PS_MODULE_DIR_.'freesamplerequest/classes/QuotesProduct.php');
		$quote_obj = new QuotesProductCart;

		// user logged in trriger
		if ($this->context->customer->logged && !$this->context->cookie->__isset('user_request_in'))
			$this->context->cookie->__set('user_request_in', '1');

		//clear cart action
		if ($this->context->cookie->__isset('request_id') && !$this->context->customer->logged &&
			$this->context->cookie->__isset('user_request_in'))
		{
			$quote_obj->id = $this->context->cookie->__get('request_id');
			$quote_obj->deleteAllProduct();

			//delete customer session
			$this->context->cookie->__unset('user_request_in');
		}

		$cart = array();
		$products = array();
		// check for user cart session. Defined in SampleCart if user add product to quote box
		if ($this->context->cookie->__isset('request_id'))
		{
			$quote_obj->id_quote = $this->context->cookie->__get('request_id');
			if ($quote_obj->getProducts())
				list($products, $cart) = $quote_obj->getProducts();
			else
			{
				$cart = array();
				$products = array();
			}
		}

		$this->context->smarty->assign('session', $this->context->cookie->__get('request_id'));
		$this->context->smarty->assign('products', $products);
		$this->context->smarty->assign('cart_total', $cart);
		$this->context->smarty->assign('active_overlay', 0);
		$this->context->smarty->assign('enable_submit', Configuration::get('MAIN_POP_SUBMIT'));
		$this->context->smarty->assign('MAIN_PRICE', Configuration::get('MAIN_PRICE'));

		$this->context->smarty->assign('quoteTotalProducts', (int)count($products));

		$tpl_path_local = $this->getLocalPath();
		$tpl_theme_path = _PS_THEME_DIR_.'modules/askforaquote/';

		if (file_exists($tpl_theme_path.'views/templates/hook/product-cart-item.tpl'))
			$this->context->smarty->assign('item_tpl_dir', $tpl_theme_path.'views/templates/hook/');
		else
			$this->context->smarty->assign('item_tpl_dir', $tpl_path_local.'views/templates/hook/');

		if (Configuration::get('MAIN_STATE'))
			return $this->display(__file__, 'quotesCart.tpl');
	}

	public function hookDisplayNav($params)
	{
		return $this->hookTop($params);
	}

	public function hookLeftColumn($params)
	{
		return $this->hookTop($params);
	}

	public function hookRightColumn($params)
	{
		return $this->hookTop($params);
	}

	/**
	 * Add ask to quote button to product
	 */
	public function hookextraRight($params)
	{
		if (!isset($params['position']))
			$params['position'] = 'right';

		$product = new Product(Tools::getValue('id_product'), true, $this->context->language->id, $this->context->shop->id);

		$customer = (($this->context->cookie->logged) ? (int)$this->context->cookie->id_customer : 0);

		$category_box = Configuration::get('CATEGORY_BOX');
		$categories = !empty($category_box) ? explode(',', $category_box) : false;

		if (!in_array($product->id_category_default, $categories) && Configuration::get('CATEGORY_RECURSIVE'))
			$show_by_category = 0;
		else
			$show_by_category = 1;

		$this->context->smarty->assign('id_product', Tools::getValue('id_product'));
		$this->context->smarty->assign('isLogged', $customer);
		$this->context->smarty->assign('product', $product);
		$this->context->smarty->assign('MAIN_QUANTITY_FIELDS', Configuration::get('MAIN_QUANTITY_FIELDS'));
		$this->context->smarty->assign('enableAnimation', Configuration::get('MAIN_ANIMATE'));
		$this->context->smarty->assign('filtered_on_status', Configuration::get('MAIN_PRODUCT_STATUS'));
		$this->context->smarty->assign('present_on_product', Configuration::get('MAIN_PRODUCT_PAGE'));
		$this->context->smarty->assign('SHOW_BY_CATEGORY', $show_by_category);

		$link_core = new Link;
		$this->context->smarty->assign('plink', $link_core->getProductLink($product->id, $product->link_rewrite, $product->id_category_default));

		if ($params['position'] == 'product_btn')
			if (Configuration::get('MAIN_STATE') && Configuration::get('HOOK_PRODUCT_BUTTON'))
				return $this->display(__file__, 'productButton.tpl');

		if ($params['position'] == 'right')
		{
			$this_display = Configuration::get('HOOK_PRODUCT_RIGHT');
			$this->context->smarty->assign('buttonPosition', 'right');
		}

		if ($params['position'] == 'left')
		{
			$this_display = Configuration::get('HOOK_PRODUCT_LEFT');
			$this->context->smarty->assign('buttonPosition', 'left');
		}

		if (Configuration::get('MAIN_STATE') && $this_display)
			return $this->display(__file__, 'extraRight.tpl');
	}

	public function hookextraLeft($params)
	{
		$params['position'] = 'left';
		return $this->hookextraRight($params);
	}

	public function hookdisplayProductButtons($params)
	{
		$params['position'] = 'product_btn';

		return $this->hookextraRight($params);
	}

	/**
	 * Add ask to quote button to product list
	 */
	public function hookdisplayProductListFunctionalButtons($params)
	{
		$customer = (($this->context->cookie->logged) ? (int)$this->context->cookie->id_customer : 0);
		$this->context->smarty->assign('isLogged', $customer);
		$this->context->smarty->assign('enableAnimation', Configuration::get('MAIN_ANIMATE'));
		$category_box = Configuration::get('CATEGORY_BOX');
		$categories = !empty($category_box) ? explode(',', $category_box) : false;
		$this->context->smarty->assign('categories', $categories);
		$this->context->smarty->assign('MAIN_QUANTITY_LIST', Configuration::get('MAIN_QUANTITY_LIST'));
		$this->context->smarty->assign('filtered_on_status', Configuration::get('MAIN_PRODUCT_STATUS'));
		$this->context->smarty->assign('present_on_product_list', Configuration::get('MAIN_PRODUCT_LIST'));
		$this->smarty->assign('product', $params['product']);
		if (Configuration::get('MAIN_STATE'))
			return $this->display(__file__, 'product-list.tpl');
	}

	/**
	 * Add quote link in my account
	 */
	public function hookCustomerAccount()
	{
		return $this->display(__file__, 'my-account.tpl');
	}

	/**
	 * Add quote link in my account footer
	 */
	public function hookdisplayMyAccountBlockfooter()
	{
		return $this->display(__file__, 'blockMyaccountFooter.tpl');
	}

	/**
	 * Add ask to quote button to product list
	 */
	private function generateCMS()
	{
		$pages = CMS::getCMSPages((int)$this->context->language->id, null, true);
		$out = array();
		if (!empty($pages))
		{
			foreach ($pages as $page)
			{
				$out[] = array(
					'id' => $page['id_cms'],
					'value' => $page['id_cms'],
					'name' => $page['meta_title'] );
			}
		}
		else
			$out[] = array( 'name' => $this->l('No cms pages found') );

		return $out;
	}

	public function hookActionOrderStatusUpdate($a_params)
	{
		include_once(_PS_MODULE_DIR_.'freesamplerequest/classes/QuotesObj.php');

		$id_cart = $a_params['cart']->id;
		$id_customer = $a_params['cart']->id_customer;

		$quote = new QuotesObj();
		$quote_arr = $quote->getQuoteByCustomerAndCart($id_customer, $id_cart);

		if (isset($quote_arr[0]))
			$quote->submitTransformQuote($quote_arr[0]['id_quote']);
	}

}
