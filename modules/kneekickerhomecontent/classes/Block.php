<?php
/**
 *  Our Home block module definition. Storing a text block and translation options.
 *
 *  Predict Marketing
 *  Ben Jackson
 * 
 */

class Block extends ObjectModel {
	
	public $id;
	public $id_shop;
	public $text;

	/**
	 * See Prestashop ObjectModel::$definition
	 * 
	 */
	public static $definition = array(
		'table' => 'homeblock',
		'primary' => 'id_homeblock',
		'multilang' => true,
		'fields' => array(
			'id_shop' => array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
			'text'    => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true)
		)
	);

}