<?php
/**
 *  Knee Kicker Home Content Block. This is for adding some about us text, maybe content 
 *  with a read more link. 
 *
 *	Predict Marketing
 *	Ben Jackson
 */
if(!defined('_PS_VERSION'))
	exit;

require_once _PS_MODULE_DIR_.'kneekickerhomecontent/classes/Block.php';

class kneeKickerHomeContent extends Module {

	/**
	 *  The module definition init for prestashop
	 */
	public function __construct()
	{
		$this->name = 'kneekickerhomecontent';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Predict Marketing';
		$this->bootstrap = true;
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('KneeKicker Home Content Block');
		$this->description = $this->l('Adds a Home content block for KneeKicker');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION);
	}

	
	/**
	 * Call the parent constructor, 
	 * register our hook on the homepage.
	 * 
	 */
	public function install()
	{
		return parent::install() && 
			   $this->installDB() && 
			   $this->registerHook('home') && 
			   $this->installFixtures() && 
			   $this->disableDevice(Context::DEVICE_TABLET | Context::DEVICE_MOBILE);
	} 


	/**
	 * A prestashop recommended way of creating tables
	 * 
	 * @return the DB mysql statements
	 */
	public function installDB()
	{
		$return = true;
		$return &= Db::getInstance()->execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'homeblock` (
				`id_homeblock` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_shop` int(10) unsigned DEFAULT NULL,
				PRIMARY KEY (`id_homeblock`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
		);

		$return &= Db::getInstance()->execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'homeblock_lang` (
				`id_homeblock` INT UNSIGNED NOT NULL,
				`id_lang` int(10) unsigned NOT NULL ,
				`text` text NOT NULL,
				PRIMARY KEY (`id_homeblock`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
		);

		return $return;
	}

	public function uninstall()
	{
		return parent::uninstall() && $this->uninstallDB();
	}

	
	/**
	 * Another prestashop method, I dont know why we are saving true to a variable
	 * and then chaining the query, this is all prestashop. 
	 * 
	 * @param  boolean $drop_table if we are dropping the table
	 * @return Db query
	 */
	public function uninstallDB($drop_table = true)
	{
		$return = true;
		if($drop_table)
			$return &=  Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'homeblock`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'homeblock_lang`');

		return $return;
	}


	public function getContent()
	{
		$id_homeblock = (int)Tools::getValue('id_homeblock');

		if (Tools::isSubmit('saveblockcmsinfo'))
		{
			if (!Tools::getValue('text_'.(int)Configuration::get('PS_LANG_DEFAULT'), false))
				return $this->html . $this->displayError($this->l('You must fill in all fields.')) . $this->renderForm();
			elseif ($this->processSaveCmsInfo())
				return $this->html . $this->renderList();
			else
				return $this->html . $this->renderForm();
		}
		elseif (Tools::isSubmit('updateblockcmsinfo') || Tools::isSubmit('addblockcmsinfo'))
		{
			$this->html .= $this->renderForm();
			return $this->html;
		}
		else if (Tools::isSubmit('deleteblockcmsinfo'))
		{
			$info = new Block((int)$id_homeblock);
			$info->delete();
			$this->_clearCache('kneekickerhomecontent.tpl');
			Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
		}
		else
		{
			$this->html .= $this->renderList();
			return $this->html;
		}

	}

	public function processSaveCmsInfo()
	{
		if ($id_homeblock = Tools::getValue('id_homeblock'))
			$info = new Block((int)$id_homeblock);
		else
		{
			$info = new Block();
			if (Shop::isFeatureActive())
			{
				$shop_ids = Tools::getValue('checkBoxShopAsso_configuration');
				if (!$shop_ids)
				{
					$this->html .= '<div class="alert alert-danger conf error">'.$this->l('You have to select at least one shop.').'</div>';
					return false;
				}
			}
			else
				$info->id_shop = Shop::getContextShopID();
		}

		$languages = Language::getLanguages(false);
		$text = array();
		foreach ($languages AS $lang)
			$text[$lang['id_lang']] = Tools::getValue('text_'.$lang['id_lang']);
		$info->text = $text;

		if (Shop::isFeatureActive() && !$info->id_shop)
		{
			$saved = true;
			foreach ($shop_ids as $id_shop)
			{
				$info->id_shop = $id_shop;
				$saved &= $info->add();
			}
		}
		else
			$saved = $info->save();

		if ($saved)
			$this->_clearCache('kneekickerhomecontent.tpl');
		else
			$this->html .= '<div class="alert alert-danger conf error">'.$this->l('An error occurred while attempting to save.').'</div>';

		return $saved;
	}

	protected function renderForm()
	{
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$fields_form = array(
			'tinymce' => true,
			'legend' => array(
				'title' => $this->l('Homepage content block'),
			),
			'input' => array(
				'id_homeblock' => array(
					'type' => 'hidden',
					'name' => 'id_homeblock'
				),
				'content' => array(
					'type' => 'textarea',
					'label' => $this->l('Text'),
					'lang' => true,
					'name' => 'text',
					'cols' => 40,
					'rows' => 10,
					'class' => 'rte',
					'autoload_rte' => true,
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
			),
			'buttons' => array(
				array(
					'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
					'title' => $this->l('Back to list'),
					'icon' => 'process-icon-back'
				)
			)
		);

		if (Shop::isFeatureActive() && Tools::getValue('id_homeblock') == false)
		{
			$fields_form['input'][] = array(
				'type' => 'shop',
				'label' => $this->l('Shop association'),
				'name' => 'checkBoxShopAsso_theme'
			);
		}


		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = 'kneekickerhomecontent';
		$helper->identifier = $this->identifier;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		foreach (Language::getLanguages(false) as $lang)
			$helper->languages[] = array(
				'id_lang' => $lang['id_lang'],
				'iso_code' => $lang['iso_code'],
				'name' => $lang['name'],
				'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
			);

		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->toolbar_scroll = true;
		$helper->title = $this->displayName;
		$helper->submit_action = 'saveblockcmsinfo';

		$helper->fields_value = $this->getFormValues();

		return $helper->generateForm(array(array('form' => $fields_form)));
	}

	protected function renderList()
	{
		$this->fields_list = array();
		$this->fields_list['id_info'] = array(
				'title' => $this->l('Block ID'),
				'type' => 'text',
				'search' => false,
				'orderby' => false,
			);

		if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
			$this->fields_list['shop_name'] = array(
					'title' => $this->l('Shop'),
					'type' => 'text',
					'search' => false,
					'orderby' => false,
				);

		$this->fields_list['text'] = array(
				'title' => $this->l('Block text'),
				'type' => 'text',
				'search' => false,
				'orderby' => false,
			);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->simple_header = false;
		$helper->identifier = 'id_info';
		$helper->actions = array('edit', 'delete');
		$helper->show_toolbar = true;
		$helper->imageType = 'jpg';
		$helper->toolbar_btn['new'] = array(
			'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
			'desc' => $this->l('Add new')
		);

		$helper->title = $this->displayName;
		$helper->table = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

		$content = $this->getListContent($this->context->language->id);

		return $helper->generateList($content, $this->fields_list);
	}



}