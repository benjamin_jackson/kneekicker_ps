          $(document).ready(function() {

              $(document).on('change', 'select[name=id_contact]', function() {
                  showElemFromSelect('id_contact', 'desc_contact')
              });
              $('#id_contact option[value="2"]').attr("selected", true);

              $(document).on('click', '.open_inquiry_form', function(e) {

                  e.preventDefault();
                  $('#message').html('');
                  $('#new_inquiry_form_error ul').html('');
                  $('#uniform-id_contact').removeAttr('style');
                  $('#uniform-id_contact span').removeAttr('style');

                  $.fancybox.open({
                      'autoSize': false,
                      'width': 600,
                      'height': 'auto',
                      'hideOnContentClick': false,
                      'href': '#new_inquiry_form'
                  });

              });
              $(document).on('click', '#submitNewInquiry', (function(e) {

                  if (window.FormData !== undefined) // for HTML5 browsers
                  {
                      var formData = new FormData($('#id_new_inquiry_form')[0]);
                      formData.append('submitMessage', '1');
                      $.ajax({
                          url: productinquiry_controller_url,
                          type: 'POST',
                          data: formData,
                          mimeType: "multipart/form-data",
                          contentType: false,
                          processData: false,
                          cache: false,
                          success: function(data) {
                              // if ($(data).find('.alert-danger').length) {
                              //     $('#new_inquiry_form_error ul').html('');
                              //     $('#new_inquiry_form_error ul').html($('<div />').html(data).find('.alert-danger ol').html());
                              //     $('#new_inquiry_form_error').slideDown('slow');
                              // } else {
                                  $.fancybox.close();
                                  var buttons = {};
                                  buttons[productinquiry_ok] = "productinquiryRefreshPage";
                                  fancyChooseBox(productinquiry_added, productinquiry_title, buttons);
                              // }
                          },
                          error: function(jqXHR, textStatus, errorThrown) {
                              $.fancybox.close();
                              var buttons = {};
                              buttons[productinquiry_ok] = "productinquiryRefreshPage";
                              fancyChooseBox(errorThrown, 'On Errors!, Please try again', buttons);
                          }
                      });
                      // Kill default behaviour
                      e.preventDefault();
                      // Form element   
                  }
              }));
          });
          function productinquiryRefreshPage() {
              window.location.reload();
          }