<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class ProductInquiry extends Module
{
	
	public function __construct()
	{
		$this->name = 'productinquiry';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Predict Marketing';
		$this->need_instance = 0;
		$this->bootstrap = true;

		
		parent::__construct();

		$this->secure_key = Tools::encrypt($this->name);

		$this->displayName = $this->l('Product Inquiry');
		$this->description = $this->l('Allows users to inquiry a product or ask questions.');
	}

	public function install()
	{		

		if (!parent::install() ||			
			!$this->registerHook('header') ||
			!$this->registerHook('displayRightColumnProduct') ||
			!$this->registerHook('productActions'))
			return false;
		return true;
		 
	}

	public function uninstall($keep = true)
	{
		if (!parent::uninstall() || 			
			!$this->unregisterHook('displayRightColumnProduct') || 
			!$this->unregisterHook('productActions') ||                    
			!$this->unregisterHook('header'))			
			return false;
		return true;
	}

	public function reset()
	{
		if (!$this->uninstall(false))
			return false;
		if (!$this->install(false))
			return false;
		return true;
	}
        
    public function hookHeader()
 	{
        $this->context->controller->addCSS($this->_path.'css/productinquiry.css', 'all');
        $this->context->controller->addJS($this->_path.'js/productinquiry.js');
        $this->context->controller->addJS(_PS_JS_DIR_.'validate.js');
    }

    public function hookProductActions($params)
    {
    	//include_once(_PS_FRONT_CONTROLLER_DIR_."/Contactcontroller.php");
                
		$id_guest = (!$id_customer = (int)$this->context->cookie->id_customer) ? (int)$this->context->cookie->id_guest : false;
		
                $product = $this->context->controller->getProduct();
		$image = Product::getCover((int)Tools::getValue('id_product'));
		$cover_image = $this->context->link->getImageLink($product->link_rewrite, $image['id_image'], 'medium_default');
                
                $email = Tools::safeOutput(Tools::getValue('from',
		((isset($this->context->cookie) && isset($this->context->cookie->email) && Validate::isEmail($this->context->cookie->email)) ? $this->context->cookie->email : '')));

                $this->context->smarty->assign(array(
			'id_product_inquiry_form' => (int)Tools::getValue('id_product'),
			'product' => $product,
			'secure_key' => $this->secure_key,
			'logged' => $this->context->customer->isLogged(true),                        
			'allow_guests' => (int)Configuration::get('PRODUCT_COMMENTS_ALLOW_GUESTS'),
			'productcomment_cover' => (int)Tools::getValue('id_product').'-'.(int)$image['id_image'], // retro compat
			'productcomment_cover_image' => $cover_image,
			'mediumSize' => Image::getSize(ImageType::getFormatedName('medium')),
                        'productinquiry_controller_url' => $this->context->link->getPageLink('contact'),
			'productinquiry_url_rewriting_activated' => Configuration::get('PS_REWRITING_SETTINGS', 0),
                        'email' => $email,
                        'contacts' => Contact::getContacts($this->context->language->id)

	   ));

		return ($this->display(__FILE__, '/productinquiry.tpl'));
    }
       
	public function hookDisplayRightColumnProduct($params)
	{
		//include_once(_PS_FRONT_CONTROLLER_DIR_."/Contactcontroller.php");
                
		$id_guest = (!$id_customer = (int)$this->context->cookie->id_customer) ? (int)$this->context->cookie->id_guest : false;
		
                $product = $this->context->controller->getProduct();
		$image = Product::getCover((int)Tools::getValue('id_product'));
		$cover_image = $this->context->link->getImageLink($product->link_rewrite, $image['id_image'], 'medium_default');
                
                $email = Tools::safeOutput(Tools::getValue('from',
		((isset($this->context->cookie) && isset($this->context->cookie->email) && Validate::isEmail($this->context->cookie->email)) ? $this->context->cookie->email : '')));

                $this->context->smarty->assign(array(
			'id_product_inquiry_form' => (int)Tools::getValue('id_product'),
			'product' => $product,
			'secure_key' => $this->secure_key,
			'logged' => $this->context->customer->isLogged(true),                        
			'allow_guests' => (int)Configuration::get('PRODUCT_COMMENTS_ALLOW_GUESTS'),
			'productcomment_cover' => (int)Tools::getValue('id_product').'-'.(int)$image['id_image'], // retro compat
			'productcomment_cover_image' => $cover_image,
			'mediumSize' => Image::getSize(ImageType::getFormatedName('medium')),
                        'productinquiry_controller_url' => $this->context->link->getPageLink('contact'),
			'productinquiry_url_rewriting_activated' => Configuration::get('PS_REWRITING_SETTINGS', 0),
                        'email' => $email,
                        'contacts' => Contact::getContacts($this->context->language->id)

	   ));

		return ($this->display(__FILE__, '/productinquiry.tpl'));
	}

	public function hookDisplayLeftColumnProduct($params)
	{
		return $this->hookProductActions($params);
	}
           	
}
