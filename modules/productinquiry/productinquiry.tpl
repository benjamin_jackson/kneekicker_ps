 {*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!--  Module ProductComments -->
<script type="text/javascript">
var productinquiry_controller_url = '{$productinquiry_controller_url}';
var secure_key = '{$secure_key}';
var productinquiry_added = '{l s='Your free sample request has been sent successfully!' mod='productinquiry' js=1}';
var productinquiry_title = '{l s='Thank you!' mod='productinquiry' js=1}';
var productinquiry_ok = '{l s='OK' mod='productinquiry' js=1}'; 
</script>
<!--bof service center  -->
<div id="open_inquiry_form" class="freeSample">                       
	<div class="contact-us-box"> <i class="fa fa-plus"></i>                
	    <span class="send-to-link"><a class="open_inquiry_form" href="#new_inquiry_form">{l s='Free Sample' mod='productinquiry'}</a></span>                        
	</div> 
</div>    
<!--eof service center  -->
<!-- Fancybox -->
<div style="display:none">
	<div id="new_inquiry_form">
		<form id="id_new_inquiry_form" action="#" class="contact-form-box" >
                    <fieldset>
                        <input id="id_product" name="id_product" type="hidden" value='{$id_product_inquiry_form}' readonly="readonly" />
			<h2 class="title">{l s='Get your free sample' mod='productinquiry'}</h2>
			{if isset($product) && $product}
			<div class="product clearfix col-xs-12 col-sm-6">
				<img src="{$productcomment_cover_image}" height="{$mediumSize.height}" width="{$mediumSize.width}" alt="{$product->name|escape:html:'UTF-8'}" />
				<div class="product_desc">
					<p class="product_name"><strong>{$product->name}</strong></p>
					{$product->description_short}
				</div>
			</div>
			{/if}
			<div class="new_inquiry_form_content col-xs-12 col-sm-6">

                            <div id="new_inquiry_form_error" class="error" style="display:none;padding:15px 25px">
					<ul></ul>
			    </div>
                                
                                <div class="form-group">
                                    <label for="id_contact">{l s='Subject Heading'}</label>
                                    <select id="id_contact" class="form-control" name="id_contact" type="hidden">
                                        <option value="0">{l s='-- Choose --'}</option>
                                        {foreach from=$contacts item=contact}
                                            <option value="{$contact.id_contact|intval}" {if isset($smarty.request.id_contact) && $smarty.request.id_contact == $contact.id_contact}selected="selected"{/if}>{$contact.name|escape:'html':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                    <span id="desc_contact0" class="desc_contact" style="display:none;">&nbsp;</span>
                                    {foreach from=$contacts item=contact}
                                        <span id="desc_contact{$contact.id_contact|intval}" class="desc_contact contact-title" style="display:none;">
                                            <i class="fa fa-comment-alt"></i>{$contact.description|escape:'html':'UTF-8'}
                                        </span>
                                    {/foreach}
                                </div>
            
                                <p class="form-group">
                                    <label for="email">{l s='Your Email'}<sup class="required">*</sup></label>                    
                                        <input class="form-control grey validate" type="text" id="email" name="from" data-validate="isEmail" value="{$email|escape:'html':'UTF-8'}" />
                                </p>
				
                                <div class="form-group">
                                    <label for="message">{l s='Your Address' mod='productinquiry'}<sup class="required">*</sup></label>
                                    <textarea class="form-control" id="message" name="message">{if isset($message)}{$message|escape:'html':'UTF-8'|stripslashes}{/if}</textarea>
                                </div>

				{if $allow_guests == true && !$logged}
				<label>{l s='Your name' mod='productinquiry'}<sup class="required">*</sup></label>
				<input id="inquiryCustomerName" name="customer_name" type="text" value=""/>
				{/if}

				<div id="new_inquiry_form_footer">
					<p class="fl required"><sup>*</sup> {l s='Required fields' mod='productinquiry'}</p>
					<p class="fr">
                                            <button class="btn" id="submitNewInquiry" name="submitMessage" type="submit" value="1">{l s='Send' mod='productinquiry'}</button>&nbsp;
						{l s='or' mod='productinquiry'}&nbsp;<a href="#" onclick="$.fancybox.close();">{l s='Cancel' mod='productinquiry'}</a>
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
            </fieldset>                            
		</form><!-- /end new_inquiry_form_content -->
	</div>
</div>
<!-- End fancybox -->
{strip}
{addJsDef productinquiry_controller_url=$productinquiry_controller_url|@addcslashes:'\''}
{addJsDef productinquiry_url_rewrite=$productinquiry_url_rewriting_activated|boolval}
{addJsDef secure_key=$secure_key}

{addJsDefL name=productcomment_added}{l s='Your comment has been added!' mod='productinquiry' js=1}{/addJsDefL}
{addJsDefL name=productcomment_added_moderation}{l s='Your comment has been added and will be available once approved by a moderator' mod='productinquiry' js=1}{/addJsDefL}
{addJsDefL name=productinquiry_title}{l s='Thank you!' mod='productinquiry' js=1}{/addJsDefL}
{addJsDefL name=productinquiry_ok}{l s='OK' mod='productinquiry' js=1}{/addJsDefL}
{/strip}
<!--  /Module ProductComments -->
                                
