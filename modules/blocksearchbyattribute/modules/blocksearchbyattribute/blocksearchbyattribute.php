<?php
/*
*
* @author Grzegorz Kowalski alias "Tarentil" (Grzegorz.Kowalski@wit.edu.pl, tarentil@tlen.pl)
* @date 2009-06-03
* @license http://www.opensource.org/licenses/osl-3.0.php Open-source licence 3.0
* @version 1.2
*
* Changes:
*	1.0 - Searching by one attribute only at time
*	1.1 - Searching by many attributes at once
*	1.2 - Proper counting of search results, sorting and pagination
*/
class blocksearchbyattribute extends Module
{
	function __construct()
	{
		$this->name = 'blocksearchbyattribute';
		$this->tab = 'Blocks';
		$this->version = 1.0;

		parent::__construct();

		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Block Search-by-Attribute');
		$this->description = $this->l('Add a search block with comboxes containing attributes.');
	}

	function install()
	{
		if (!parent::install())
			return false;
		if (!$this->registerHook('leftColumn'))
			return false;
		return true;
	}
	
	function hookRightColumn($params)
	{
		return $this->hookLeftColumn($params);
	}
	
	/**
	* Returns module content
	*
	* @param array $params Parameters
	* @return string Content
	*/
	function hookLeftColumn($params)
	{
		global $cookie;
		global $smarty;
		$attributeGroups = AttributeGroup::getAttributesGroups(intval($cookie->id_lang));
		if (!sizeof($attributeGroups) || $attributeGroups === false) {
			return '';
		}

		$text = '';

		$counter = 0;

		foreach ($attributeGroups AS $group) {
			$attributes = AttributeGroup::getAttributes(intval($cookie->id_lang), $group['id_attribute_group']);			
			if (!sizeof($attributes) || $attributes === false) {
				continue;
			}

			$id_name = '_sba_' . $group['id_attribute_group'];

			$text = $text . '<br><select class="nav_input" style="width:150px; float:left;" name="id' . $counter . '" id="id'. $counter .'">';
			$text = $text . '<option id="1zz" value="0">' . $group['public_name'] . '</option>';
			
			
			
			foreach ($attributes AS $attribute) 
			{
				if(AttributeGroup::checkifproductinattribute($attribute['id_attribute']))
				{
					if($_GET['id'.$counter ] == $attribute['id_attribute'] )
					{
						$text = $text . '<option selected value="' . $attribute['id_attribute'] . '">' . $attribute['name'] . '</option>';
					}
					else
					{
						$text = $text . '<option value="' . $attribute['id_attribute'] . '">' . $attribute['name'] . '</option>';
					}
					
				}
			}

			$text = $text . '</select>';
			if($_GET['id'.$counter ] <> 0 )
			{
				$text = $text . '  <a href=javascript:function1("id' . $counter . '");>X</a><br>';
			}
			else
			{
				$text = $text . '<br>';
			}
			
			$counter++;
		}

		$text = $text . '<input type="hidden" name="attribute_id" value="' . $counter . '">';

		$smarty->assign('text', $text);
		return $this->display(__FILE__, 'blocksearchbyattribute.tpl');
	}

}
?>