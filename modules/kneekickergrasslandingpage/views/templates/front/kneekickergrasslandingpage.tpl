<div class="landingPage col-md-12">
	<div class="landingPageHeader">
		<div class="landingPageHeaderContent">
			<h1>Artificial Grass Cost</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, numquam, dolore. Sit est fuga provident totam debitis veniam placeat in dolore illo, vitae nesciunt. Neque ut incidunt officia, laboriosam aliquam.</p>
		</div>
	</div>
</div>
<div class="landingPage col-md-12">
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickergrasslandingpage/img/grass.jpg" title="Artificial Grass" />
		</div>
		<div class="col-md-6 landingPageContent">
			<h3>Use it at Home...</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
	</div>
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageContent">
			<h3>Use it commercially...</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickergrasslandingpage/img/grass.jpg" title="Artificial Grass" />
		</div>
	</div>
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickergrasslandingpage/img/grass.jpg" title="Artificial Grass" />
		</div>		
		<div class="col-md-6 landingPageContent">
			<h3>Installation</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
	</div>
</div>
<div class="landingPage col-md-12">
	<div class="landingPageFooter">
		<div class="landingPageFooterContent">
			<h1>What our customers think</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, numquam, dolore. Sit est fuga provident totam debitis veniam placeat in dolore illo, vitae nesciunt. Neque ut incidunt officia, laboriosam aliquam.</p>
		</div>
	</div>
</div>