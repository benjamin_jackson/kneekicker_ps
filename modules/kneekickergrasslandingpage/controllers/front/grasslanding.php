<?php

/*
 *
 * Custom Landing Page class written for KneeKicker.
 *
 * Predict Marketing
 * Ben Jackson
 */

class kneeKickerGrassLandingPageGrasslandingModuleFrontController extends ModuleFrontController {


        /**
         * Lets set up out template
         *
         */
        public function initContent()
        {
                parent::initContent();
                $this->setTemplate('kneekickergrasslandingpage.tpl');
        }

        public function setMedia()
        {
                parent::setMedia();
                $this->addCSS(__PS_BASE_URI__.'modules/'.$this->module->name.'/css/'.$this->module->name.'.css');
                $this->addJS(__PS_BASE_URI__.'modules/'.$this->module->name.'/js/'.$this->module->name.'.js');
        }



}