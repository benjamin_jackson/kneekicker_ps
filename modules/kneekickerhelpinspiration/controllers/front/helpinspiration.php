<?php

/*
 *
 * Help and Inspiration Page.
 *
 * Predict Marketing
 * Ben Jackson
 */

class kneeKickerHelpAndInspirationHelpInspirationModuleFrontController extends ModuleFrontController {


        /**
         * Lets set up out template
         *
         */
        public function initContent()
        {
                parent::initContent();
                $this->setTemplate('kneekickerhelpandinspiration.tpl');
        }

        public function setMedia()
        {
                parent::setMedia();
                $this->addCSS(__PS_BASE_URI__.'modules/'.$this->module->name.'/css/'.$this->module->name.'.css');
        }


}