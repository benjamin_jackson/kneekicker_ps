<div class="landingPage col-md-12">
	<div class="landingPageHeader">
		<div class="landingPageHeaderContent">
			<h1>Why Underlay Can Save You Money</h1>
		</div>
	</div>
</div>
<div class="landingPage col-md-12">
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickergrasslandingpage/img/grass.jpg" title="Artificial Grass" />
		</div>
		<div class="col-md-6 landingPageContent">
			<h3>A Common Problem</h3>
			<p>There's no beating the feeling of luxury and comfort experienced when walking across a high quality carpet. Unfortunately these enjoyable sensations come at a fairly high price. You could pay as much as £100 per square foot of woollen carpet. Thankfully there is a relatively inexpensive solution. It's possible to greatly enhance the comfort of a cheap carpet by fitting protective underlay. 
This method can be used when fitting budget-friendly polyester or acrylic carpets. They won't have the same level of durability as more expensive varieties. However, there'll be little reason to worry about the accumulation of stains and debris.</p>
		</div>
	</div>
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageContent">
			<h3>The Ideal Solution</h3>
			<p>You may be surprised to discover that there are various types of underlay. The aptly named waffle underlay features air pockets for added shock absorption and comfort. It is ideal for hallways and sitting rooms. There's also the option of hard-wearing flat underlay. If you're keen to achieve the greatest feeling of comfort and resistance to everyday wear and tear then it would be worth paying a little extra for high-quality underlay. Rubber underlay is highly recommended because of it's luxurious feel and extreme dust-resistance.</p>
		</div>
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickergrasslandingpage/img/grass.jpg" title="Artificial Grass" />
		</div>
	</div>
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickergrasslandingpage/img/grass.jpg" title="Artificial Grass" />
		</div>		
		<div class="col-md-6 landingPageContent">
			<h3>Notable Benefits</h3>
			<p>We've already mentioned that underlay is a useful means of soaking up the pressure that is exerted when people walk through your home or office. The protective layer will ensure that your carpet retains it's optimum appearance for a good amount of time. So you'll be able to save up for a carpet which really transforms the appearance of your interior space. Underlay also minimises the amount of noise and disturbance in high traffic areas. There's a good chance that you'll be persuaded by the many benefits of underlay. However, you're more than welcome to give us for further information about your buying options.</p>
		</div>
	</div>
</div>
<div class="landingPage col-md-12">
	<div class="landingPageFooter">
		<div class="landingPageFooterContent">
			<h1>What our customers think</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, numquam, dolore. Sit est fuga provident totam debitis veniam placeat in dolore illo, vitae nesciunt. Neque ut incidunt officia, laboriosam aliquam.</p>
		</div>
	</div>
</div>