<?php

if (!defined('_PS_VERSION_'))
	exit;

class PPBSProductUnit extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_ppbs_unit;

	/** @var integer Product ID */
	public $id_product;

	/** @var integer Shop ID */
	public $id_shop;

	/** @var integer Visible */
	public $visible;

	/** @var integer Min val */
	public $min;

	/** @var integer Max val */
	public $max;

	/** @var string default value/text */
	public $default;

	/** @var string Input type */
	public $input_type;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ppbs_product_unit',
		'primary' => 'id_ppbs_unit',
		'fields' => array(
			'id_product' => array(
				'type' => self::TYPE_INT,
			),
			'id_shop' => array(
				'type' => self::TYPE_INT,
			),
			'visible' => array(
				'type' => self::TYPE_INT,
			),
			'min' => array(
				'type' => self::TYPE_FLOAT,
			),
			'max' => array(
				'type' => self::TYPE_FLOAT,
			),
			'default' => array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 255,
				'required' => true
			),
			'input_type' => array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 255,
				'required' => true
			)
		)
	);

}
