<?php

if (!defined('_PS_VERSION_'))
	exit;

class PPBSUnit extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_ppbs_unit;

	/** @var string Dimension Name */
	public $name;

	/** @var string symbol position */
	public $symbol;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ppbs_unit',
		'primary' => 'id_ppbs_unit',
		'multilang' => true,
		'fields' => array(
			'name'  =>	array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 255,
				'required' => true
			),
			'symbol' =>	array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 64,
				'required' => true,
				'lang' => true
			)
		)
	);

	public static function getUnits()
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ppbs_unit', 'u');
		return Db::getInstance()->executeS($sql);
	}

	public function getByName($name)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ppbs_unit');
		$sql->where('name LIKE "'.pSQL($name).'"');
		$row = Db::getInstance()->getRow($sql);
		if (is_array($row))
			$this->hydrate($row);
	}

};