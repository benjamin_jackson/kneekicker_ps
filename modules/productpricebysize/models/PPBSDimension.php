<?php

if (!defined('_PS_VERSION_'))
	exit;

class PPBSDimension extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_ppbs_dimension;

	/** @var integer Shop ID */
	public $id_shop;

	/** @var string Dimension Name */
	public $name;

	/** @var string Display name */
	public $display_name;

	/** @var integer display position */
	public $position;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ppbs_dimension',
		'primary' => 'id_ppbs_dimension',
		'multilang' => true,
		'fields' => array(
			'id_shop'     =>	array(
				'type' => self::TYPE_INT,
			),
			'name'     =>	array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 32,
				'required' => true
			),
			'display_name'  =>	array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 32,
				'required' => true,
				'lang' => true
			),
			'position' => array(
				'type' => self::TYPE_INT
			)
		)
	);

	public static function get_dimension_definition($id_dimension, $id_lang = 1)
	{
		$sql = '
			SELECT
				ppbs_d.id_ppbs_dimension,
				ppbs_d.name,
				ppbs_d.position,
				ppbs_dl.display_name
			FROM `'._DB_PREFIX_.'ppbs_dimension` ppbs_d
			JOIN `'._DB_PREFIX_.'ppbs_dimension_lang` ppbs_dl ON (ppbs_d.id_ppbs_dimension = ppbs_dl.id_ppbs_dimension)
			WHERE ppbs_dl.`id_lang` = '.(int)$id_lang.'
			AND ppbs_d.id_ppbs_unit = '.(int)$id_dimension.'
			';
		$unit = Db::getInstance()->executeS($sql);
		if (is_array($unit)) return $unit[0];
		else return false;
	}

	public static function getDimensions($id_lang = 1, $id_shop = 1)
	{
		$sql = '
			SELECT
				ppbs_d.id_ppbs_dimension,
				ppbs_d.name,
				ppbs_d.position,
				ppbs_d.id_shop,
				ppbs_dl.display_name
			FROM `'._DB_PREFIX_.'ppbs_dimension` ppbs_d
			JOIN `'._DB_PREFIX_.'ppbs_dimension_lang` ppbs_dl ON (ppbs_d.id_ppbs_dimension = ppbs_dl.id_ppbs_dimension)
			WHERE ppbs_dl.`id_lang` = '.(int)$id_lang.'
			AND ppbs_d.id_shop = '.(int)$id_shop.'
			ORDER BY ppbs_d.position ASC';
		$units = Db::getInstance()->executeS($sql);
		return $units;
	}

	public static function getDimensionsList($id_lang = 1, $id_shop = null)
	{
		$dimensions_collection = array(); //of TPPBSUnit
		$dimensions = self::getDimensions($id_lang, $id_shop);

		if (is_array($dimensions))
		{
			foreach ($dimensions as $row)
			{
				$dimension = new TPPBSDimension();
				$dimension->id_ppbs_unit = $row['id_ppbs_unit'];
				$dimension->display_name = $row['display_name'];
				$dimension->name = $row['name'];
				$dimension->position = $row['position'];
				$dimension->translations = array();
				$dimensions_collection[] = $row;
			}
		}
		return $dimensions_collection;
	}

	public function getByName($name)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ppbs_dimension');
		$sql->where('name LIKE "'.pSQL($name).'"');
		$row = Db::getInstance()->getRow($sql);
		$this->hydrate($row);
	}

};