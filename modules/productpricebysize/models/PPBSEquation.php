<?php

if (!defined('_PS_VERSION_'))
	exit;

class PPBSEquation extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_equation;

	/** @var integer Product ID */
	public $id_product;

	/** @var integer Combination ID */
	public $ipa;

	/** @var string Equation */
	public $equation;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ppbs_equation',
		'primary' => 'id_equation',
		'fields' => array(
			'id_product' => array('type' => self::TYPE_INT, 'required' => true),
			'ipa' => array('type' => self::TYPE_INT, 'required' => true),
			'equation' => array('type' => self::TYPE_STRING)
		)
	);

	public function getByProduct($id_product, $ipa, $fallback = false)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ppbs_equation');
		$sql->where('id_product = '.(int)$id_product);
		$sql->where('ipa= '.(int)$ipa);
		$row = Db::getInstance()->getRow($sql);

		if ((empty($row) || $row['equation'] == '') && $ipa > 0)
			$this->getByProduct($id_product, 0);

		if (!$row) $row = array();
		$this->hydrate($row);
	}

	public function getAllByProduct($id_product)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ppbs_equation');
		$sql->where('id_product = '.(int)$id_product);
		$results = Db::getInstance()->executeS($sql);
		if (!$results) $results = array();
		return $this->hydrateCollection('PPBSEquation', $results);
	}

};