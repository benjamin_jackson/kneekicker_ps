<?php

if (!defined('_PS_VERSION_'))
	exit;

class PPBSProduct extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_ppbs_product;

	/** @var integer Product ID */
	public $id_product;

	/** @var integer Shop ID */
	public $id_shop;

	/** @var boolean enabled val */
	public $enabled = 0;

	/** @var boolean Front Conversion enabled val */
	public $front_conversion_enabled = 0;

	/** @var string Front Conversion Operator val */
	public $front_conversion_operator = '';

	/** @var boolean Front Conversion Value val */
	public $front_conversion_value = 0;

	/** @var boolean Attribute Price as Area Price val */
	public $attribute_price_as_area_price = 0;

	/** @var float Min Price val */
	public $min_price = 0;

	/** @var float Prduct Setup Fee val */
	public $setup_fee = 0;

	/** @var Int Equation Enabled val */
	public $equation_enabled = 0;

	/** @var Str Equation val */
	public $equation = '';

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ppbs_product',
		'primary' => 'id_ppbs_product',
		'fields' => array(
			'id_product' => array('type' => self::TYPE_INT, 'required' => true),
			'id_shop' => array('type' => self::TYPE_INT, 'required' => true),
			'enabled' => array('type' => self::TYPE_INT),
			'front_conversion_enabled' => array('type' => self::TYPE_INT),
			'front_conversion_operator' => array('type' => self::TYPE_STRING),
			'front_conversion_value' => array('type' => self::TYPE_INT),
			'attribute_price_as_area_price' => array('type' => self::TYPE_INT),
			'min_price' => array('type' => self::TYPE_FLOAT),
			'setup_fee' => array('type' => self::TYPE_FLOAT),
			'equation_enabled' => array('type' => self::TYPE_INT),
			'equation' => array('type' => self::TYPE_STRING),
		)
	);

	public function getByProduct($id_product, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ppbs_product', 'p');
		$sql->where('p.id_product = '.(int)$id_product);
		$sql->where('p.id_shop = '.(int)$id_shop);
		$row = Db::getInstance()->getRow($sql);

		if (!empty($row['id_ppbs_product']))
		{
			$this->id = $row['id_ppbs_product'];
			$this->id_ppbs_product = $row['id_ppbs_product'];
			$this->id_shop = $row['id_shop'];
			$this->id_product = $row['id_product'];
			$this->enabled = $row['enabled'];
			$this->attribute_price_as_area_price = $row['attribute_price_as_area_price'];
			$this->front_conversion_enabled = $row['front_conversion_enabled'];
			$this->front_conversion_operator = $row['front_conversion_operator'];
			$this->front_conversion_value = $row['front_conversion_value'];
			$this->min_price = $row['min_price'];
			$this->setup_fee = $row['setup_fee'];
			$this->equation_enabled = $row['equation_enabled'];
			$this->equation = $row['equation'];
		}
	}

}
