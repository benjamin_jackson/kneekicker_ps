<?php

class PPBSInstall extends PPBSInstallCore
{

	public static function install_db()
	{
		$return = true;
		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_."ppbs_area_price` (
			  `id_area_price` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `id_shop` int(10) unsigned DEFAULT '1',
			  `id_product` int(10) unsigned DEFAULT NULL,
			  `area_low` decimal(10,2) unsigned DEFAULT '0.00',
			  `area_high` decimal(10,2) unsigned DEFAULT '0.00',
			  `impact` char(6) DEFAULT '+',
			  `price` decimal(10,6) unsigned DEFAULT '0.000000',
			  `weight` decimal(10,6) unsigned DEFAULT '0.000000',
			  PRIMARY KEY (`id_area_price`)
		)ENGINE="._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_."ppbs_dimension` (
			  `id_ppbs_dimension` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `id_shop` mediumint(8) unsigned NOT NULL DEFAULT '1',
			  `name` varchar(32) NOT NULL,
			  `position` int(10) unsigned NOT NULL DEFAULT '0',
			  PRIMARY KEY (`id_ppbs_dimension`)
		)ENGINE="._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ppbs_dimension_lang` (
		  `id_ppbs_dimension` int(10) unsigned NOT NULL,
		  `id_lang` int(10) unsigned NOT NULL,
		  `display_name` varchar(128) NOT NULL,
		  PRIMARY KEY (`id_ppbs_dimension`,`id_lang`)
		)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_."ppbs_product` (
			`id_ppbs_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`id_product` int(10) unsigned NOT NULL,
			`id_shop` int(10) unsigned NOT NULL,
			`enabled` tinyint(3) unsigned NOT NULL,
			`front_conversion_enabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
			`front_conversion_operator` varchar(3) NOT NULL,
			`front_conversion_value` decimal(15,2) NOT NULL,
			`attribute_price_as_area_price` tinyint(3) NOT NULL DEFAULT '0',
			`min_price` decimal(15,2) NOT NULL DEFAULT '0.00',
			`setup_fee` decimal(15,2) NOT NULL DEFAULT '0.00',
			`equation_enabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
			`equation` varchar(512) NOT NULL,
			PRIMARY KEY (`id_ppbs_product`)
		)ENGINE="._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_."ppbs_product_field` (
			`id_ppbs_product_field` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`id_ppbs_dimension` int(10) unsigned NOT NULL,
			`id_product` int(10) unsigned NOT NULL,
			`id_shop` int(10) unsigned NOT NULL,
			`id_ppbs_unit` tinyint(3) unsigned NOT NULL DEFAULT '0',
			`min` int(11) NOT NULL,
			`max` int(11) NOT NULL,
			`default` varchar(16) NOT NULL DEFAULT '0',
			`input_type` varchar(12) NOT NULL DEFAULT 'textbox',
			`visible` tinyint(3) unsigned NOT NULL,
			`position` smallint(5) unsigned NOT NULL DEFAULT '0',
			PRIMARY KEY (`id_ppbs_product_field`)
		)ENGINE="._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_."ppbs_product_field_option` (
			`id_option` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`id_ppbs_product_field` int(10) unsigned NOT NULL,
			`text` varchar(255) NOT NULL,
			`value` varchar(255) NOT NULL,
			`position` mediumint(8) unsigned NOT NULL DEFAULT '0',
			PRIMARY KEY (`id_option`)
		)ENGINE="._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ppbs_product_field_ratio` (
			`id_ppbs_unit` int(10) unsigned NOT NULL,
			`id_product` int(10) unsigned NOT NULL,
			`id_shop` int(10) unsigned NOT NULL,
			`ratio` decimal(10,2) NOT NULL,
			PRIMARY KEY (`id_ppbs_unit`,`id_product`,`id_shop`)
		)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ppbs_translations` (
			`id_translation` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`id_language` int(10) unsigned NOT NULL,
			`id_shop` int(10) unsigned NOT NULL,
			`name` varchar(255) NOT NULL,
			`text` text NOT NULL,
			PRIMARY KEY (`id_translation`)
		)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ppbs_unit` (
			`id_ppbs_unit` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`name` varchar(255) NOT NULL,
			`symbol` varchar(64) NOT NULL,
			PRIMARY KEY (`id_ppbs_unit`)
		)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ppbs_unit_lang` (
			`id_ppbs_unit` int(10) unsigned NOT NULL,
			`id_lang` smallint(5) unsigned NOT NULL,
			`symbol` varchar(32) NOT NULL,
			PRIMARY KEY (`id_ppbs_unit`,`id_lang`)
		)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ppbs_equation` (
			`id_equation` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
			`id_product` int(10) unsigned NOT NULL,
			`ipa` int(10) unsigned NOT NULL,
			`equation` varchar(512) NOT NULL,
			PRIMARY KEY (`id_equation`)
		)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		self::addColumn('cart_product', 'ppbs', 'SMALLINT UNSIGNED DEFAULT 0');
		self::addColumn('customized_data', 'ppbs_dimensions', 'TEXT');
		self::addColumn('customized_data', 'ppbs_roomshape', 'TEXT');
		self::addColumn('customization_field', 'ppbs', 'tinyint(1) UNSIGNED NOT NULL');
	}

	public static function uninstall()
	{
		self::dropTable('ppbs_area_price');
		self::dropTable('ppbs_dimension');
		self::dropTable('ppbs_dimension_lang');
		self::dropTable('ppbs_product');
		self::dropTable('ppbs_product_field');
		self::dropTable('ppbs_product_field_option');
		self::dropTable('ppbs_product_field_ratio');
		self::dropTable('ppbs_translations');
		self::dropTable('ppbs_unit');
		self::dropTable('ppbs_unit_lang');
	}

	public static function addColumn($table, $name, $type)
	{
		try
		{
			$return = Db::getInstance()->execute('ALTER TABLE  `'._DB_PREFIX_.bqSQL($table).'` ADD  `'.bqSQL($name).'` '.bqSQL($type));
		} catch(Exception $e)
		{
			$return = true;
		}
	}

	private static function _add_translation($name, $text, $id_lang, $id_shop)
	{
		$sql = 'SELECT COUNT(*) AS total_count
				FROM '._DB_PREFIX_.'ppbs_translations
				WHERE id_shop = '.(int)$id_shop.'
				AND id_language = '.(int)$id_lang.'
				AND name LIKE "'.pSQL($name).'"
				';
		$result = Db::getInstance()->executeS($sql);
		if ($result && $result[0]['total_count'] == 0)
		{
			Db::getInstance()->insert('ppbs_translations', array(
				'id_shop' => (int)$id_shop,
				'id_language' => (int)$id_lang,
				'name' => pSQL($name),
				'text' => pSQL($text),
			));
		}
	}

	private static function _addUnit($name, $symbol, $id_lang)
	{
		$ppbs_unit = new PPBSUnit();
		$ppbs_unit->getByName($name);

		if (empty($ppbs_unit->id_ppbs_unit))
		{
			$ppbs_unit->name = pSQL($name);
			$ppbs_unit->symbol[$id_lang] = pSQL($symbol);
			$ppbs_unit->add(false);
		}
	}

	private static function _addDimension($name, $display_name)
	{
		$ppbs_dimension = new PPBSDimension();
		$languages = Language::getLanguages();

		if (empty($ppbs_dimension->id_ppbs_dimension))
		{
			$ppbs_dimension->name = pSQL($name);
			$ppbs_dimension->id_shop = Context::getContext()->shop->id;
			foreach ($languages as $language)
				$ppbs_dimension->display_name[$language['id_lang']] = pSQL($display_name);
			$ppbs_dimension->add(false);
		}
	}

	public static function install_data()
	{
		/* Install global module translations */
		$languages = Language::getLanguages();
		$shops = ShopCore::getCompleteListOfShopsID();
		foreach ($shops as $id_shop)
		{
			foreach ($languages as $language)
			{
				self::_add_translation('min_max_error', '{min} - {max}', $language['id_lang'], $id_shop);
				self::_add_translation('generic_error', 'check dimensions above', $language['id_lang'], $id_shop);
				self::_add_translation('unit_price_suffix', 'per m2', $language['id_lang'], $id_shop);
			}
		}

		/* Add the Generic Dimensions */
		self::_addDimension('height', 'Height');
		self::_addDimension('width', 'Width');
		self::_addDimension('depth', 'Depth');

		/* Install sample dimensions */
		foreach ($shops as $id_shop)
		{
			foreach ($languages as $language)
			{
				self::_addUnit('centimeter', 'cm', $language['id_lang']);
				self::_addUnit('millimeter', 'mm', $language['id_lang']);
				self::_addUnit('meter', 'm', $language['id_lang']);
				self::_addUnit('inch', '"', $language['id_lang']);
				self::_addUnit('foot', 'ft', $language['id_lang']);
			}
		}
		return true;
	}
}