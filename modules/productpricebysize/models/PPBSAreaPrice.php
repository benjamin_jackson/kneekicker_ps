<?php

if (!defined('_PS_VERSION_'))
	exit;

class PPBSAreaPrice extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_area_price;

	/** @var integer Product ID */
	public $id_product;

	/** @var integer Shop ID */
	public $id_shop;

	/** @var float Area_Low val */
	public $area_low;

	/** @var float Area_Low val */
	public $area_high;

	/** @var string Impact val */
	public $impact;

	/** @var float Price val */
	public $price;

	/** @var float Weight val */
	public $weight;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ppbs_area_price',
		'primary' => 'id_area_price',
		'fields' => array(
			'id_product' => array(
				'type' => self::TYPE_INT,
				'required' => true
			),
			'id_shop' => array(
				'type' => self::TYPE_INT,
				'required' => true
			),
			'area_low' => array(
				'type' => self::TYPE_FLOAT,
				'required' => true
			),
			'area_high' => array(
				'type' => self::TYPE_FLOAT,
				'required' => true
			),
			'impact' => array(
				'type' => self::TYPE_STRING,
				'required' => true
			),
			'price' => array(
				'type' => self::TYPE_FLOAT
			),
			'weight' => array(
				'type' => self::TYPE_FLOAT
			)
		)
	);

	public static function getCollectionByProduct($id_product, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('
			ap.id_area_price,
			ap.id_product,
			ap.id_shop,
			ap.area_low,
			ap.area_high,
			ap.impact,
			ap.price,
			ap.weight
		');
		$sql->from('ppbs_area_price', 'ap');
		$sql->where('ap.id_product = '.(int)$id_product);
		$sql->where('ap.id_shop = '.(int)$id_shop);
		$sql->orderBy('ap.area_low');
		return Db::getInstance()->executeS($sql);
	}


}
