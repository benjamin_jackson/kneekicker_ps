<?php

if (!defined('_PS_VERSION_'))
	exit;

class PPBSProductFieldOption extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_option;


	/** @var integer Product Unit ID */
	public $id_ppbs_product_field;

	/** @var string Option text */
	public $text;

	/** @var string Option value */
	public $value;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ppbs_product_field_option',
		'primary' => 'id_option',
		'fields' => array(
			'id_ppbs_product_field' => array(
				'type' => self::TYPE_INT,
			),
			'text' => array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 255,
				'required' => true
			),
			'value' => array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 255,
				'required' => true
			)
		)
	);

	public static function deleteAllByProductFieldID($id_ppbs_product_field)
	{
		DB::getInstance()->delete(self::$definition['table'], 'id_ppbs_product_field='.(int)$id_ppbs_product_field);
	}

	public static function getFieldOptions($id_ppbs_product_field)
	{
		$sql = new DbQuery();
		$sql->select('value, text');
		$sql->from(self::$definition['table']);
		$sql->where('id_ppbs_product_field = '.(int)$id_ppbs_product_field);
		$sql->orderBy('position');
		$result = Db::getInstance()->executeS($sql);
		return $result;
	}


}
