<?php

class PPBSModel {

	protected static $_pricesPPBS;

	private static function do_encoding($matches)
	{
		return mb_convert_encoding(pack('H*', $matches[1]), 'UTF-8', 'UTF-16');
	}

	public static function RawJsonEncode($input)
	{
		return preg_replace_callback(
			'/\\\\u([0-9a-zA-Z]{4})/',
			array('PPBSModel', 'do_encoding'),
			Tools::jsonEncode($input)
		);
	}


	/**
	 * @return array of TPPBSCartUnit
	 */
	public static function get_cart_product_units($id_product, $id_cart, $ipa, $id_shop = 1)
	{
		$cart_unit_collection = array();

		if ($id_product == '') return false;
		if ($id_cart == '') return false;
		
		$sql = 'SELECT
					ppbs_dimensions,
					ppbs_roomshape,
					quantity
				FROM '._DB_PREFIX_.'customized_data cd
				INNER JOIN '._DB_PREFIX_.'customization c ON cd.id_customization = c.id_customization
				WHERE 
				    c.id_cart = '.(int)$id_cart.'
				    AND c.id_product = '.(int)$id_product.'
				    AND c.id_product_attribute = '.(int)$ipa;
		$rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		if (is_array($rows) && count($rows) > 0)
			return $rows;
		else return false;

	}

	public static function getIDCustomizationField($id_product)
	{
		$sql = 'SELECT id_customization_field
				FROM '._DB_PREFIX_.'customization_field
				WHERE id_product = '.(int)$id_product.'
				AND ppbs = 1';
		$row = DB::getInstance()->getRow($sql);
		if (isset($row['id_customization_field']))
			return $row['id_customization_field'];
		else return 0;
	}


	/**
	 * @return TPBBSProductCustomization
	 */
	public static function getProductCustomization($id_product, $ipa, $id_cart, $id_customization, $return_raw = false)
	{
		$sql = new DbQuery();
		$sql->select('
			c.quantity,
			cf.ppbs
		');
		$sql->from('customization', 'c');
		$sql->leftJoin('customized_data', 'cd', 'c.`id_customization` = cd.`id_customization`');
		$sql->leftJoin('customization_field', 'cf', 'cd.`index` = cf.`id_customization_field`');
		$sql->where('c.id_cart = '.(int)$id_cart);
		$sql->where('c.id_product = '.(int)$id_product);
		$sql->where('c.id_product_attribute = '.(int)$ipa);
		$sql->where('c.id_customization = '.(int)$id_customization);
		$row = Db::getInstance()->getRow($sql);

		if (!$return_raw && $row)
		{
			$ppbs_product_customization = new TPBBSProductCustomization();
			$ppbs_product_customization->ppbs = $row['ppbs'];
			$ppbs_product_customization->quantity = $row['quantity'];
			return $ppbs_product_customization;
		}
		return $row;
	}

	public static function getAllProductCustomizations($id_product, $ipa, $id_cart)
	{
		$sql = new DbQuery();
		$sql->select('
			c.quantity,
			cf.ppbs,
			cd.value
		');
		$sql->from('customization', 'c');
		$sql->leftJoin('customized_data', 'cd', 'c.`id_customization` = cd.`id_customization`');
		$sql->leftJoin('customization_field', 'cf', 'cd.`index` = cf.`id_customization_field`');
		$sql->where('c.id_cart = '.(int)$id_cart);
		$sql->where('c.id_product = '.(int)$id_product);
		$sql->where('c.id_product_attribute = '.(int)$ipa);
		$results = Db::getInstance()->executeS($sql);

		if ($results)
			return $results;
		else
			return false;
	}

	public static function updateCartProduct($id_product, $id_cart, $ipa, $id_address_delivery, $cart_unit_collection, $roomShape, $quantity, $id_shop = 1, $awp_vars = '')
	{
		if (!$cart_unit_collection) return false;
		if ($id_product == '') return false;
		if ($id_cart == '') return false;
		if ($roomShape == '') return false;

		Db::getInstance()->insert('customization', array(
			'id_product_attribute' => (int)$ipa,
			'id_cart' => (int)$id_cart,
			'id_product' => (int)$id_product,
			'id_address_delivery' => (int)$id_address_delivery,
			'quantity' => (int)$quantity,
			'in_cart' => 1,
		));

		$id_customization = Db::getInstance()->Insert_ID();

		$value = '';
		if (is_array($cart_unit_collection))
			foreach ($cart_unit_collection as $cart_unit)
				$value .= $cart_unit->display_name.' : '.$cart_unit->value.' '.$cart_unit->symbol.'. ';

		Db::getInstance()->insert('customized_data', array(
			'id_customization' => (int)$id_customization,
			'type' => 1,
			'index' => (int)self::getIDCustomizationField($id_product),
			'value' => pSQL($value),
			'ppbs_dimensions' => pSQL(self::RawJsonEncode($cart_unit_collection), true),
			'ppbs_roomshape' => pSQL($roomShape)
		));

		Db::getInstance()->update('cart_product', array(
				'quantity' => 1,
				'ppbs' => 1
			),
			'id_cart='.(int)$id_cart.'
			AND id_product = '.(int)$id_product.'
			AND id_product_attribute = '.(int)$ipa.'
			AND id_shop = '.(int)$id_shop
		);
		return true;
	}

	/*
	 * Updates existing ppbs customization product's quantity (substuting PS default qty up / down functions)
	 */
	public static function updatCartProductQty($id_product, $id_cart, $ipa, $id_customization, $id_address_delivery, $quantity)
	{
		DB::getInstance()->update('customization', array(
			'quantity' => (int)$quantity
		),
			'id_product='.(int)$id_product.' AND
			id_product_attribute='.(int)$ipa.' AND
			id_address_delivery='.(int)$id_address_delivery.' AND
			id_cart='.(int)$id_cart.' AND
			id_customization='.(int)$id_customization
		);
		return '';
	}

	public static function getProductUnits($id_product, $id_lang = 1, $visible = true, $id_shop = null, $return_raw = false)
	{
		$product_units = array();

		$sql = 'SELECT
					u.id_ppbs_unit,
					u.name,
					u.position,
					ul.display_name,
					ul.suffix,
					pu.min,
					pu.max,
					pu.default,
					pu.input_type,
					pu.visible,
					ppur.ratio
				FROM '._DB_PREFIX_.'ppbs_product_unit pu
				INNER JOIN '._DB_PREFIX_.'ppbs_unit u ON pu.id_ppbs_unit = u.id_ppbs_unit
				INNER JOIN '._DB_PREFIX_."ppbs_unit_lang ul ON pu.id_ppbs_unit = ul.id_ppbs_unit AND ul.id_lang=$id_lang
				LEFT JOIN "._DB_PREFIX_.'ppbs_product_unit_ratios ppur ON pu.id_ppbs_unit = ppur.id_ppbs_unit AND ppur.id_product = '.(int)$id_product.'
				WHERE pu.id_product = '.(int)$id_product.'
				AND pu.id_shop = '.(int)$id_shop.'
				';
		if ($visible) $sql .= ' AND pu.visible = 1';

		$product_units_rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		if ($return_raw) return $product_units_rows;

		foreach ($product_units_rows as $key => $row)
		{
			$product_unit = new TPPBSProductUnit();
			$product_unit->id_ppbs_unit = $row['id_ppbs_unit'];
			$product_unit->name = $row['name'];
			$product_unit->display_name = $row['display_name'];
			$product_unit->suffix = $row['suffix'];
			$product_unit->min = $row['min'];
			$product_unit->max = $row['max'];
			$product_unit->position = $row['position'];
			$product_unit->visible = $row['visible'];
			$product_unit->ratio = $row['ratio'];
			$product_unit->default = $row['default'];
			$product_unit->input_type = $row['input_type'];
			$product_units[$product_unit->id_ppbs_unit] = $product_unit;
		}
		return $product_units;
	}

	public static function save($id_shop)
	{
		$id_customization_field = -1;

		if (_PS_VERSION_ < 1.6) $ps15_suffix = '_on';
		
		$sql = 'SELECT COUNT(id_product) AS total_count
				FROM '._DB_PREFIX_.'ppbs_product
				WHERE id_product='.(int)Tools::getValue('id_product').'
				AND id_shop = '.(int)$id_shop;
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		/* Insert */
		if ($result && $result[0]['total_count'] == 0)
		{
			$sql = 'INSERT INTO '._DB_PREFIX_.'ppbs_product (
						`id_product`,
						`id_shop`,
						`enabled`,
						`attribute_price_as_area_price`,
						min_price
			        ) VALUES (
			            '.(int)Tools::getValue('id_product').',
			            '.(int)$id_shop.',
			            '.(int)Tools::getValue('enabled'.$ps15_suffix).',
			            '.(int)Tools::getValue('attribute_price_as_area_price'.$ps15_suffix).',
			            '.(float)Tools::getValue('min_price').'
			        )
			';
			Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
		}
		else
		{
			Db::getInstance()->update('ppbs_product', array(
					'enabled' => (int)Tools::getValue('enabled'.$ps15_suffix),
					'attribute_price_as_area_price' => (int)Tools::getValue('attribute_price_as_area_price'.$ps15_suffix),
					'min_price' => (float)Tools::getValue('min_price')
				),
				'id_product='.(int)Tools::getValue('id_product').' AND id_shop='.(int)$id_shop
			);
		}

		/* Insert unit to product associations */
		/*$sql = 'DELETE FROM '._DB_PREFIX_.'ppbs_product_unit WHERE id_product='.(int)Tools::getValue('id_product').' AND id_shop='.(int)$id_shop;
		Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);*/
		
		$ppbs_units = PPBSUnit::getUnits(Context::getContext()->language->id, Context::getContext()->shop->id);
		if (is_array($ppbs_units))
		{
			foreach ($ppbs_units as $unit)
			{
				$id_ppbs_unit = $unit['id_ppbs_unit'];
				$sql = 'SELECT COUNT(*) AS total_count FROM '._DB_PREFIX_.'ppbs_product_unit
						WHERE id_ppbs_unit='.(int)$id_ppbs_unit.'
						AND id_product='.(int)Tools::getValue('id_product').'
						AND id_shop='.(int)$id_shop;
				$row = DB::getInstance()->getRow($sql);

				if ($row['total_count'] == 0)
				{
					$sql = 'INSERT INTO '._DB_PREFIX_.'ppbs_product_unit (
						id_ppbs_unit,
						id_product,
						id_shop,
						min,
						max,
						visible
					) VALUES (
						'.(int)$id_ppbs_unit.',
						'.(int)Tools::getValue('id_product').','.
						(int)$id_shop.',
						0,
						0,
						'.(int)Tools::getValue('unit_'.$id_ppbs_unit.$ps15_suffix).'
					)
					';
					Db::getInstance()->execute($sql);
				}
				else
					DB::getInstance()->update('ppbs_product_unit', array(
						'visible' => (int)Tools::getValue('unit_'.$id_ppbs_unit.$ps15_suffix)
					), 'id_product='.(int)Tools::getValue('id_product').' AND id_ppbs_unit='.(int)$id_ppbs_unit.' AND id_shop='.(int)$id_shop);
			}
		}

		/* Create / Update Product Customization data for PPBS */
		if (Tools::getValue('enabled'.$ps15_suffix) == 1)
			self::createProductCustomization(Tools::getValue('id_product'), true, $id_shop);
		else
			self::createProductCustomization(Tools::getValue('id_product'), false, $id_shop);
		return true;
	}

	/* creates the product customization entry for the product */
	public static function createProductCustomization($id_product, $ppbs_enabled, $id_shop)
	{
		if ($ppbs_enabled)
			Configuration::updateValue('PS_CUSTOMIZATION_FEATURE_ACTIVE', '1');

		$sql = 'SELECT id_customization_field FROM '._DB_PREFIX_.'customization_field
					WHERE id_product = '.(int)$id_product.'
					AND ppbs = 1';
		$row = DB::getInstance()->getRow($sql);
		if (isset($row['id_customization_field']))
		{
			$id_customization_field = $row['id_customization_field'];
			$sql = 'DELETE FROM '._DB_PREFIX_.'customization_field
					WHERE id_customization_field = '.(int)$id_customization_field;

			DB::getInstance()->execute($sql);

			$id_customization_field = $row['id_customization_field'];
			$sql = 'DELETE FROM '._DB_PREFIX_.'customization_field_lang
					WHERE id_customization_field = '.(int)$id_customization_field;
			DB::getInstance()->execute($sql);
		}

		if ($ppbs_enabled)
		{
			Db::getInstance()->insert('customization_field', array(
				'id_product' => (int)$id_product,
				'type' => 1,
				'required' => 0,
				'ppbs' => 1,
			));
			$id_customization_field_new = Db::getInstance()->Insert_ID();

			$languages = Language::getLanguages();

			foreach ($languages as $language)
			{
				Db::getInstance()->insert('customization_field_lang', array(
					'id_customization_field' => (int)$id_customization_field_new,
					'id_lang' => (int)$language['id_lang'],
					'name' => 'dimensions'
				));
			}

			Db::getInstance()->update('customized_data', array(
					'index' => (int)$id_customization_field_new
				),
				'`index`='.(int)$id_customization_field
			);
			DB::getInstance()->update('product',
				array(
					'customizable' => '1',
					'text_fields' => 1
				),
				'id_product = '.(int)$id_product
			);

			DB::getInstance()->update('product_shop',
				array(
					'customizable' => '1',
					'text_fields' => 1
				),
				'id_product = '.(int)$id_product.' AND id_shop='.(int)$id_shop
			);
		}
		else
		{
			DB::getInstance()->update('product',
				array(
					'customizable' => '0',
					'text_fields' => 1
				),
				'id_product = '.(int)$id_product
			);
		}
	}

	public static function saveRatios($id_lang, $id_shop)
	{
		$ppbs_units = PPBSUnit::getUnits($id_lang, $id_shop);
		DB::getInstance()->delete('ppbs_product_unit_ratios', 'id_shop='.(int)$id_shop.' AND id_product='.(int)(int)Tools::getValue('id_product'));

		if (is_array($ppbs_units))
		{
			foreach ($ppbs_units as $unit)
			{
				$id_ppbs_unit = $unit['id_ppbs_unit'];
				if (Tools::getValue('unit_ratio_'.$id_ppbs_unit) != '')
				{
					DB::getInstance()->insert('ppbs_product_unit_ratios', array(
						'id_ppbs_unit' => (int)$id_ppbs_unit,
						'id_product' => (int)Tools::getValue('id_product'),
						'id_shop' => (int)$id_shop,
						'ratio' => (float)Tools::getValue('unit_ratio_'.$id_ppbs_unit)
					));
				}
			}
		}
		return true;
	}


	public static function save_restraints($id_lang, $id_shop)
	{
		$ppbs_units = PPBSUnit::getUnits($id_lang, $id_shop);
		$arr_restraint_types = array('min', 'max');

		if (is_array($ppbs_units))
		{
			foreach ($ppbs_units as $unit)
			{
				$id_ppbs_unit = $unit['id_ppbs_unit'];
				foreach ($arr_restraint_types as $restraint_type)
				{
					if (Tools::getValue('unit_'.$restraint_type.'_'.$id_ppbs_unit) != '')
					{
						$sql = 'UPDATE '._DB_PREFIX_."ppbs_product_unit
							SET
								$restraint_type = ".Tools::getValue('unit_'.$restraint_type.'_'.$id_ppbs_unit).'
							WHERE id_ppbs_unit = '.(int)$id_ppbs_unit.'
							AND id_product = '.(int)Tools::getValue('id_product').'
							AND id_shop='.(int)$id_shop;
						Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
					}
				}
			}
		}
		return true;
	}

	public static function save_translations($ppbs_translation_array, $id_shop)
	{
		foreach ($ppbs_translation_array as $key => $ppbs_translation)
		{
			$name = $key;
			$sql = 'DELETE FROM '._DB_PREFIX_."ppbs_translations WHERE name LIKE '".pSQL($name)."' AND id_shop = ".(int)$id_shop;
			Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
			foreach ($ppbs_translation->text as $key2 => $text)
			{
				$id_language = $key2;
				Db::getInstance()->insert('ppbs_translations', array(
					'id_shop' => (int)$id_shop,
					'id_language' => (int)$id_language,
					'name' => pSQL($name),
					'text' => pSQL($text),
				));
			}
		}
	}

	public static function load_translations($id_shop = 1)
	{
		$sql = 'SELECT * FROM '._DB_PREFIX_.'ppbs_translations
		        WHERE id_shop = '.(int)$id_shop;
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		$ppbs_translation_collection = array();

		if (is_array($result) && count($result) > 0)
		{
			foreach ($result as $key => $row)
			{
				$ppbs_translation = new TPPBSTranslation();
				$ppbs_translation->text = $row['text'];
				$ppbs_translation->name = $row['name'];
				$ppbs_translation_collection[$row{'name'}][$row{'id_language'}] = $ppbs_translation;
			}
			return $ppbs_translation_collection;
		}
		else return false;
		

	}

	/**
	 * @return TPPBSProduct
	 */
	public static function Load_ppbs_product($id_product, $id_shop = 1)
	{
		if (!$id_product) return false;

		$sql = 'SELECT
					enabled,
		            front_conversion_enabled,
		            front_conversion_operator,
		            front_conversion_value,
		            attribute_price_as_area_price,
		            min_price
				FROM '._DB_PREFIX_.'ppbs_product
				WHERE id_product = '.(int)$id_product.'
				AND id_shop = '.(int)$id_shop.'
				';
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		if ($result && count($result) > 0)
		{
			$row = $result[0];
			$ppbs_product = new TPPBSProduct();
			$ppbs_product->enabled = (int)$row['enabled'];
			$ppbs_product->front_conversion_enabled = $row['front_conversion_enabled'];
			$ppbs_product->front_conversion_operator = $row['front_conversion_operator'];
			$ppbs_product->front_conversion_value = $row['front_conversion_value'];
			$ppbs_product->attribute_price_as_area_price = $row['attribute_price_as_area_price'];
			$ppbs_product->min_price = $row['min_price'];
			return $ppbs_product;
		}
		else return false;

	}

	public static function save_front_conversions($id_product, $front_conversion_enabled, $front_conversion_operator, $front_conversion_value, $id_shop = 1)
	{
		if (!$id_product) return false;

		$sql = 'UPDATE '._DB_PREFIX_.'ppbs_product
				SET
					front_conversion_enabled = '.(int)$front_conversion_enabled.",
					front_conversion_operator = '".pSQL($front_conversion_operator)."',
					front_conversion_value = ".pSQL($front_conversion_value).'
				WHERE id_product='.(int)$id_product.'
				';
		Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
	}

	/**
	 * @return TPPBSAreaPrice
	 */
	public static function getAreaPriceByArea($id_product, $total_area, $id_shop)
	{
		$sql = 'SELECT
					id_area_price,
					id_product,
					id_shop,
					area_low,
					area_high,
					impact,
					price,
					weight
				FROM '._DB_PREFIX_.'ppbs_area_price
				WHERE id_product = '.(int)$id_product.'
				AND id_shop = '.(int)$id_shop.'
				AND ('.(float)$total_area.' >= area_low AND '.(float)$total_area.' <= area_high)
				';
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		if ($result)
		{
			$areaPrice = new TPPBSAreaPrice();
			$areaPrice->areaLow = $result[0]['area_low'];
			$areaPrice->areaHigh = $result[0]['area_high'];
			$areaPrice->impact = $result[0]['impact'];
			$areaPrice->price = $result[0]['price'];
			$areaPrice->weight = $result[0]['weight'];
			$areaPrice->id_product = $result[0]['id_product'];
			return $areaPrice;
		}
		return false;
	}

	/**
	 * @return array of TPPBSAreaPrice
	 */
	public static function getAreaPrices($id_product, $id_shop = 1)
	{
		$areaPriceCollection = array();
		$sql = 'SELECT
					id_area_price,
					id_product,
					id_shop,
					area_low,
					area_high,
					impact,
					price
				FROM '._DB_PREFIX_.'ppbs_area_price
				WHERE id_product = '.(int)$id_product.'
				AND id_shop = '.(int)$id_shop.'
				ORDER BY (area_low * area_high)';
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if ($result)
		{
			foreach ($result as $key => $row)
			{
				$areaPrice = new TPPBSAreaPrice();
				$areaPrice->areaLow = $row['area_low'];
				$areaPrice->areaHigh = $row['area_high'];
				$areaPrice->impact = $row['impact'];
				$areaPrice->price = $row['price'];
				$areaPrice->id_product = $row['id_product'];
				$areaPriceCollection[] = $areaPrice;
			}
			return $areaPriceCollection;
		}
		return false;
	}

	public static function getAreaPricesRaw($id_product, $id_shop = 1)
	{
		$sql = 'SELECT
					id_area_price,
					id_product,
					id_shop,
					area_low,
					area_high,
					impact,
					price,
					weight
				FROM '._DB_PREFIX_.'ppbs_area_price
				WHERE id_product = '.(int)$id_product.'
				AND id_shop = '.(int)$id_shop.'
				ORDER BY (area_low * area_high)';
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		return $result;
	}

	public static function save_area_price(TPPBSAreaPrice $areaPrice, $id_shop)
	{
		Db::getInstance()->insert('ppbs_area_price', array(
			'id_shop' => (int)$id_shop,
			'area_low' => (float)$areaPrice->areaLow,
			'area_high' => (float)$areaPrice->areaHigh,
			'price' => (float)$areaPrice->price,
			'weight' => (float)$areaPrice->weight,
			'impact' => pSQL($areaPrice->impact),
			'id_product' => (int)$areaPrice->id_product
		));
	}

	public static function deleteAreaPrice($id_area_price)
	{
		$sql = 'DELETE FROM '._DB_PREFIX_.'ppbs_area_price WHERE id_area_price='.(int)$id_area_price;
		Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
	}

	/**
	 * @return TPPBSProductUnit
	 */
	public static function getProductUnitSettings($id_ppbs_unit, $id_product, $id_shop)
	{
		$sql = 'SELECT
		 			id_ppbs_unit,
		 			id_product,
		 			id_shop,
		 			`min`,
		 			`max`,
		 			`input_type`,
		 			`default`,
		 			visible
				FROM '._DB_PREFIX_.'ppbs_product_unit
				WHERE id_ppbs_unit='.(int)$id_ppbs_unit.'
				AND id_product='.(int)$id_product.'
				AND id_shop='.(int)$id_shop.'
				';
		$row = DB::getInstance()->getRow($sql);
		if ($row)
		{
			$ppbs_product_unit = new TPPBSProductUnit();
			$ppbs_product_unit->id_ppbs_unit = $row['id_ppbs_unit'];
			$ppbs_product_unit->id_product = $row['id_product'];
			$ppbs_product_unit->min = $row['min'];
			$ppbs_product_unit->max = $row['max'];
			$ppbs_product_unit->default = $row['default'];
			$ppbs_product_unit->visible = $row['visible'];
			$ppbs_product_unit->input_type = $row['input_type'];
			return $ppbs_product_unit;
		}
		else
			return false;
	}

	public static function saveProductUnitSettings(TPPBSProductUnit $ppbs_unit)
	{
		DB::getInstance()->delete('ppbs_product_unit',
			'id_product='.(int)$ppbs_unit->id_product.'
			AND id_ppbs_unit='.(int)$ppbs_unit->id_ppbs_unit.'
			AND id_shop='.(int)$ppbs_unit->id_shop);

		DB::getInstance()->insert('ppbs_product_unit', array(
			'id_product' => (int)$ppbs_unit->id_product,
			'id_ppbs_unit' => (int)$ppbs_unit->id_ppbs_unit,
			'id_shop' => (int)$ppbs_unit->id_shop,
			'min' => (float)$ppbs_unit->min,
			'max' => (float)$ppbs_unit->max,
			'default' => (float)$ppbs_unit->default,
			'visible' => (float)$ppbs_unit->visible,
		));
	}

	/*
	 * Allow only 1 qty for PPBS Cart Products
	 */
	public static function saveHookActionCartSave($id_cart, $id_shop)
	{
		Db::getInstance()->update('cart_product', array(
				'quantity' => '1'
			),
			'id_cart='.(int)$id_cart.'
			AND id_shop = '.(int)$id_shop.'
			AND ppbs = 1'
		);

		/* Delete all PPBS products in the cart which do not have an associated customisation, these are illegal products (orphans) */
		$sql = 'SELECT
					id_product,
					id_product_attribute,
					id_cart,
					id_shop
				FROM '._DB_PREFIX_.'cart_product cp
				WHERE id_cart NOT IN
					(
						SELECT id_cart
						FROM '._DB_PREFIX_.'customization c
						WHERE
							id_cart = cp.id_cart
							AND id_shop = cp.id_shop
							AND id_product = cp.id_product
							AND id_product_attribute = cp.id_product_attribute
					)
				AND cp.ppbs = "1"
				';
		
		$result = DB::getInstance()->executeS($sql);
		
		if ($result)
		{
			foreach ($result as $row)
			{
				$sql = 'DELETE FROM '._DB_PREFIX_.'cart_product
						WHERE id_product='.(int)$row['id_product'].'
						AND id_product_attribute = '.(int)$row['id_product_attribute'].'
						AND id_cart = '.(int)$row['id_cart'];
				Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
			}
		}
	}

	public static function getProductAttributePrice($id_product, $id_shop, $id_product_attribute)
	{
		$cache_id_2 = $id_product.'-'.$id_shop;
		if (!isset(self::$_pricesPPBS[$cache_id_2]))
		{
			$sql = new DbQuery();
			$sql->select('product_shop.`price`, product_shop.`ecotax`');
			$sql->from('product', 'p');
			$sql->innerJoin('product_shop', 'product_shop', '(product_shop.id_product=p.id_product AND product_shop.id_shop = '.(int)$id_shop.')');
			$sql->where('p.`id_product` = '.(int)$id_product);

			if (Combination::isFeatureActive())
			{
				$sql->select('product_attribute_shop.id_product_attribute, product_attribute_shop.`price` AS attribute_price, product_attribute_shop.default_on');
				$sql->leftJoin('product_attribute', 'pa', 'pa.`id_product` = p.`id_product`');
				$sql->leftJoin('product_attribute_shop', 'product_attribute_shop', '(product_attribute_shop.id_product_attribute = pa.id_product_attribute AND product_attribute_shop.id_shop = '.(int)$id_shop.')');
			}
			else
				$sql->select('0 as id_product_attribute');

			$res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

			foreach ($res as $row)
			{
				$array_tmp = array(
					'price' => $row['price'],
					'ecotax' => $row['ecotax'],
					'attribute_price' => (isset($row['attribute_price']) ? $row['attribute_price'] : null)
				);
				self::$_pricesPPBS[$cache_id_2][(int)$row['id_product_attribute']] = $array_tmp;

				if (isset($row['default_on']) && $row['default_on'] == 1)
					self::$_pricesPPBS[$cache_id_2][0] = $array_tmp;
			}
		}
		/*if (!isset(self::$_pricesLevel2[$cache_id_2][(int)$params['id_product_attribute']]))
			return;*/
		$result = self::$_pricesPPBS[$cache_id_2][(int)$id_product_attribute];
		return $result;
	}


	/*
	 * Extends the original cart.duplicate method, ensuring the PPBS_dimensions column in the customized_data is copied
	 * to the new entries
	 */
	public static function duplicateForReorder($parent, $old_id_cart)
	{
		$old_cart = new Cart($old_id_cart);
		$old_products = $old_cart->getProducts();
		if ($old_products)
		{
			foreach ($old_products as $product)
			{
				$sql = new DbQuery();
				$sql->select('
					cp.id_cart, cp.id_product, cp.id_address_delivery, cp.id_shop, cp.id_product_attribute,
					cp.ppbs
				');
				$sql->from('cart_product', 'cp');
				$sql->where('cp.id_cart = '.(int)$old_id_cart);
				$sql->where('cp.id_product = '.(int)$product['id_product']);
				$sql->where('cp.id_product_attribute = '.(int)$product['id_product_attribute']);
				$sql->where('cp.id_shop = '.(int)$product['id_shop']);

				$results = Db::getInstance()->executeS($sql);
				if ($results)
				{
					foreach ($results as $old_line_item)
					{
						if ($old_line_item['ppbs'] == '1')
						{
							// set PPBS flag of duplicated cart product
							DB::getInstance()->update('cart_product', array(
								'ppbs' => '1'
							), 'id_cart='.(int)$parent['cart']->id.' AND id_product='.(int)$old_line_item['id_product'].' AND id_product_attribute='.(int)$old_line_item['id_product_attribute'].' AND id_shop='.(int)$old_line_item['id_shop']);

							//get ppbs_dimensions customization from old product
							$sql = new DbQuery();
							$sql->select('
								cd.ppbs_dimensions
							');
							$sql->from('customization', 'c');
							$sql->innerJoin('customized_data', 'cd', 'c.`id_customization` = cd.`id_customization`');
							$sql->where('c.id_cart = '.(int)$old_id_cart);
							$sql->where('c.id_product = '.(int)$old_line_item['id_product']);
							$sql->where('c.id_product_attribute = '.(int)$old_line_item['id_product_attribute']);

							$row = Db::getInstance()->getRow($sql);

							// copy ppbs data to new customization data
							if ($row && !empty($row['ppbs_dimensions']))
							{
								$sql = new DbQuery();
								$sql->select('
									c.id_customization
								');
								$sql->from('customization', 'c');
								$sql->where('c.id_cart = '.(int)$parent['cart']->id);
								$sql->where('c.id_product = '.(int)$old_line_item['id_product']);
								$sql->where('c.id_product_attribute = '.(int)$old_line_item['id_product_attribute']);
								$row_new = DB::getInstance()->getRow($sql);

								if ($row_new && !empty($row_new['id_customization']))
								{
									Db::getInstance()->update('customized_data', array(
										'ppbs_dimensions' => $row['ppbs_dimensions']
									), 'id_customization='.(int)$row_new['id_customization']);
								}
							}
						}
					}
				}
			}
		}
	}

	/* Attribute Wizard Pro */
	public static function getIPAFromAWPVars($instructions_id, $id_cart, $id_product, $id_shop)
	{
		$sql = 'SELECT id_product_attribute
				FROM '._DB_PREFIX_.'cart_product
				WHERE id_cart='.(int)$id_cart.'
				AND id_product='.(int)$id_product.'
				AND id_shop='.(int)$id_shop.'
				ORDER BY date_add DESC';
		$row = DB::getInstance()->getRow($sql);
		return $row['id_product_attribute'];
	}
}