<?php

if (!defined('_PS_VERSION_'))
	exit;

class PPBSProductField extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_ppbs_product_field;

	/** @var integer Dimension ID */
	public $id_ppbs_dimension;

	/** @var integer Dimension ID */
	public $id_ppbs_unit;

	/** @var integer Product ID */
	public $id_product;

	/** @var integer Shop ID */
	public $id_shop;

	/** @var integer Min val */
	public $min;

	/** @var integer Max val */
	public $max;

	/** @var string default value/text */
	public $default;

	/** @var string Input type */
	public $input_type;

	/** @var integer Visible */
	public $visible;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ppbs_product_field',
		'primary' => 'id_ppbs_product_field',
		'fields' => array(
			'id_ppbs_dimension' => array(
				'type' => self::TYPE_INT,
			),
			'id_ppbs_unit' => array(
				'type' => self::TYPE_INT,
			),
			'id_ppbs_unit' => array(
				'type' => self::TYPE_INT,
			),
			'id_product' => array(
				'type' => self::TYPE_INT,
			),
			'id_shop' => array(
				'type' => self::TYPE_INT,
			),
			'visible' => array(
				'type' => self::TYPE_INT,
			),
			'min' => array(
				'type' => self::TYPE_FLOAT,
			),
			'max' => array(
				'type' => self::TYPE_FLOAT,
			),
			'default' => array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 255,
				'required' => true
			),
			'input_type' => array(
				'type' => self::TYPE_STRING,
				'validate' => 'isMessage',
				'size' => 255,
				'required' => true
			)
		)
	);

	public static function getCollectionByProduct($id_product, $id_shop, $id_lang = 1, $visible_only)
	{
		$sql = new DbQuery();
		$sql->select('
			pd.name AS dimension_name,
			pu.name AS unit_name,
			pf.id_ppbs_product_field,
			pf.id_ppbs_dimension,
			pf.id_ppbs_unit,
			pf.input_type,
			pf.visible,
			pf.default,
			pf.min,
			pf.max,
			pdl.display_name
		');
		$sql->from('ppbs_product_field', 'pf');
		$sql->innerJoin('ppbs_dimension', 'pd', 'pd.id_ppbs_dimension = pf.id_ppbs_dimension');
		$sql->innerJoin('ppbs_dimension_lang', 'pdl', 'pf.id_ppbs_dimension = pdl.id_ppbs_dimension AND pdl.id_lang='.(int)$id_lang);
		$sql->innerJoin('ppbs_unit', 'pu', 'pu.id_ppbs_unit = pf.id_ppbs_unit');
		$sql->where('pf.id_product = '.(int)$id_product);
		$sql->where('pf.id_shop = '.(int)$id_shop);
		if ($visible_only)
			$sql->where('pf.visible = 1');
		$sql->orderBy('pf.position');
		return Db::getInstance()->executeS($sql);
	}

	public function loadProductField($id_product, $id_ppbs_dimension, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ppbs_product_field', 'pf');
		$sql->where('pf.id_product = '.(int)$id_product);
		$sql->where('pf.id_shop = '.(int)$id_shop);
		$sql->where('pf.id_ppbs_dimension = '.(int)$id_ppbs_dimension);
		$row = Db::getInstance()->getRow($sql);
		$this->hydrate($row);
	}

	public static function updatePosition($id_ppbs_product_field, $position)
	{
		DB::getInstance()->update('ppbs_product_field', array(
			'position' => (int)$position
		), 'id_ppbs_product_field ='.(int)$id_ppbs_product_field);
	}

}
