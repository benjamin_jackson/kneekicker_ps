<?php
if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/productpricebysize/lib/bootstrap.php');

class ProductPriceBySize extends Module
{
	public $table_unit = 'ppbs_unit';

	public $module_file = __FILE__;

	/** @var PPBSAdminTabController */
	protected $form_tab;

	/** @var PPBSConfigController */
	protected $controller_config;

	/** @var PPBSFrontController */
	protected $controller_front;

	/** @var PPBSCartController */
	protected $controller_cart;

	protected static $_pricesPPBS;

	public function __construct()
	{
		$this->name = 'productpricebysize';
		$this->tab = 'others';
		$this->version = '1.4.3';
		$this->author = 'Predict Marketing';
		$this->need_instance = 0;
		$this->module_key = 'a44fdab3786e7699a005cbaacc7f36f2';
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

		parent::__construct();
		$this->displayName = $this->l('Product Price by Size');
		$this->description = $this->l('Allow customers to customize products by area and calculate prices and weights dynamically');
		$this->bootstrap = true;

		$this->_initialiseControllers();
	}

	private function _initialiseControllers()
	{
		/* Initialise Controllers */
		if (_PS_VERSION_ < 1.6)
		{
			$this->controller_config = new PPBSConfigController($this);
			$this->form_tab = new PPBSAdminTabController15($this);
			$this->controller_front = new PPBSFrontController($this);
			$this->controller_cart = new PPBSCartController($this);
		}
		else
		{
			$this->controller_config = new PPBSConfigController($this);
			$this->form_tab = new PPBSAdminTabController($this);
			$this->controller_front = new PPBSFrontController($this);
			$this->controller_cart = new PPBSCartController($this);
		}
	}

	public function install()
	{
		if (parent::install() == false
			|| !$this->registerHook('productTabContent')
			|| !$this->registerHook('backOfficeHeader')
			|| !$this->registerHook('displayLeftColumnProduct')
			|| !$this->registerHook('displayAdminProductsExtra')
			|| !$this->registerHook('actionCartSave')
			|| !$this->registerHook('ppbsPriceCalculation')
			|| !$this->registerHook('ppbsGetProducts')
			|| !$this->registerHook('displayPPBS')
			|| !$this->registerHook('displayPPBSPrice')
			|| !$this->registerHook('displayUnitPriceText')
			|| !$this->registerHook('displayShoppingCart')
			|| !$this->registerHook('actionProductSave')
			|| !$this->registerHook('ppbsDeleteCartProduct')
			|| !$this->registerHook('displayFooter')
			|| !$this->registerHook('actionCartListOverride')
			|| !$this->registerHook('displayProductPriceBlock')
			|| !$this->registerHook('PPBSOrderConfirmationProductsOverride')
			|| !$this->install_module())
			return false;
		return true;
	}

	public function install_module()
	{
		PPBSInstall::install_db();
		PPBSInstall::install_data();
		return true;
	}

	public function uninstall()
	{
		PPBSInstall::uninstall();
		parent::uninstall();
	}

	public function getContent()
	{
		return $this->controller_config->route();
	}

	/* Module Hooks */

	public function hookActionCartSave($params)
	{
		PPBSModel::saveHookActionCartSave(Context::getContext()->cookie->id_cart, Context::getContext()->shop->id);
	}

	public function hookPpbsDeleteCartProduct($params)
	{
		$sql = 'SELECT COUNT(*) AS total_count
				FROM '._DB_PREFIX_.'customization c
				INNER JOIN '._DB_PREFIX_.'cart_product cp ON c.id_cart = cp.id_cart AND (cp.id_product = '.(int)$params['id_product'].' AND cp.id_product_attribute = '.$params['id_product_attribute'].' AND ppbs = 1)
				WHERE c.id_customization ='.(int)$params['id_customization'].'
				AND c.id_cart = '.(int)$params['cart']->id;
		$result = DB::getInstance()->getRow($sql);
		if ($result['total_count'] > 0)
		{
			DB::getInstance()->delete('customization', 'id_customization='.(int)$params['id_customization']);
			DB::getInstance()->delete('customized_data', 'id_customization='.(int)$params['id_customization']);
			PPBSModel::saveHookActionCartSave($params['cart']->id, $params['cart']->id_shop);
			return true;
		}
		else
			return false;
	}

	public function hookPpbsPriceCalculation($params)
	{
		$ppbs_cart_controller = new PPBSCartController($this);
		return $ppbs_cart_controller->hookPpbsPriceCalculation($params);
	}

	public function hookDisplayAdminProductsExtra($params)
	{
		return $this->form_tab->route();
	}

	public function hookPpbsGetProducts($params)
	{
	}

	public function hookDisplayPPBS($product)
	{
		return $this->controller_front->render(__FILE__);
	}

	public function hookDisplayPPBSPrice()
	{
		$ppbs_product = new PPBSProduct();
		$ppbs_product->getByProduct(Tools::getValue('id_product'), $this->context->shop->id);
		if (empty($ppbs_product->id) || $ppbs_product->enabled == false) return '';
		return "<div id='ppbs_price'></div>";
	}

	public function hookDisplayProductPriceBlock($params)
	{
		if ($params['type'] == 'after_price')
		{
			$return = '';
			$return .= $this->hookDisplayPPBS($params['product']);
			$return .= $this->hookDisplayPPBSPrice();
			return $return;
		}
	}


	public function hookDisplayUnitPriceText($params)
	{
		$ppbs_product = PPBSModel::load_ppbs_product($params['id_product'], $this->context->shop->id);
		if (isset($ppbs_product->enabled) && $ppbs_product->enabled)
		{
			$translations = PPBSModel::load_translations($this->context->shop->id);
			if (isset($translations['unit_price_suffix']['1']->text) && $translations['unit_price_suffix']['1']->text != '')
				return '<span class="ppbs_pricesuffix"><small>'.$translations['unit_price_suffix']['1']->text.'<small></span>';
			else return '';
		}
	}
	
	public function hookDisplayShoppingCart($params)
	{
		if ($params['products'])
		{
			foreach ($params['products'] as &$product)
			{
				$ppbs_product = PPBSModel::load_ppbs_product($product['id_product'], $this->context->shop->id);
				if (isset($ppbs_product->enabled) && $ppbs_product->enabled)
				{
					$product['is_discounted'] = 0;
					$product['total_customization_wt'] = $product['price_wt'];
					$product['total_customization'] = $product['price'];
					//$product['price'] = $product['price'] / $product['customizationQuantityTotal'];
					$product['customizationQuantityTotal'] = 1;
				}
			}
		}
		Context::getContext()->smarty->assign($params);
	}
	
	public function actionProductSave($params)
	{
	}

	public function hookDisplayFooter()
	{
		return $this->controller_cart->renderHookDisplayFooter(__FILE__);
	}

	public function hookActionProductSave($params)
	{
		$this->form_tab->hookActionProductSave($params);
	}

	public function hookActionCartListOverride($params)
	{
		$json = Tools::jsonDecode($params['json']);
		$json->refresh = 1;
		$params['json'] = Tools::jsonEncode($json);
	}

	/*
	 * the customizations displayed in order conf. emails are not correct, override it here
	 */
	public function hookPPBSOrderConfirmationProductsOverride(&$params)
	{
		$product_customization = PPBSModel::getAllProductCustomizations($params['product']['id_product'], $params['product']['id_product_attribute'], $this->context->cart->id);

		$new_customizations = array();

		if (!empty($params['product_var_tpl']['customization']))
		{
			foreach ($product_customization as $customization_item)
			{
				$found_ppbs = false;
				foreach ($params['product_var_tpl']['customization'] as $key => $item)
					if (strpos($item['customization_text'], $customization_item['value']) !== false)
						$found_ppbs = true;

				if ($found_ppbs)
				{
					$new_customization = $item;
					$new_customization['customization_quantity'] = $customization_item['quantity'];
					$new_customization['quantity'] = '';
					$new_customizations[] = $new_customization;
				}
			}
		}
		if (!empty($new_customizations))
			$params['product_var_tpl']['customization'] = $new_customizations;
	}
	
}