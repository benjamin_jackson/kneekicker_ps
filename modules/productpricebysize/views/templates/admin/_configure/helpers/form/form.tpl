{*
* Extend default form, adjusting the arrangement of the field min and max inputs 
*}

{extends file="helpers/form/form.tpl"}
{block name="script"}
	var ps_force_friendly_product = false;
{/block}

{block name="defaultForm"}
	{counter start=0 skip=1 assign=field_count}
	{$smarty.block.parent}
{/block}


{block name="input_row"}
	{if $submit_action == "submit_restraints"}
		{if $input.name|strstr:"unit_"}
			{if $field_count%2==0}
				<div style="clear:both;"></div>
				<div style="float:left;" class="col-lg-2">
			{else}				
				<div style="float:left;" class="col-lg-2">
			{/if}
				{$smarty.block.parent}
			</div>
			<strong>{counter}</strong>
		{else}
			{$smarty.block.parent}
		{/if}
	{else}
		{$smarty.block.parent}
	{/if}	
{/block}

{block name="footer"}
	{if $submit_action == "submit_restraints"}
		<div class="clear"></div>
	{/if}
	{$smarty.block.parent}
{/block}

{block name="label"}
	{if $submit_action == "submit_restraints" && $input.name|strstr:"unit_"}
		{if isset($input.label)}
			<label class="control-label col-lg-8{if isset($input.required) && $input.required && $input.type != 'radio'} required{/if}">
			{if isset($input.hint)}
				<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{if is_array($input.hint)}
					{foreach $input.hint as $hint}
						{if is_array($hint)}
							{$hint.text|escape:'html':'UTF-8'}
						{else}
							{$hint|escape:'html':'UTF-8'}
						{/if}
					{/foreach}
					{else}
						{$input.hint|escape:'html':'UTF-8'}
				{/if}">
			{/if}
			{$input.label|escape:'html':'UTF-8'}
			{if isset($input.hint)}
				</span>
			{/if}
			</label>
		{/if}	
	{else}
		{$smarty.block.parent}
	{/if}	
{/block}

{block name="after"}
	{$smarty.block.parent}
	<style>
		form#configuration_form .col-lg-9  { width:25%;	}
	</style>
{/block}

