<div id="dd_options_wrapper" class="col-xs-12 col-sm-10 col-md-8">
	<div class="form" style="margin-bottom:10px">
		<div class="row">
			<div class="col-xs-3">
				<label>Option Text</label>
				<input type="text" name="option-text">
			</div>

			<div class="col-xs-3">
				<label>Option Value (numeric)</label>
				<input type="text" name="option-value">
			</div>

			<div class="col-xs-4">
				<button class="btn-add-option btn btn-default" style="margin-top:20px">Add Option</button>
			</div>
		</div>
	</div>
	<div class="header">
		<span class="text">Option Text</span>
		<span class="value">Option Value</span>
		<span class="action">Action</span>
	</div>

	<div class="options">
	</div>

	<input name="dropdown_options" type="hidden" value="">
</div>

<script>
	$(document).ready(function() {
		ddconfigwidget = new DropdownConfigWidget("#dd_options_wrapper");
	});
</script>