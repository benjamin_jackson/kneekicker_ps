<div class="panel">
	<h3>Add area range based price</h3>
	<input type="hidden" name="id_area_price" value="{$area_price->id_area_price|intval}">
	<input type="hidden" name="id_product" value="{$id_product|intval}">
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="id_ppbs_dimension">
					<span class="label-tooltip" title="Mionimum value customer must enter">From (area)</span>
				</label>
				<div class="col-lg-6">
					<input id="area_low" name="area_low" type="text" value="{$area_price->area_low|escape:'htmlall':'UTF-8'}" maxlength="8">
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Minimum value customer must enter">To (area)</span>
				</label>
				<div class="col-lg-6">
					<input id="area_high" name="area_high" type="text" value="{$area_price->area_high|escape:'htmlall':'UTF-8'}" maxlength="8">
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Mionimum value customer must enter">Impact on price</span>
				</label>
				<div class="col-lg-6">
					<select id="impact" name="impact">
						<option value="=" {if $area_price->impact eq '='}selected{/if}>Fixed Price</option>
						<option value="+" {if $area_price->impact eq '+'}selected{/if}>Increase by</option>
						<option value="-" {if $area_price->impact eq '-'}selected{/if}>Decrease by</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Mionimum value customer must enter">Price</span>
				</label>

				<div class="col-lg-6">
					<input id="price" name="price" type="text" value="{$area_price->price|escape:'htmlall':'UTF-8'}" maxlength="8">
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Mionimum value customer must enter">Weight</span>
				</label>
				<div class="col-lg-6">
					<input id="weight" name="weight" type="text" value="{$area_price->weight|escape:'htmlall':'UTF-8'}" maxlength="8">
				</div>
			</div>
		</div>
	</div>

	<div class="panel-footer">
		<a href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="ppbs-popup-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>
</div>