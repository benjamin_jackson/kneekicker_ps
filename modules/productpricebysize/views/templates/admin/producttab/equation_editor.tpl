<div id="equation-panel-1" class="panel">
	<input type="hidden" name="id_product" value="{$smarty.get.id_product|intval}">
	<input type="hidden" name="ipa" value="{$smarty.get.ipa|intval}">
	<h3>Equation Editor</h3>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<div id="equation-display"></div>
				<div id="equation-buttons">
					<span class="button single-digit" data-value="0">0</span>
					<span class="button single-digit" data-value="1">1</span>
					<span class="button single-digit" data-value="2">2</span>
					<span class="button single-digit" data-value="3">3</span>
					<span class="button single-digit" data-value="4">4</span>
					<span class="button single-digit" data-value="5">5</span>
					<span class="button single-digit" data-value="6">6</span>
					<span class="button single-digit" data-value="7">7</span>
					<span class="button single-digit" data-value="8">8</span>
					<span class="button single-digit" data-value="9">9</span>
					<span class="button single-digit" data-value=".">.</span>

					<div class="row">
						<div class="col-lg-12">
							{foreach from=$fields item=field}
								<span class="button text-digit" data-value="[{$field.dimension_name|escape:'htmlall':'UTF-8'}]" data-type="variable">{$field.dimension_name|escape:'htmlall':'UTF-8'}</span>
							{/foreach}
							<span class="button text-digit" data-value="[product_price]" data-type="variable">product price</span>
							<span class="button text-digit" data-value="[base_price]" data-type="variable">base price</span>
							<span class="button text-digit" data-value="[attribute_price]" data-type="variable">attribute price</span>
						</div>
					</div>

					<span class="button single-digit" data-value="(" data-type="parenthesis">(</span>
					<span class="button single-digit" data-value=")" data-type="parenthesis">)</span>
					<span class="button single-digit" data-value="+" data-type="operator">+</span>
					<span class="button single-digit" data-value="+" data-type="operator">-</span>
					<span class="button single-digit" data-value="/" data-type="operator">/</span>
					<span class="button single-digit" data-value="*" data-type="operator">*</span>
				</div>
			</div>
		</div>
	</div>


	<div class="panel-footer">
		<a href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="ppbs-equation-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>
</div>

<script>
	equation_tokens = {$equation_tokens|@json_encode};
</script>