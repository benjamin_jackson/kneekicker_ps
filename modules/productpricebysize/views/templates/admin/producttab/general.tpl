<div id="ppbs-general" class="panel">
	<div class="panel-heading">
		<i class="icon-cogs"></i> Product Price By Size Options
	</div>

	<div class="form-wrapper">
		<div class="form-group">

			<label class="control-label col-lg-3">
				enabled for this product?
			</label>

			<div class="col-lg-9 ">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="enabled" id="enabled_on" value="1" {if $ppbs_product->enabled eq "1"}checked{/if}>
					<label for="enabled_on">Yes</label>
					<input type="radio" name="enabled" id="enabled_off" value="0" {if $ppbs_product->enabled neq "1"}checked{/if}>
					<label for="enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>

		</div>


		<div class="form-group">
			<label class="control-label col-lg-3">
				<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="If enable, the attribute price will be included in the total area price. If disabled attribute price will simply be added on the area price">
					Use attribute price in area calculation?
				</span>
			</label>
			<div class="col-lg-9 ">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="attribute_price_as_area_price" id="attribute_price_as_area_price_on" value="1" {if $ppbs_product->attribute_price_as_area_price eq "1"}checked{/if}>
					<label for="attribute_price_as_area_price_on">Yes</label>
					<input type="radio" name="attribute_price_as_area_price" id="attribute_price_as_area_price_off" value="0" {if $ppbs_product->attribute_price_as_area_price neq "1"}checked{/if}>
					<label for="attribute_price_as_area_price_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-3">
				Charge Minimum price (Ex Tax):
			</label>

			<div class="col-lg-9 ">
				<input type="text" name="min_price" id="min_price" value="{$ppbs_product->min_price|escape:'htmlall':'UTF-8'}" class="">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-3">
				Apply setup fee
			</label>

			<div class="col-lg-9 ">
				<input type="text" name="setup_fee" id="setup_fee" value="{$ppbs_product->setup_fee|escape:'htmlall':'UTF-8'}" class="">
			</div>
		</div>


		<div class="form-group">
			<label class="control-label col-lg-3">
				Convert Units entered by customer?
			</label>

			<div class="col-lg-9 ">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="front_conversion_enabled" id="front_conversion_enabled_on" value="1" {if $ppbs_product->front_conversion_enabled eq "1"}checked{/if}>
					<label for="front_conversion_enabled_on">Yes</label>

					<input type="radio" name="front_conversion_enabled" id="front_conversion_enabled_off" value="0" {if $ppbs_product->front_conversion_enabled neq "1"}checked{/if}>
					<label for="front_conversion_enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-3">
				Conversion formula
			</label>

			<div class="col-lg-9 ">
				<select name="front_conversion_operator" style="width:140px; display: inline-block;">
					<option value="*" {if $ppbs_product->front_conversion_operator eq '*'}selected="selected"{/if}>Multiply by</option>
					<option value="/" {if $ppbs_product->front_conversion_operator eq '/'}selected="selected"{/if}>Divide by</option>
				</select>
				<input name="front_conversion_value" type="text" value="{$ppbs_product->front_conversion_value|escape:'htmlall':'UTF-8'}" style="width:140px; display: inline-block;">
			</div>

		</div>


		<div class="form-group hide">
			<input type="hidden" name="id_product" id="id_product" value="{$smarty.get.id_product|escape:'htmlall':'UTF-8'}">
		</div>
	</div>
	<!-- /.form-wrapper -->


	<div class="panel-footer">
		<button type="button" value="1" id="ppbs-general-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>

</div>

<script>
	$(document).ready(function () {
		module_tab_url = '{$module_tab_url|escape:'quotes':'UTF-8'}';
		var ppbs_general = new PPBSGeneral('#ppbs-general');
	});
</script>