{if isset($id_product)}
	<div id="ppbs-fields" class="panel product-tab">
		<h3>{l s='Assign Fields to this product' mod='productpricebysize'}</h3>

		<table id="fieldsTable" class="table tableDnD">
			<thead>
			<tr class="nodrag nodrop">
				<th><span class="title_box">{l s='Name' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Unit' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Input Type' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Default' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Min' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Max' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Visible' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Action' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Position' mod='productpricebysize'}</span></th>
			</tr>
			</thead>

			<tbody>
				{foreach from=$fields item=field}
					<tr data-id_ppbs_product_field="{$field.id_ppbs_product_field|intval}">
						<td>{$field.dimension_name|escape:'htmlall':'UTF-8'}</td>
						<td>{$field.unit_name|escape:'htmlall':'UTF-8'}</td>
						<td>{$field.input_type|escape:'htmlall':'UTF-8'}</td>
						<td>{$field.default|escape:'htmlall':'UTF-8'}</td>
						<td>{$field.min|escape:'htmlall':'UTF-8'}</td>
						<td>{$field.max|escape:'htmlall':'UTF-8'}</td>
						<td class="visible">
							<span class="list-action-enable {if $field.visible eq 0}action-disabled{else}action-enabled{/if}">
								<i class="ppbs-visible icon icon-check" {if $field.visible eq 0}hidden{/if} data-state="0"></i>
								<i class="ppbs-visible icon icon-remove" {if $field.visible eq 1}hidden{/if} data-state="1"></i>
							</span>
						</td>
						<td>
							<a href="#edit" class="ppbs-edit"><i class="icon icon-pencil"></i></a>
							<a href="#delete" class="ppbs-delete"><i class="icon icon-trash"></i></a>
						</td>
						<td class="dragHandle pointer">
							<div class="dragGroup">
								&nbsp;
							</div>
						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>

		<a href="#add_field" id="add_field" class="btn btn-default">
			<i class="icon-plus-sign"></i> {l s='Add a field' mod='productpricebysize'}
		</a>
	</div>
{/if}
<script>
	module_tab_url = '{$module_tab_url|escape:'quotes':'UTF-8'}';
	$(document).ready(function () {
		ppbs_fields = new PPBSFields("#ppbs-fields");
	});
</script>