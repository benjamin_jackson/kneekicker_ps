<input type="hidden" name="id_ppbs_product_field" value="{$field->id_ppbs_product_field|intval}">
<input type="hidden" name="id_product" value="{$id_product|intval}">
<input type="hidden" name="ppbs_product_field_options" id="ppbs_product_field_options" value="">

<div id="panel1" class="panel">
	<h3>Add / Edit Field</h3>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="id_ppbs_dimension">
					<span class="label-tooltip" title="Minimum value customer must enter">Dimension</span>
				</label>
				<div class="col-lg-6">
					<select id="id_ppbs_dimension" name="id_ppbs_dimension">
						{foreach from=$dimensions item=dimension}
							<option value="{$dimension.id_ppbs_dimension|escape:'htmlall':'UTF-8'}" {if $field->id_ppbs_dimension eq $dimension.id_ppbs_dimension}selected{/if}>{$dimension.name|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Mionimum value customer must enter">Unit</span>
				</label>
				<div class="col-lg-6">
					<select id="id_ppbs_unit" name="id_ppbs_unit">
						{foreach from=$units item=unit}
							<option value="{$unit.id_ppbs_unit|intval}" {if $unit.id_ppbs_unit eq $field->id_ppbs_unit}selected{/if}>{$unit.name|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Minimum value customer must enter">{l s='Min' mod='productpricebysize'}</span>
				</label>
				<div class="col-lg-6">
					<input id="min" name="min" type="text" value="{$field->min|escape:'htmlall':'UTF-8'}" maxlength="8">
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Maximum value customer must enter">{l s='Max' mod='productpricebysize'}</span>
				</label>

				<div class="col-lg-6">
					<input id="max" name="max" type="text" value="{$field->max|escape:'htmlall':'UTF-8'}" maxlength="8">
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip">{l s='Default Value' mod='productpricebysize'}</span>
				</label>

				<div class="col-lg-6">
					<input id="default" name="default" type="text" value="{$field->default|escape:'htmlall':'UTF-8'}" maxlength="8">
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip">{l s='Input Type' mod='productpricebysize'}</span>
				</label>

				<div class="col-lg-6">
					<select id="input_type" name="input_type">
						<option value="textbox" {if $field->input_type eq 'textbox'}selected{/if}>Text Box</option>
						<option value="dropdown" {if $field->input_type eq 'dropdown'}selected{/if}>Drop down</option>
					</select>
					<a id="edit-field-options" href="#edit-field_options" class="btn btn-default">
						<i class="icon icon-list"></i>
						<span>{l s='Edit Dropdown Values' mod='productpricebysize'}</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="panel-footer">
		<a href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="ppbs-popup-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> {l s='Save' mod='productpricebysize'}
		</button>
	</div>
</div>

{* Start : Panel2 Dropdown Values *}
<div id="panel2" class="panel subpanel">
	<h3>Dropdown values</h3>

	<div class="row">
		<div class="col-sm-12"><h4>Add dropdown value</h4></div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<div class="form-group">
				<label class="control-label col-lg-6" for="spm_currency_0">
					<span class="label-tooltip" title="">Numeric Value</span>
				</label>

				<div class="col-lg-6">
					<input id="value" name="value" type="text" value="" maxlength="8">
				</div>
			</div>
		</div>

		<div class="col-lg-5">
			<div class="form-group">
				<label class="control-label col-lg-6" for="spm_currency_0">
					<span class="label-tooltip" title="">Display Value</span>
				</label>

				<div class="col-lg-6">
					<input id="text" name="text" type="text" value="" maxlength="8">
				</div>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<a href="#add-field-option" id="add-field-option" class="btn btn-default"><i class="icon-plus"></i> Add</a>
			</div>
		</div>

	</div>

	<div class="row">
		<table id="dropdownTable" class="table tableDnD">
			<thead>
			<tr class="nodrag nodrop">
				<th><span class="title_box">Numeric Value </span></th>
				<th><span class="title_box">Dislpay Value </span></th>
				<th><span class="title_box">Position</span></th>
				<th><span class="title_box">Delete</span></th>
			</tr>
			</thead>

			<tbody>

				{foreach from=$field_options item=field_option}
					<tr>
						<td class="value">{$field_option.value|escape:'htmlall':'UTF-8'}</td>
						<td class="text">{$field_option.text|escape:'htmlall':'UTF-8'}</td>
						<td class="dragHandle pointer">
							<div class="dragGroup">
								&nbsp;
							</div>
						</td>
						<td>
							<a href="#delete" class="ppbs-field-option-delete"><i class="icon icon-trash"></i></a>
						</td>
					</tr>
				{/foreach}

				<tr class="cloneable hidden">
					<td class="value"></td>
					<td class="text"></td>
					<td class="dragHandle pointer">
						<div class="dragGroup">
							&nbsp;
						</div>
					</td>
					<td>
						<a href="#delete" class="ppbs-field-option-delete"><i class="icon icon-trash"></i></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="panel-footer">
		<a href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="ppbs-dropdown-done" class="btn btn-default pull-right">
			<i class="process-icon-check icon-check"></i> Done
		</button>
	</div>

</div>
{* End: Panel2 Dropdown Values *}