{if isset($id_product)}
	<div id="ppbs-arearanges" class="panel product-tab">
		<h3>{l s='Area based prices' mod='productpricebysize' }</h3>

		<div class="alert alert-info">
			Adjust dynamic price based on the total area the customer enters.
		</div>

		<table id="fieldsTable" class="table tableDnD">
			<thead>
			<tr class="nodrag nodrop">
				<th><span class="title_box">{l s='From (area)' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='To (area)' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Price Impact' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Price' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Weight' mod='productpricebysize'}</span></th>
				<th><span class="title_box">{l s='Action' mod='productpricebysize'}</span></th>
			</tr>
			</thead>

			<tbody>
				{foreach from=$area_prices item=area_price}
					<tr data-id_area_price="{$area_price.id_area_price|intval}">
						<td>{$area_price.area_low|escape:'htmlall':'UTF-8'}</td>
						<td>{$area_price.area_high|escape:'htmlall':'UTF-8'}</td>
						<td>{$area_price.impact|escape:'htmlall':'UTF-8'}</td>
						<td>{$area_price.price|escape:'htmlall':'UTF-8'}</td>
						<td>{$area_price.weight|escape:'htmlall':'UTF-8'}</td>
						<td>
							<a href="#edit" class="ppbs-edit"><i class="icon icon-pencil"></i></a>
							<a href="#delete" class="ppbs-delete"><i class="icon icon-trash"></i></a>
						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>

		<a href="#add_arearange" id="add_arearange" class="btn btn-default">
			<i class="icon-plus-sign"></i> {l s='Add a new area range based price' mod='productpricebysize'}
		</a>
	</div>
{/if}
<script>
	module_tab_url = '{$module_tab_url|escape:'quotes':'UTF-8'}';
	$(document).ready(function () {
		ppbs_arearanges = new PPBSAreaRanges("#ppbs-arearanges");
	});
</script>