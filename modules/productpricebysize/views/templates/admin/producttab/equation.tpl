<div id="ppbs-equation" class="panel">
	<div class="panel-heading">
		<i class="icon-cogs"></i> Custom Calculation
	</div>

	<div class="form-wrapper">
		<div class="form-group">

			<label class="control-label col-lg-3">
				Use custom equation?
			</label>

			<div class="col-lg-9 ">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="equation_enabled" id="equation_enabled_on" value="1" {if $ppbs_product->equation_enabled eq "1"}checked{/if}>
					<label for="equation_enabled_on">Yes</label>
					<input type="radio" name="equation_enabled" id="equation_enabled_off" value="0" {if $ppbs_product->equation_enabled neq "1"}checked{/if}>
					<label for="equation_enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
				<button type="button" value="1" id="ppbs-open-equation-editor" class="btn btn-default pull-right">
					<i class="process-icon-save"></i> Edit
				</button>
			</div>
		</div>

		<table id="table-combinations-list" class="table configuration">
			<thead>
			<tr class="nodrag nodrop">
				<th class=" left">
					<span class="title_box">Attribute - value pair</span>
				</th>
				<th class=" left"><span class="title_box">Action</span></th>
			</tr>
			</thead>
			<tbody>
			{foreach from=$combinations item=combination}
				<tr class="{if $combination.default_on eq 1}{/if} odd">
					<td class=" left">
						{$combination.attributes|escape:'htmlall':'UTF-8'}
					</td>
					<td class=" left">
						<a href="#edit" class="ppbs-combination-equation-edit" data-ipa="{$combination.id_product_attribute|escape:'htmlall':'UTF-8'}"><i class="icon icon-pencil"></i></a>
					</td>
				</tr>
			{/foreach}
			</tbody>
		</table>

		<div class="form-group hide">
			<input type="hidden" name="id_product" id="id_product" value="{$smarty.get.id_product|escape:'htmlall':'UTF-8'}">
		</div>
	</div>
	<!-- /.form-wrapper -->



</div>

<script>
	$(document).ready(function () {
		module_tab_url = '{$module_tab_url|escape:'quotes':'UTF-8'}';
		var ppbs_equation = new PPBSEquation('#ppbs-equation');
	});
</script>