<label class="attribute_label">Calculate Size</label>
<div id="ppbs_widget">
	<!--  Room Layout Type -->
	<div class="roomTypeSelection clearfix">
			<input id="roomShapeField" type="hidden" class="roomShape" name="roomShape" value="rectangle">
		<div class="unit-entry productRoomType">
			<label class="radio roomLabel inline" for="rectangle_room_choice">
				<input class="form-control" type="radio" id="rectangle_room_choice" value="rectangle" name="room_shape" />
				<img id="rectangle_image" src="{$tpl_uri}modules/productpricebysize/img/rectangle-room.png" title="Rectangle Floor"  class="selected" />
				Rectangle
			</label>
			<label class="radio roomLabel inline" for="square_room_choice">
				<input class="form-control" type="radio" id="square_room_choice" value="square" name="room_shape" />
				<img id="square_image" src="{$tpl_uri}modules/productpricebysize/img/shape-square.png" title="Square Floor" />
				Square
			</label>
		</div>
		<div class="unit-entry productRoomType">
			<label class="radio roomLabel inline" for="l_shaped_room_choice">
				<input class="form-control" type="radio" id="l_shaped_room_choice" value="l_shaped" name="room_shape" />
				<img id="l_image" src="{$tpl_uri}modules/productpricebysize/img/l-shape.jpg" title="L Shaped Floor" />
				L Shaped
			</label>
			<label class="radio roomLabel inline" for="bay_window_room_choice">
				<input class="form-control" type="radio" id="bay_window_room_choice" value="bay_window" name="room_shape" />
				<img id="bay_image" src="{$tpl_uri}modules/productpricebysize/img/bay-window.jpg" title="Bay Window" />
				Bay Window
			</label>
		</div>
	</div>

	{foreach from=$product_fields item=field}
		<div class="unit-entry unit-measurement" id="unit-{$field.dimension_name|escape:'htmlall':'UTF-8'}" >
			<label>{$field.display_name|escape:'htmlall':'UTF-8'}</label>
			{if $field.input_type eq 'dropdown'}
				<input name="ppbs_field-{$field.id_ppbs_product_field|intval}" style="width:50px; display: none;" class="unit" autocomplete="off" value="{if $field.default gt 0}{$field.default|escape:'htmlall':'UTF-8'}{/if}"/>
				<select name="ppbs_field-{$field.id_ppbs_product_field|intval}_dd" class="dd_options" data-dimension_name="{$field.dimension_name|escape:'htmlall':'UTF-8'}" data-for="ppbs_field-{$field.id_ppbs_product_field|intval}">
					<option value=""></option>
					{foreach from=$field.options item=option}
						<option value="{$option.value|escape:'htmlall':'UTF-8'}" {if $field.default eq $option.value}selected{/if}>{$option.text|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>
				<span class="suffix">{$field['unit']->symbol|escape:'htmlall':'UTF-8'}</span>
			{else}
				<input name="ppbs_field-{$field.id_ppbs_product_field|intval}" style="width:50px;" data-dimension_name="{$field.dimension_name|escape:'htmlall':'UTF-8'}" class="unit unit-{$field.dimension_name|escape:'htmlall':'UTF-8'}" autocomplete="off" value="0"/>
				<span class="suffix">{$field['unit']->symbol|escape:'htmlall':'UTF-8'}</span>
			{/if}

			<div class="error-unit">
				{$field.error|escape:'htmlall':'UTF-8'}
			</div>
		</div>
	{/foreach}
	<div class="ppbs_error" style="display:none;">
		<i class="icon icon-warning"></i>
		{$translations.generic_error|escape:'htmlall':'UTF-8'}
	</div>
	<!-- End Room Layout Type -->
	<!-- <div class="unit-entry unit-measurement">
		<label>A</label>
		<input name="ppbs_field-1" style="width:50px;" data-dimension_name="a" class="unit unit-a" autocomplete="off" value="">
		<span class="suffix">m</span>			
	</div>
	<div class="unit-entry unit-measurement">
		<label>B</label>
		<input name="ppbs_field-2" style="width:50px;" data-dimension_name="b" class="unit unit-b" autocomplete="off" value="">
		<span class="suffix">m</span>
	</div>
	<div class="unit-entry unit-measurement">
		<label>C</label>
		<input name="ppbs_field-3" style="width:50px;" data-dimension_name="c" class="unit unit-c" autocomplete="off" value="">
		<span class="suffix">m</span>
	</div>
	<div class="unit-entry unit-measurement">
		<label>D</label>
		<input name="ppbs_field-4" style="width:50px;" data-dimension_name="d" class="unit unit-d" autocomplete="off" value="">
		<span class="suffix">m</span>
	</div> -->
</div>

<script>
	/* setup globals for js controller */
    product_base_price_tax_excl = productPriceTaxExcluded;
	product_base_price_tax_incl = {$product->base_price|intval}
	product_fields = {$product_fields_json|@json_encode};
    ppbs_product_json = {$ppbs_product_json|@json_encode};
	ppbs_price_adjustments_json = {$ppbs_price_adjustments_json|@json_encode};
	ppbs_equations_collection = {$ppbs_equations_collection|@json_encode};
	var ppbs_translations = {
		unit_price_suffix : "{$translations.unit_price_suffix|escape:'htmlall':'UTF-8'}"
	};

    $(document).ready(function(){
        var ppbs = new PPBSFrontController($("#ppbs_widget"));
    });
</script>