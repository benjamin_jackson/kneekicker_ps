PPBSFrontController = function(widget) {
    var self = this;
    self.$widget = $(widget);
    self.final_price = 0.10;
    self.attribute_price_as_area_price = ppbs_product_json.attribute_price_as_area_price;
	self.min_price = ppbs_product_json.min_price;

    self.base_price = 0.00; // price without attr impacts (with reductions)
    self.price_with_attr = 0.00;   // price with attr impact
    self.roomShape = 'rectangle';  // Room Type default

    /* removes alphabetical characters to return a decimal value only (price) */
    self.removeFormatting = function(number) {
        if (number.indexOf('.') > 0)
            number = number.replace(",", "");
        else
            number = number.replace(",", ".");
        number = number.replace(/[^\d\.-]/g,'');
        return(number);
    };

    self.addTax = function(value) {
        return value + (value * (taxRate / 100));
    };

    self.recalculatePrices = function() {
        //product_base_price = product_base_price_tax_incl;
        product_base_price = product_base_price_tax_excl;
        if (typeof selectedCombination.price !== 'undefined')
            self.price_with_attr = product_base_price + selectedCombination.price;
        else
            self.price_with_attr = product_base_price;

        if (typeof selectedCombination.specific_price !== 'undefined') {
            if (selectedCombination.specific_price.reduction_percent > 0 && !specific_price.unavailable) {
                //product_base_price = product_base_price - ((product_base_price / 100) * selectedCombination.specific_price.reduction_percent);
                product_base_price = self.price_with_attr - ((self.price_with_attr / 100) * selectedCombination.specific_price.reduction_percent);
                self.price_with_attr = product_base_price;
            }
            if (selectedCombination.specific_price.reduction_price > 0 && !specific_price.unavailable) {
                product_base_price = product_base_price - selectedCombination.specific_price.reduction_price;
                self.price_with_attr = product_base_price;
            }
        }
        else {
            /* for products with no combinations, but a price reduction */
            if (reduction_percent > 0) {
                product_base_price = product_base_price - (product_base_price * (reduction_percent / 100));
            }
        }
        //self.price_with_attr = self.addTax(self.price_with_attr);
        self.base_price = product_base_price;
    }

    self.validate = function(validate_empty) {
        var error = false;
        $(".ppbs_error").hide();

        self.$widget.find("input.unit").removeClass("error");
        self.$widget.find(".error-unit").hide();

        self.$widget.find("input.unit").each(function(i, obj) {
            arr_temp = $(obj).attr("name").split("-");
            id = arr_temp[1];
            val = $(obj).val();

            if (typeof product_fields[id] !== "undefined") {
                min =  parseFloat(product_fields[id].min).toFixed(4);
                max =  parseFloat(product_fields[id].max).toFixed(4);
                if (val != "") {
                    var this_error = false;
                    if (parseFloat(min) > 0 && parseFloat(val).toFixed(4) < parseFloat(min)) this_error = true;
                    if (parseFloat(max) > 0 && parseFloat(val).toFixed(4) > parseFloat(max)) this_error = true;
                    if (this_error) {
                        error = true;
                        $(obj).addClass("error");
                        $(obj).parent(".unit-entry").find(".error-unit").fadeIn();
                    }
                }
                if (validate_empty && val == "") {
                    error = true;
                    $(obj).addClass("error");
                    $(obj).next("select").addClass("error");
                }
            }
        });
        if (error) $(".ppbs_error").fadeIn();
        return !error;
    }

    self.getSelectedCombinationPrice = function() {
        if (typeof selectedCombination['price'] !== 'undefined')
            return parseFloat(selectedCombination['price']);
        else return 0;
    }
	
    self._applyDiscount = function (price, amount, type) {
        return price - (price * (amount / 100));
    };	

    self.getPrice = function(totalArea) {
        var return_price = Array();
        return_price.value = 0.00;
        return_price.impact = '';

        self.recalculatePrices();
        if (self.attribute_price_as_area_price == 1)
            return_price.value = parseFloat(self.price_with_attr);
        else
            return_price.value = parseFloat(self.base_price);

        price = return_price.value;

        /* adjust price based on area */
        $.each(ppbs_price_adjustments_json, function( key, areaPrice ) {
            if (totalArea >= areaPrice.areaLow && totalArea <= areaPrice.areaHigh) {
                if (areaPrice.impact == '-') price = price - parseFloat(areaPrice.price);
                if (areaPrice.impact == '+') price = price + parseFloat(areaPrice.price);
                if (areaPrice.impact == '=') price = parseFloat(areaPrice.price);
                return_price.impact = areaPrice.impact;
                return_price.value = price;
				
                if (typeof (selectedCombination.specific_price) !== 'undefined')
                    return_price.value = self._applyDiscount(return_price.value, selectedCombination.specific_price['reduction_percent'], 'percentage');
				
            }
        });
        return return_price;
    };

    self.getEquation = function() {
		var ipa = $('#idCombination').val();
		var equation = '';

		if (typeof(ppbs_equations_collection[ipa]) !== "undefined")
			equation = ppbs_equations_collection[ipa].equation;
		else
			equation = ppbs_equations_collection[0].equation;
		return equation;
    };

	self.replaceAll = function(search, replace, subject) {	
        return subject.split(search).join(replace);
    };

    self.calculatePriceFromEquation = function() {
        equation_parsed = self.getEquation();
        self.$widget.find("input.unit").each(function (i, obj) {
            val = $(obj).val();
            if (val == '' || isNaN(val)) val = 0;
            equation_parsed = equation_parsed.replace('['+ $(obj).attr("data-dimension_name") + ']', val);
        });

        equation_parsed = self.replaceAll('[attribute_price]', selectedCombination.price, equation_parsed);
        equation_parsed = self.replaceAll('[base_price]', product_base_price, equation_parsed);
        equation_parsed = self.replaceAll('[product_price]', self.price_with_attr, equation_parsed);
        price = parseFloat(eval(equation_parsed));
        return price;
    };

    // Convenience methods to get values
    self.getA = function() {
        var obj = self.$widget.find("input.unit-a"); 
        var val = self.removeFormatting($(obj).val());
        if (val == "" || isNaN(val)) val = 0.00;
        return val;
    };
    self.getB = function() {
        var obj = self.$widget.find("input.unit-b");
        var val = self.removeFormatting($(obj).val());
        if (val == "" || isNaN(val)) val = 0.00;
        return val;
    };
    self.getC = function() {
        var obj = self.$widget.find("input.unit-c");
        var val = self.removeFormatting($(obj).val());
        if (val == "" || isNaN(val)) val = 0.00;
        return val;
    };
    self.getD = function() {
        var obj = self.$widget.find("input.unit-d");
        var val = self.removeFormatting($(obj).val());
        if (val == "" || isNaN(val)) val = 0.00;
        return val;
    };

    // Round the width 
    self.roundWidth = function(width) {
        var result = width; 
        if(width <= 4) {
            result = 4;
        }
        if(width > 4 && width <= 5) {
            result = 5;
        }

        return result;
    };

    self.roundNumbers = function(dimensions) {

        var A = dimensions[0];
        var B = dimensions[1];

        // Both numbers less then 4
        // Round highest to 4
        if ((A < 4) && (B < 4)) 
        {
            if (A > B)
            {
                dimensions[0] = 4;
                return dimensions;
            }
            else {
                dimensions[1] = 4;
                return dimensions;
            }

        }

        // One number is < 4, the other is 4 < x < 5
        // Round smallest to 4 
        if ((A < 4) && ((B > 4) && (B < 5 )))
        {
            dimensions[0] = 4;
            return dimensions;
        }

        if (((A > 4) && (A < 5)) && (B < 4))
        {
            dimensions[1] = 4;
            return dimensions;
        }

        // One number is < 4, the other is > 5
        // Round smallest to 4
        if ((A < 4) && (B > 5))
        {
            dimensions[0] = 4;
            return dimensions;
        }

        if ((B < 4) && (A > 5))
        {
            dimensions[1] = 4;
            return dimensions;
        } 

        // Both numbers are 4 < x < 5
        // Round highest to 5
        if (((A > 4) && (A < 5)) && ((B > 4) && (B < 5 )))
        {
            if (A > B)
            {
                dimensions[0] = 5;
                return dimensions;
            }
            else {
                dimensions[1] = 5;
                return dimensions;
            }
        }

        // If one number is higher then 5
        // Other is 4 < x < 5
        // Round smallest to 5
        
        if ((A > 5) && ((B > 4) && (B < 5 )))
        {
            dimensions[1] = 5;
            return dimensions;
        }

        if (((A > 4) && (A < 5)) && (B > 5))
        {
            dimensions[0] = 5;
            return dimensions;
        }

        // Otherwise return numbers
         return dimensions;

    };

    self.calculateSquare = function() {
        var dimensions = [parseFloat(self.getA()), parseFloat(self.getB())];
        var dimensions_rounded = self.roundNumbers(dimensions);
        var area = dimensions_rounded[0] * dimensions_rounded[1];
        return area;
    };
    
    self.calculateLShape = function() {
        var dimensions = [parseFloat(self.getA()), parseFloat(self.getB())];
        var dimensions_rounded = self.roundNumbers(dimensions);
        var area1 = dimensions_rounded[0] * dimensions_rounded[1];
        var area2 = (dimensions_rounded[0] - parseFloat(self.getC())) * (dimensions_rounded[1] - parseFloat(self.getD()));
        var final_area = area1 - area2;
        return final_area;
    };

    /*
     * Here We update the Price
     */
    self.updatePrice = function() {
        if (ppbs_product_json.equation_enabled == 1 && self.getEquation() != '') {
            price = self.addTax(self.calculatePriceFromEquation());
            $("div#ppbs_price").html(formatCurrency(price, currencyFormat, currencySign, currencyBlank));
            return true;
        }

    
    var multiplier = 1;
    switch (self.roomShape) {
        case "square":
            multiplier = self.calculateSquare();
            multiplier = parseFloat(multiplier.toPrecision(12));
            break;
        case "rectangle":
            multiplier = self.calculateSquare();
            multiplier = parseFloat(multiplier.toPrecision(12));
            break;
        case "lshape":
            multiplier = self.calculateSquare();
            multiplier = parseFloat(multiplier.toPrecision(12));
            break;
        case "baywindow":
            multiplier = self.calculateSquare();
            multiplier = parseFloat(multiplier.toPrecision(12));
            break;
        default:
            break;
    }

    /*    var multiplier = 1;
		self.$widget.find("input.unit").each(function(i, obj) {
			var val = self.removeFormatting($(obj).val());
			
             if (val == "" || isNaN(val)) val = 0.00;

             if (val > 0 && ppbs_product_json.front_conversion_enabled == 1) {
                switch(ppbs_product_json.front_conversion_operator) {
                    case "*":
                        val = val * parseFloat(ppbs_product_json.front_conversion_value);
                        break;
                    case "/":
                        val = val / parseFloat(ppbs_product_json.front_conversion_value);
                        break;
                }
            }
            multiplier = parseFloat(multiplier) * parseFloat(val);
            multiplier = parseFloat(multiplier.toPrecision(12));
        });
    */

        var price = self.getPrice(multiplier);

        if (price.impact != '=') {
            var final_price = price.value * multiplier.toPrecision(4);
        }
        else
            var final_price = price.value + self.getSelectedCombinationPrice();

        /* add selected attributes price */
        if (self.attribute_price_as_area_price == 0) {
            final_price = final_price + parseFloat(self.getSelectedCombinationPrice());
            final_price = final_price;
        }
		if (final_price < self.min_price) final_price = parseFloat(self.min_price);
        final_price = self.addTax(parseFloat(final_price.toPrecision(8)));
        $("div#ppbs_price").html(formatCurrency(final_price, currencyFormat, currencySign, currencyBlank));
    }

    self.constrainProportions = function($sender) {
        if (isNaN($sender.val()) || $sender.val() == '') return false;
        var name_sender = $sender.attr("name");
        var name_shadow = $("#ppbs_widget input.unit").not($sender).attr("name");
        var $shadow = $("#ppbs_widget input.unit").not($sender);

        var constraints = new Array();
        self.$widget.find("input.unit").each(function(i, obj) {
            arr_temp = $(obj).attr("name").split("_");
            id = arr_temp[1];
            val = $(obj).val();
            if (typeof product_fields[id] !== "undefined") {
                if (product_fields[id].ratio < 1) product_fields[id].ratio = 1;
                constraints['unit_'+id] = product_fields[id].ratio;
            }
        });

        if (constraints[name_sender] > constraints[name_shadow])
            shadow_value = parseFloat($sender.val()) / parseFloat(constraints[name_sender]);
        else
            shadow_value = parseFloat($sender.val()) * parseFloat(constraints[name_shadow]);
        shadow_value = parseFloat(shadow_value).toFixed(2);
        $shadow.val(shadow_value);
    };

    $('input.unit').typeWatch({
        callback: function () {
            if (self.validate()) {
                self.updatePrice();
            }
        },
        wait: 500,
        highlight: false,
        captureLength: 0
    });

    self.$widget.find("select.dd_options").change(function() {
        $input = $("input[name='"+$(this).attr("data-for")+"']");
        $input.val($(this).val());
		self.updatePrice();
    });


    $(document).on('click', '#add_to_cart button', function(e) {
    });

    self.getCartTotals = function(product) {
        $.ajax({
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            url: baseDir + 'modules/productpricebysize/ajax.php?rand=' + new Date().getTime(),
            async: false,
            cache: false,
            dataType : "json",
            data: 'action=get_cart_totals',
            success: function(jsonData,textStatus,jqXHR) {
                $('.ajax_block_cart_total',parent.document).text(jsonData.order_total);
                $('.ajax_block_products_total',parent.document).text(jsonData.cart_total);
                $('#layer_cart_product_price',parent.document).text($('#ppbs_price').html());
                $('.ajax_cart_total',parent.document).text(jsonData.order_total);
            }
        });
    }

    self.addDimensionsToCart = function(from_quickview) {
        var id_product = $('#product_page_product_id').val();
        var ipa = $('#idCombination').val();

        var str_data = "&";
        self.$widget.find("input.unit").each(function(i, obj) {
            str_data = str_data + $(obj).attr("name") + "=" + $(obj).val() + "&";
        });

        /* Add Attribute Wizard Pro ars so we can identify the IPA server side */
        if (typeof awp_get_attributes == 'function') {
            vars = awp_get_attributes();
            var awp_vars = $.map(vars, function(value, index) {
                return [value];
            });
            str_data = str_data + '&awp_vars='+awp_vars;
        }

        str_data = str_data + "&quantity=1";

        str_room = "&roomshape=" + self.roomShape; 

        $.ajax({
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            url: baseDir + 'modules/productpricebysize/ajax.php?rand=' + new Date().getTime(),
            async: true,
            cache: false,
            dataType : "json",
            data: 'id_product='+id_product+ str_room+"&ipa="+ipa+str_data,
            complete: function(jsonData,textStatus,jqXHR) {
                self.getCartTotals(this);
            },
            success: function(jsonData,textStatus,jqXHR) {
				ajaxCart.refresh();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }
	
    /* update main price when selected attribute changes */
    self.updateMainPrice = function(e) {
        e.preventDefault();
        findCombination();

        /* when attribute impact is not calculated as area price, we need to show base price*/
        if (self.attribute_price_as_area_price == 0) {
            $("#our_price_display").html(formatCurrency(self.addTax(self.base_price), currencyFormat, currencySign, currencyBlank));
            self.updatePrice();
            return false;
        }

        getProductAttribute();
        self.updatePrice();
    };
	
	
	/* This overrides the handler in themes/js/product.js when attribute impact is calculates as area price */
	if (self.attribute_price_as_area_price == 0) {
		$(document).off('change', '.attribute_select');
		$(document).on('click', '.attribute_radio');
		$(document).on('click', '#color_to_pick_list .color_pick');
	}

	$(document).on('change', '.attribute_select', function(e) {
		self.updateMainPrice(e);
	});

	$(document).on('click', '.attribute_radio', function(e) {
		self.updateMainPrice(e);
	});

	$(document).on('click', '#color_to_pick_list .color_pick', function(e) {
		self.updateMainPrice(e);
	});

    /*
     * Listen for the choice of room shape and update the form
     * 
     */

    $("#rectangle_room_choice").click(function() {          
        if($('#rectangle_room_choice').is(':checked')){ self.selectRectangle(); }
    });

    $("#square_room_choice").click(function() {         
        if($('#square_room_choice').is(':checked')) { self.selectSquare(); }
    });

    $("#l_shaped_room_choice").click(function() {
        if($('#l_shaped_room_choice').is(':checked')) { self.selectLShaped(); }
    });

    $("#bay_window_room_choice").click(function() {
        if($('#bay_window_room_choice').is(':checked')) { self.selectBayWindow(); }
    });

    // Show C and D Input fields
    self.hideCandD = function(e) {
        $('#unit-c').hide('slow', 'linear');
        $('#unit-d').hide('slow', 'linear');
    };

    self.showCandD = function(e) {
        $('#unit-c').css('display','inline-block');
        $('#unit-d').css('display','inline-block');
    };

    // Hide C and D Input fields 
    

    /*
     * Update room measurements option based on room type selection
     */
    self.selectSquare = function(e) {
        self.removeActiveClassFromImages();
        self.hideCandD();
        $('#square_image').addClass('selected');
        self.roomShape = 'square';
        $('#roomShapeField').val("square");
        self.updatePrice();
    }
  
    self.selectRectangle = function(e) {
        self.removeActiveClassFromImages();
        self.hideCandD();
        $('#rectangle_image').addClass('selected');
        self.roomShape = 'rectangle';
        $('#roomShapeField').val("rectangle");
        self.updatePrice();        
    }

    self.selectLShaped = function(e) {
        self.removeActiveClassFromImages();
        self.showCandD();
        $('#l_image').addClass('selected');
        self.roomShape = 'lshape';
        $('#roomShapeField').val("lshape");
        self.updatePrice();
    }

    self.selectBayWindow = function(e) {
        self.removeActiveClassFromImages();
        self.hideCandD();
        $('#bay_image').addClass('selected');
        self.roomShape = 'baywindow';
        $('#roomShapeField').val("baywindow");
        self.updatePrice();
    }

    /*
     * Remove selected class from images if switched
     */    
    self.removeActiveClassFromImages = function(e) {
        $('#rectangle_image').removeClass('selected');
        $('#square_image').removeClass('selected');
        $('#l_image').removeClass('selected');
        $('#bay_image').removeClass('selected');
        $('#hall_image').removeClass('selected');
        $('#landing_image').removeClass('selected');
        $('#hall_image').removeClass('selected');
        $('#landing_image').removeClass('selected');
    }

	

    self.init = function() {
        self.recalculatePrices();
		
        var tab_id = $('#customizationForm').parents('.page-product-box').attr('id');
        $('#customizationForm').parents('.page-product-box').hide(); //hide the customization form
        $("#more_info_tabs").find("a[href='#"+tab_id+"']").hide();
        self.updatePrice();

        /* Extend the ajax cart add function, to check dimensions before allowing add to cart */
        ajaxCart.add = (function(original_function) {
            function extendsAdd(idProduct, idCombination, addedFromProductPage, callerElement, quantity, whishlist) {
                if (self.validate(true)) {
                    original_function(idProduct, idCombination, addedFromProductPage, callerElement, quantity, whishlist);
                }
            }
            return extendsAdd;
        })(ajaxCart.add);


        /* Extend the updateCartInformation function in ajax cart for the QuickView popup product page */
        window.parent.ajaxCart.updateCartInformation = (function(original_function) {
            function extendsUpdateCartInformation(jsonData, addedFromProductPage) {
                original_function(jsonData, addedFromProductPage);
                if (!jsonData.hasError || jsonData.hasError == false) {
                    // Add unit data to cart associations
                    self.addDimensionsToCart(true);
                }
            }
            return extendsUpdateCartInformation;
        })(window.parent.ajaxCart.updateCartInformation);

		//$("#our_price_display").append('<span class="ppbs_unit_price_suffix">' + ppbs_translations.unit_price_suffix + '</span>');
		if (typeof ppbs_price_adjustments_json === 'undefined' || ppbs_price_adjustments_json.length == 0)
			$('<span class="ppbs_unit_price_suffix">' + ppbs_translations.unit_price_suffix + '</span>').insertAfter("#our_price_display");

        /* Extend the updateCartInformation function in ajax cart */
        /*ajaxCart.updateCartInformation = (function(original_function) {
            function extendsUpdateCartInformation(jsonData, addedFromProductPage) {
                original_function(jsonData, addedFromProductPage);
                if (!jsonData.hasError || jsonData.hasError == false) {
                    // Add unit data to cart associations
                    self.addDimensionsToCart(false);
                }
            }
            return extendsUpdateCartInformation;
        })(ajaxCart.updateCartInformation);*/
    }
    self.init();
}