DropdownConfigWidget = function(wrapper) {
	self.$wrapper = $(wrapper);

	self._add_option = function(option_text, option_value) {
		var option_value_markup = '<span class="value">' + option_value + '</span>';
		var option_text_markup = '<span class="text">' + option_text + '</span>';
		var delete_button_html = '<span class="action"><a class="btn btn-default btn-delete"><i class="icon-trash"></i></a></span>'

		self.$wrapper.find(".options").append('<span class="option" data-sort-value="'+option_value+'">' + option_text_markup + option_value_markup + delete_button_html + '</span>');
	}

	self.sortOptions = function() {
		self.$wrapper.find('.option').sort(function (a, b) {
			return $(a).attr('data-sort-value') - $(b).attr('data-sort-value');
		})
		.appendTo(self.$wrapper.find(".options"));
	}

	self.saveToForm = function() {
		var options_array = [];
		self.$wrapper.find(".option").each(function (index) {
			var option_obj = {
				value: parseFloat($(this).find(".value").html()),
				text: $(this).find(".text").html()
			};
			options_array.push(option_obj);
		});
		var options_json = JSON.stringify(options_array);
		self.$wrapper.find("input[name='dropdown_options']").val(options_json);
	}

	self.clearOptions = function() {
		self.$wrapper.find(".options").html('');
	}

	self.preload = function() {
		self.clearOptions();
		var preload = $("input[name='ppbs_unit_options_preload']").val();
		if (preload != '')
			var options = JSON.parse(preload);

		for (i = 0; i < options.length; i++)
			self._add_option(options[i].text, options[i].value);
	}

	self.$wrapper.find('.btn-add-option').click(function() {
		var option_text = self.$wrapper.find("input[name='option-text']").val();
		var option_value = self.$wrapper.find("input[name='option-value']").val();
		self._add_option(option_text, option_value);
		self.sortOptions();
		self.saveToForm();
		return false;
	})

	self.$wrapper.on("click", ".btn-delete", function () {
		$(this).parents(".option").remove();
	});

	$("#input_type").change(function() {
		if ($(this).val() == 'dropdown') {
			$(this).parents('.form-group').next('.form-group').show();
		}
		else
			$(this).parents('.form-group').next('.form-group').hide();
	})

	self.init = function () {
		self.preload();
		$("#input_type").trigger('change');
	}
	self.init();

}