PPBSPopup = function(id, dom_parent) {
	var self = this;
	self.dom_parent = "body";
	self.id = id;

	self._createOverlay = function () {
		$(self.dom_parent).prepend("<div id='ppbs-popup-wrapper'><span id='ppbs-popup-close'><i class='icon icon-times'></i></span><div class='ppbs-popup-content'></div></div>");
		$(self.dom_parent).prepend("<div id='ppbs-overlay'></div>");
	};

	self._positionSubPanels = function () {
		$("#ppbs-popup-wrapper .ppbs-popup-content .subpanel").each(function() {
			var x = $("#ppbs-popup-wrapper").width();
			var height = $("#ppbs-popup-wrapper").height();
			$(this).css("left", x + "px");
			$(this).css("width", x + "px");
			$(this).css("height", height + "px");
		});
	};

	self.showSubPanel = function(id) {
		$("#" + self.id).find("#" + id).animate({
			left: "0px",
		}, 250, function () {
		});
	};

	self.hideSubPanel = function (id) {
		var x = $("#ppbs-popup-wrapper").width();
		$("#" + self.id).find("#" + id).animate({
			left: x + "px",
			}, 250, function () {
			}
		);
	};


	self.loadContent = function(url, data) {
		$("#ppbs-popup-wrapper").load(url,
			function () {
				$("#ppbs-overlay").fadeIn();
				self._positionSubPanels();
			}
		);
	};

	self.show = function() {
		self._createOverlay();
	};

	self.showContent = function(url, data, callback_function) {
		self._createOverlay();
		$(".ppbs-popup-content").load(url,
			function () {
				$("#ppbs-overlay").fadeIn();
				$(".ppbs-popup-content").attr('id', self.id);
				self._positionSubPanels();

				if (typeof(callback_function) === 'function')
					callback_function();
			}
		);
	};

	self.close = function() {
		$("#ppbs-popup-wrapper").remove();
		$("#ppbs-overlay").remove();
	};

	/* events */
	$("body").on("click", "#ppbs-popup-close", function () {
		self.close();
	});

	self.init = function(dom_parent) {
		if (typeof dom_parent !== 'undefined')
			self.dom_parent = dom_parent;
	}
	self.init(dom_parent);
}