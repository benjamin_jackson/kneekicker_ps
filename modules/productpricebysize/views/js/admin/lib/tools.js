var Tools = {

	waitStart: function() {
		$("body").append('<div class="mp-wait-wrapper"><i class="icon icon-circle-o-notch icon-spin"></i></div>');
	},

	waitEnd: function () {
		$(".mp-wait-wrapper").remove();
	}

};

