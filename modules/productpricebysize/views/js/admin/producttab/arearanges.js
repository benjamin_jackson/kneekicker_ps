PPBSAreaRanges = function (wrapper) {
	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);

	self.popupAddFormId = 'ppbs-popup-addarearange';
	self.list = "ppbs-arearanges";
	self.popup; //instance of modal popup

	self.openAddForm = function (id_area_price) {
		Tools.waitStart();
		var url = module_tab_url + '&route=ppbsadmintabarearanges&action=renderaddform&id_product=' + id_product;
		if (typeof (id_area_price) !== 'undefined')
			url = url + '&id_area_price=' + id_area_price;
		self.popup = new PPBSPopup(self.popupAddFormId, '#ppbs-fields');
		self.popup.showContent(url, null, Tools.waitEnd);
		return false;
	};

	self.processAddForm = function () {
		$.ajax({
			type: 'POST',
			url: module_tab_url + '&route=ppbsadmintabarearanges&action=processaddform',
			async: true,
			cache: false,
			dataType: "json",
			data: $("#" + self.popupAddFormId + " :input").serialize(),
			success: function (jsonData) {
				self.popup.close();
				self.refreshList();
			}
		});
	};

	self.refreshList = function () {
		$("#"+self.list).load(
			module_tab_url + '&route=ppbsadmintabarearanges&action=renderlist #' + self.list + ' > *',
			{
				'id_product': id_product,
				'is_ajax': 1
			},
			function () {
			}
		);
	};

	self.deleteField = function (id_area_price) {
		if (confirm('Are you sure you want to delete this field?')) {
			$.ajax({
				type: 'POST',
				url: module_tab_url + '&route=ppbsadmintabarearanges&action=processdelete',
				async: true,
				cache: false,
				dataType: "json",
				data: {'id_area_price': id_area_price},
				success: function (jsonData) {
					self.refreshList();
				}
			});
		}
	};


	self.init = function () {
	};
	self.init();

	/* Event Handlers */
	$("body").on("click", self.wrapper + " a#add_arearange", function () {
		self.openAddForm();
		return false;
	});

	// open add / edit area price form
	$("body").on("click", "#" + self.popupAddFormId + " #ppbs-popup-save", function () {
		self.processAddForm();
		return false;
	});

	// edit area price in list
	$("body").on("click", "#" + self.list + " a.ppbs-edit", function () {
		self.openAddForm($(this).parents("tr").attr("data-id_area_price"));
		return false;
	});

	/* Delete area price in list*/
	$("body").on("click", "#" + self.list + " a.ppbs-delete", function () {
		self.deleteField($(this).parents("tr").attr("data-id_area_price"));
		return false;
	});

};

