PPBSGeneral = function(wrapper) {
	var self = this;
	self.$wrapper = $(wrapper);

	self.processForm = function() {
		Tools.waitStart();
		console.log(module_tab_url);
		$.ajax({
			type: 'POST',
			url: module_tab_url + '&route=ppbsadmintabgeneral&action=processform',
			async: true,
			cache: false,
			dataType: "json",
			data: self.$wrapper.find(" :input").serialize(),
			success: function (jsonData) {
				Tools.waitEnd();
			}
		});
	};

	self.init = function() {
	};
	self.init();

	/* Events */
	self.$wrapper.find("#ppbs-general-save").click(function() {
		self.processForm();
		return false;
	});
};

