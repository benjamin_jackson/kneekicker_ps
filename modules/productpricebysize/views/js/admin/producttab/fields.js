PPBSFields = function(wrapper) {
	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);

	self.popupAddFormId = 'ppbs-popup-addfield';

	self.dropDownPanelID = 'panel2';

	self.list = "ppbs-fields";
	self.popup; //instance of modal popup

	/*
	* Serialise field options
	*/
	self._serialiseFieldOptions = function() {
		var options = new Array();
		$("#" + self.dropDownPanelID + ' #dropdownTable tbody tr').each(function () {
			if (!$(this).hasClass('cloneable')) {
				var option = {
					'value': $(this).find("td.value").html(),
					'text': $(this).find("td.text").html().replace(/\"/g, '&quot;')
				};
				options.push(option);
			}
		});
		return JSON.stringify(options);
	};

	/*
	 *  Add new field option to the table
	 */
	self._addNewFieldOption = function() {
		var $dropDownPanel = $("#" + self.dropDownPanelID);
		var value = $("#" + self.dropDownPanelID).find("input#value").val();
		var text = $("#" + self.dropDownPanelID).find("input#text").val();
		var $cloned = $dropDownPanel.find("#dropdownTable tr.cloneable").clone();
		$cloned.removeClass("cloneable");
		$cloned.removeClass("hidden");
		$cloned.find("td.value").html(value);
		$cloned.find("td.text").html(text);
		$cloned.appendTo($dropDownPanel.find("#dropdownTable tbody"));
		$dropDownPanel.find("#dropdownTable").tableDnDUpdate();
	}

	self.initDropdownPanel = function() {
		Tools.waitEnd();
		$("#dropdownTable").tableDnD({
			dragHandle: 'dragHandle',
			onDragClass: 'myDragClass',
			onDragStart: function (table, row) {
			},
			onDrop: function (table, row) {
			}
		});
	};

	self.openAddForm = function(id_ppbs_product_field) {
		Tools.waitStart();
		var url = module_tab_url + '&route=ppbsadmintabfields&action=renderaddform&id_product=' + id_product;
		if (typeof (id_ppbs_product_field) !== 'undefined')
			url = url + '&id_ppbs_product_field=' + id_ppbs_product_field;
		self.popup = new PPBSPopup(self.popupAddFormId, '#ppbs-fields');
		self.popup.showContent(url, null, self.initDropdownPanel);
		return false;
	};

	self.processAddForm = function() {
		Tools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_tab_url + '&route=ppbsadmintabfields&action=processaddform',
			async: true,
			cache: false,
			dataType: "json",
			data: $("#" + self.popupAddFormId + " :input").serialize(),
			success: function (jsonData) {
				self.popup.close();
				self.refreshList();
				Tools.waitEnd();
			}
		});
	};

	self.refreshList = function() {
		Tools.waitStart();
		$("#ppbs-fields").load(
			module_tab_url + '&route=ppbsadmintabfields&action=renderlist #ppbs-fields > *',
			{
				'id_product': id_product,
				'is_ajax' : 1
			},
			function () {
				Tools.waitEnd();
			}
		);
	};

	self.savePositions = function() {
		var positions = new Array();
		$("#" + self.list + " tr").each(function () {
			if (typeof $(this).attr("data-id_ppbs_product_field") !== 'undefined')
				positions.push($(this).attr("data-id_ppbs_product_field"));
		});

		$.ajax({
			type: 'POST',
			url: module_tab_url + '&route=ppbsadmintabfields&action=processpositions',
			async: true,
			cache: false,
			dataType: "json",
			data: {'ids_ppbs_product_field' : positions},
			success: function (jsonData) {
			}
		});
	};

	self.deleteField = function(id_ppbs_product_field) {
		if (confirm('Are you sure you want to delete this field?')) {
			Tools.waitStart();
			$.ajax({
				type: 'POST',
				url: module_tab_url + '&route=ppbsadmintabfields&action=processdelete',
				async: true,
				cache: false,
				dataType: "json",
				data: {'id_ppbs_product_field': id_ppbs_product_field},
				success: function (jsonData) {
					Tools.waitEnd();
					self.refreshList();
				}
			});
		}
	};

	self.visibilityField = function(id_ppbs_product_field, state, $parent_td) {
		if (state == '1') {
			$parent_td.find(".icon-check").show();
			$parent_td.find(".icon-remove").hide();
			$parent_td.find("span.list-action-enable").removeClass('action-disabled');
			$parent_td.find("span.list-action-enable").addClass('action-enabled');
		} else {
			$parent_td.find(".icon-check").hide();
			$parent_td.find(".icon-remove").show();
			$parent_td.find("span.list-action-enable").removeClass('action-enabled');
			$parent_td.find("span.list-action-enable").addClass('action-disabled');
		}
		$.ajax({
			type: 'POST',
			url: module_tab_url + '&route=ppbsadmintabfields&action=processvisibility',
			async: true,
			cache: false,
			dataType: "json",
			data: {
				'id_ppbs_product_field': id_ppbs_product_field,
				'state' : state
			},
			success: function (jsonData) {
			}
		});
	};

	self.init = function() {
		$("#ppbs-fields #fieldsTable").tableDnD({
			dragHandle: 'dragHandle',
			onDragClass: 'myDragClass',
			onDragStart: function (table, row) {
			},
			onDrop: function (table, row) {
				self.savePositions();
			}
		});

	};
	self.init();

	$("body").on("click", self.wrapper + " a#add_field", function () {
		self.openAddForm();
		return false;
	});

	$("body").on("click", "#" + self.popupAddFormId+ " #ppbs-popup-save", function () {
		self.processAddForm();
		return false;
	});

	$("body").on("click", "#" + self.popupAddFormId + " #edit-field-options", function () {
		self.popup.showSubPanel('panel2');
		return false;
	});


	$("body").on("change", "#" + self.popupAddFormId + " select#input_type", function () {
		var $popupform = $("#" + self.popupAddFormId);
		if ($(this).val() == "dropdown")
			$popupform.find("a#edit-field-options").show();
		else
			$popupform.find("a#edit-field-options").hide();
		return false;
	});


	/* Edit field in list*/
	$("body").on("click", "#" + self.list + " a.ppbs-edit", function () {
		self.openAddForm($(this).parents("tr").attr("data-id_ppbs_product_field"));
		return false;
	});

	/* Delete field in list*/
	$("body").on("click", "#" + self.list + " a.ppbs-delete", function () {
		self.deleteField($(this).parents("tr").attr("data-id_ppbs_product_field"));
		return false;
	});

	/* Visibility click */
	$("body").on("click", "#" + self.list + " i.ppbs-visible", function () {
		self.visibilityField($(this).parents("tr").attr("data-id_ppbs_product_field"), $(this).attr("data-state"), $(this).parents("td.visible"));
		return false;
	});


	/*
	 * ###### Events for Dropdown Panel #######
	*/

	$("body").on("click", "#" + self.dropDownPanelID + " #add-field-option", function () {
		self._addNewFieldOption();
		return false;
	});

	// dropdown form save
	$("body").on("click", "#" + self.dropDownPanelID + " #ppbs-dropdown-done", function() {
		$("#" + self.popupAddFormId + " input#ppbs_product_field_options").val(self._serialiseFieldOptions());
		self.popup.hideSubPanel('panel2');
		return false;
	});

	/*
	 * remove field option from the display list
	 */
	$("body").on("click", "#" + self.dropDownPanelID + " a.ppbs-field-option-delete", function () {
		$(this).parents("tr").remove();
	});


};

