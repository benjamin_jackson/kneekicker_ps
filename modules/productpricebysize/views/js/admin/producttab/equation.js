PPBSEquation = function (wrapper) {
	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);

	self.popupFormId = 'ppbs-popup-equation';
	self.popup; //instance of modal popup

	/*
	 * Serialise field options
	 */
	self._serialiseFieldOptions = function () {
		var options = new Array();
		$("#" + self.dropDownPanelID + ' #dropdownTable tbody tr').each(function () {
			if (!$(this).hasClass('cloneable')) {
				var option = {
					'value': $(this).find("td.value").html(),
					'text': $(this).find("td.text").html().replace(/\"/g, '&quot;')
				};
				options.push(option);
			}
		});
		return JSON.stringify(options);
	};

	self.openForm = function (ipa) {
		Tools.waitStart();
		var url = module_tab_url + '&route=ppbsadmintabequation&action=renderform&id_product=' + id_product;
		if (typeof (ipa) !== 'undefined')
			url = url + '&ipa=' + ipa;
		self.popup = new PPBSPopup(self.popupAddFormId, '#ppbs-equation');
		self.popup.showContent(url, null, function() {
			self.renderEquationTokens(equation_tokens);
			Tools.waitEnd();
		});
		return false;
	};

	/* Methods for Equation Editor */

	self.renderEquationTokens = function(tokens) {
		if (tokens.length > 0) {
			for (i=0; i < tokens.length; i++) {
				self.insertParam(tokens[i].value, tokens[i].text, tokens[i].type);
			}
		}
	};

	self.insertParam = function(value, text,  type) {
		var $display = $(self.wrapper + " #equation-display");
		$display.append("<span data-value='" + value + "' class='ppbs-param " + type + "'>" + text + "<i class='icon icon-times' style='display:none;'></i></span>");
	};

	self.createEquationString = function() {
		var str = '';
		$(self.$wrapper.find("#equation-display span.ppbs-param")).each(function (index) {
			str += $(this).attr('data-value');
		});
		return str;
	};

	self.processForm = function(id_product, ipa, str_equation) {
		Tools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_tab_url + '&route=ppbsadmintabequation&action=processform',
			async: true,
			cache: false,
			data: {
				id_product : id_product,
				ipa : ipa,
				equation : str_equation
			},
			dataType: "json",
			success: function (jsonData) {
				self.popup.close();
				Tools.waitEnd();
			}
		});
	};

	self.processEquationState = function(id_product, state) {
		Tools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_tab_url + '&route=ppbsadmintabequation&action=processequationstate',
			async: true,
			cache: false,
			data: {
				id_product: id_product,
				equation_enabled: state
			},
			dataType: "json",
			success: function (jsonData) {
				Tools.waitEnd();
			}
		});
	};


	self.init = function () {
	};
	self.init();

	/* Events for main config panel */

	$("body").on("change", self.wrapper + " input[name='equation_enabled']", function () {
		var id_product = self.$wrapper.find("input[name='id_product']").val();
		self.processEquationState(id_product, $(this).val());
	});

	$("body").on("click", self.wrapper + " #ppbs-open-equation-editor", function () {
		self.openForm();
		return false;
	});

	$("body").on("click", self.wrapper + " a.ppbs-combination-equation-edit", function () {
		var ipa = $(this).attr("data-ipa");
		self.openForm(ipa);
		return false;
	});

	/* Events for Equation Editor */

	/* insert param */
	$("body").on("click", self.wrapper + " span.button", function () {
		self.insertParam($(this).attr("data-value"), $(this).html(), $(this).attr("data-type"));
		return false;
	});

	/* remove param */
	$("body").on("click", self.wrapper + " .ppbs-param i", function () {
		$(this).parents(".ppbs-param").remove();
	});

	/* save */
	$("body").on("click", self.wrapper + " #ppbs-equation-save", function () {
		var str_equation = self.createEquationString();
		var id_product = self.$wrapper.find("input[name='id_product']").val();
		var ipa = self.$wrapper.find("input[name='ipa']").val();
		self.processForm(id_product, ipa, str_equation);
		return false;
	});

};

