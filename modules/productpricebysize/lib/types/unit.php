<?php

class TPPBSDimension {
	public
		$id_ppbs_dimension = -1,
		$name = "",
		$display_name = "",
		$suffix = "",
		$translations = array(); //of TPPBSUnitTranslation
}

	class TPPBSUnitTranslation {
		public
			$id_lang = -1,
			$display_name = "",
			$suffix = "";
	}

class TPPBSProductUnit {
	public
		$id_product = -1,
		$id_ppbs_unit = -1,
		$id_shop,
		$name = "",
		$display_name = "",
		$suffix = "",
		$max = 0,
		$min = 0,
		$default = 0,
		$position = 0,
		$input_type = 'textbox',
		$visible = false,
		$ratio = 0;
}

class TPPBSProduct {
	public
		$enabled = 0,
		$front_conversion_enabled = 0,
		$front_conversion_operator = "",
		$front_conversion_value = 0,
		$attribute_price_as_area_price = false;
}

class TPPBSCartUnit {
	public
		$id_ppbs_dimension = '',
		$display_name = '',
		$symbol = '',
		$value = 0;
}

class TPPBSAreaPrice
{
	public
		$areaLow = -1,
		$areaHigh = -1,
		$impact = '+',
		$price = 0.00,
		$weight = 0.00,
		$id_product = 0;
}

class TPBBSProductCustomization {
	public
		$quantity = 0,
		$ppbs = false;

}