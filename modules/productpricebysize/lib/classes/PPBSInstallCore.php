<?php

class PPBSInstallCore
{
	protected static function dropTable($table_name)
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.$table_name.'`';
		DB::getInstance()->execute($sql);
	}
}