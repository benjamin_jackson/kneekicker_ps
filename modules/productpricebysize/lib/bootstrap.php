<?php

/* Types */
include_once(_PS_MODULE_DIR_."/productpricebysize/lib/types/unit.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/lib/types/translation.php");

/* Library */
include_once(_PS_MODULE_DIR_."/productpricebysize/lib/classes/PPBSControllerCore.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/lib/classes/PPBSInstallCore.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/lib/classes/PPBSMathEval.php");

/* Models */
include_once(_PS_MODULE_DIR_."/productpricebysize/models/ppbs.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/models/PPBSInstall.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/models/PPBSProduct.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/models/PPBSDimension.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/models/PPBSUnit.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/models/PPBSProductField.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/models/PPBSAreaPrice.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/models/PPBSProductFieldOption.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/models/PPBSEquation.php");

/* helpers */
include_once(_PS_MODULE_DIR_.'/productpricebysize/helpers/form/UnitDropdownConfigWidget.php');
include_once(_PS_MODULE_DIR_.'/productpricebysize/helpers/PPBSProductHelper.php');

/* controllers */
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/PPBSConfigControllerAdd.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/PPBSConfigController.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/PPBSConfigControllerUnits.php");

include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/producttab/PPBSAdminTabFields.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/producttab/PPBSAdminTabAreaRanges.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/producttab/PPBSAdminTabGeneral.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/producttab/PPBSAdminTabEquation.php");

include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/PPBSAdminTabUnitController.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/PPBSAdminTabController.php");


include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/front/PPBSFrontController.php");
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/front/PPBSCartController.php");

/* PS 1.5.x */
include_once(_PS_MODULE_DIR_."/productpricebysize/controllers/admin/PPBSAdminTabController15.php");