<?php

class PPBSFrontController extends Module {

	protected $sibling;

	public function __construct(&$sibling)
	{
		parent::__construct();
		if ($sibling !== null && Tools::getValue('id_product') != '')
		{
			$ppbs_product = PPBSModel::load_ppbs_product((int)Tools::getValue('id_product'), $this->context->shop->id);
			$this->sibling = &$sibling;

			if (isset($ppbs_product->enabled) && $ppbs_product->enabled == 1 && Context::getContext()->controller->php_self == 'product')
			{
				$this->sibling->context->controller->addJquery();
				$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/front/ppbs.js');
				$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/front/ppbs.css');
				$this->sibling->context->controller->addJqueryPlugin('typewatch');
			}
		}

		if ($sibling !== null && isset(Context::getContext()->controller->php_self))
			if (Context::getContext()->controller->php_self == 'order' || Context::getContext()->controller->php_self == 'order-opc')
				$sibling->context->controller->addJS($sibling->_path.'views/js/front/ppbs_cart.js');
	}

	public function shouldShowAreaPrice()
	{
		$allowed_callers = array(
			'blockcart.php' => array(
				'file' => 'blockcart.php',
				'function' => ''
			),
			'cart.php' => array(
				'file' => 'cart.php',
				'function' => ''
			),
			'paymentmodule.php' => array(
				'file' => 'paymentmodule.php',
				'function' => ''
			)
		);

		$stack = debug_backtrace();

		if (count($stack) > 10) $loop_max = 10;
		else $loop_max = count($stack) - 1;
		for ($x = 0; $x <= $loop_max; $x++)
		{
			if (!empty($stack[$x]['file']))
			{
				$filename = basename($stack[$x]['file']);
				if (isset($allowed_callers[Tools::strtolower($filename)]))
					return true;
			}
		}
		return false;
	}
	
	/*
	 * Prevent widget being rendered multiple times on product page
	 */
	public function shouldRenderWidget()
	{
		$shouldRender = true;
		$stack = debug_backtrace();

		if (count($stack) > 10) $loop_max = 10;
		else $loop_max = count($stack) - 1;
		for ($x = 0; $x <= $loop_max; $x++) 
		{
			if (!empty($stack[$x]['file'])) 
			{
				$filename = basename($stack[$x]['file']);
				if (strpos($filename, 'product-slider') !== false) $shouldRender = false;
			}
		}
		return $shouldRender;
	}
	

	public function render($module_file)
	{
		$translations = array();
		
		if (!$this->shouldRenderWidget()) return '';

		$product_fields = PPBSProductField::getCollectionByProduct(Tools::getValue('id_product'), $this->context->shop->id, $this->context->language->id, true);

		$ppbs_product = new PPBSProduct();
		$ppbs_product->getByProduct(Tools::getValue('id_product'), $this->context->shop->id);

		$translations_obj = PPBSModel::load_translations();

		if (empty($ppbs_product->id) || $ppbs_product->enabled == false) return '';

		$translations['generic_error'] = $translations_obj['generic_error'][$this->context->language->id]->text;
		$translations['unit_price_suffix'] = $translations_obj['unit_price_suffix'][$this->context->language->id]->text;

		/* Add min max error to the Units collection, and make key the field ID */
		$product_fields_arranged = array();
		foreach ($product_fields as &$field)
		{
			if ($field['visible'] == 1)
			{
				$field['error'] = $translations_obj['min_max_error'][$this->context->language->id]->text;
				$field['error'] = str_replace('{min}', $field['min'], $field['error']);
				$field['error'] = str_replace('{max}', $field['max'], $field['error']);

				if ($field['input_type'] == 'dropdown')
					$field['options'] = PPBSProductFieldOption::getFieldOptions($field['id_ppbs_product_field']);

				$unit = new PPBSUnit($field['id_ppbs_unit'], $this->context->language->id);
				if (!empty($unit->id))
					$field['unit'] = $unit;

				$product_fields_arranged[$field['id_ppbs_product_field']] = $field;
			}
		}

		/* Price Adjustments */
		$areaPriceCollection = PPBSModel::getAreaPrices((int)Tools::getValue('id_product'), $this->context->shop->id);

		/* Custom equations */
		$ppbs_equations = new PPBSEquation();
		$ppbs_equations_collection = $ppbs_equations->getAllByProduct((int)Tools::getValue('id_product'));

		$this->sibling->smarty->assign(array(
			'ppbs_product' => $ppbs_product,
			'ppbs_product_json' => $ppbs_product,
			'ppbs_price_adjustments_json' => $areaPriceCollection,
			'ppbs_equations_collection' => PPBSProductHelper::createCombinationsLookup($ppbs_equations_collection),
			'product_fields' => $product_fields_arranged,
			'product_fields_json' => $product_fields_arranged,
			'id_language' => $this->context->language->id,
			'translations' => $translations
		));

		return $this->sibling->display($module_file, 'views/templates/front/ppbs.tpl');
	}

	public function getCartTotals()
	{
		$return = array();

		$return['cart_total'] = Context::getContext()->cart->getTotalCart(Context::getContext()->cart->id, false, Cart::ONLY_PRODUCTS);
		$return['order_total'] = Tools::displayPrice(Context::getContext()->cart->getOrderTotal(), Currency::getCurrencyInstance((Context::getContext()->cart->id_currency), false));
		return $return;
	}

	
	public function addDimensionsToCart()
	{
		if (!isset($this->context->cart->id)) return false;
		$id_cart = $this->context->cart->id;
		$id_product = Tools::getValue('id_product');
		$ipa = Tools::getValue('ipa');
		$roomShape = Tools::getValue('roomshape');
		$quantity = Tools::getValue('quantity');
		$awp_vars = Tools::getValue('awp_vars');

		if (!empty($awp_vars) && empty($ipa))
			$ipa = PPBSModel::getIPAFromAWPVars(','.$awp_vars, $id_cart, $id_product, Context::getContext()->shop->id);

		$cart_unit_collection = array();
		$ppbs_dimensions = PPBSDimension::getDimensions($this->context->language->id, $this->context->shop->id);
		$ppbs_product_field_collection = PPBSProductField::getCollectionByProduct(Tools::getValue('id_product'), Context::getContext()->shop->id, Context::getContext()->language->id, true);

		if (is_array($ppbs_product_field_collection))
		{
			foreach ($ppbs_product_field_collection as $field)
			{
				$ppbs_product_field = new PPBSProductField();
				$ppbs_product_field->loadProductField(Tools::getValue('id_product'), $field['id_ppbs_dimension'], Context::getContext()->shop->id);
				$ppbs_unit = new PPBSUnit($ppbs_product_field->id_ppbs_unit, $this->context->language->id);

				$cart_unit = new TPPBSCartUnit();
				$cart_unit->id_ppbs_dimension = $field['id_ppbs_dimension'];
				$cart_unit->symbol = $ppbs_unit->symbol;
				$cart_unit->display_name = $field['display_name'];
				$cart_unit->value = Tools::getValue('ppbs_field-'.$field['id_ppbs_product_field']);

				if ((float)Tools::getValue('ppbs_field-'.$field['id_ppbs_product_field']) > 0)
					$cart_unit_collection[] = $cart_unit;
			}
		}
		PPBSModel::updateCartProduct($id_product, $id_cart, $ipa, Context::getContext()->cart->id_address_delivery, $cart_unit_collection, $roomShape, $quantity, $this->context->shop->id);
	}
}