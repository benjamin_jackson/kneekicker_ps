<?php

class PPBSCartController extends FrontController {

	protected $module_sibling;

	public function __construct($module_sibling = null)
	{
		if ($module_sibling != null)
			$this->module_sibling = $module_sibling;
	}

	public function renderHookDisplayFooter($module_file)
	{
		if (Context::getContext()->controller->php_self == 'order' || Context::getContext()->controller->php_self == 'order-opc')
		{
			$module_controller_url = 'modules/productpricebysize/ajax.php';
			return '
				<script>
					$(document).ready(function() {
						module_controller_url = "'.$module_controller_url.'";
						var ppbs_cart_controller = new PPBSCartController();
					});
				</script>';
		}
		else return '';
	}
	
	private function _applyDiscount($price, $amount, $type)
	{
		if ($type == 'percentage')
			$price = $price - ($price * $amount);
		return $price;
	}
	
	private function _removeNumberFormatting($number)
	{
		if (substr_count($number, ',') == 1)
			return str_replace(',', '.', $number);
		return $number;
	}

	public function log($data)
	{
		echo("<script>console.log('Log: ".json_encode($data)."')</script>");
	}

	/*
	 * Round up the numbers:
	 */
	public function round($dimensions)
	{
		$A = $dimensions['A'];
		$B = $dimensions['B'];

		// Both numbers less then 4
		// Round highest to 4
		if (($A < 4) && ($B < 4)) 
		{
			if ($A > $B)
			{
				$dimensions['A'] = 4;
				return $dimensions;
			}
			else {
				$dimensions['B'] = 4;
				return $dimensions;
			}

		}

		// One number is < 4, the other is 4 < x < 5
		// Round smallest to 4 
		if (($A < 4) && (($B > 4) && ($B < 5 )))
		{
			$dimensions['A'] = 4;
			return $dimensions;
		}

		if ((($A > 4) && ($A < 5)) && ($B < 4))
		{
			$dimensions['B'] = 4;
			return $dimensions;
		}

		// One number is < 4, the other is > 5
		// Round smallest to 4
		if (($A < 4) && ($B > 5))
		{
			$dimensions['A'] = 4;
			return $dimensions;
		}

		if (($B < 4) && ($A > 5))
		{
			$dimensions['B'] = 4;
			return $dimensions;
		} 

		// Both numbers are 4 < x < 5
		// Round highest to 5
		if ((($A > 4) && ($A < 5)) && (($B > 4) && ($B < 5 )))
		{
			if ($A > $B)
			{
				$dimensions['A'] = 5;
				return $dimensions;
			}
			else {
				$dimensions['B'] = 5;
				return $dimensions;
			}
		}

		// If one number is higher then 5
		// Other is 4 < x < 5
		// Round smallest to 5
		
		if (($A > 5) && (($B > 4) && ($B < 5 )))
		{
			$dimensions['B'] = 5;
			return $dimensions;
		}

		if ((($A > 4) && ($A < 5)) && ($B > 5))
		{
			$dimensions['A'] = 5;
			return $dimensions;
		}

		// Otherwise return numbers
		 return $dimensions;
			
	}

	public function getABCDValues($cart_units)
	{
		$dimension_values = array();

		foreach ($cart_units as $key => $unit)
		{
			switch ($unit->display_name) {
				case 'A':
					$dimension_values['A'] = $this->_removeNumberFormatting($unit->value);
					break;
				case 'B':
					$dimension_values['B'] = $this->_removeNumberFormatting($unit->value);
					break;
				case 'C':
					$dimension_values['C'] = $this->_removeNumberFormatting($unit->value);
					break;
				case 'D':
					$dimension_values['D'] = $this->_removeNumberFormatting($unit->value);
					break;				
				default:
					break;
			}
		}

		return $dimension_values;
	}

	public function calculateSquare($cart_units)
	{
		$dimensions =  $this->getABCDValues($cart_units);

		$dimensions_rounded = $this->round($dimensions);

		$total_area = $dimensions_rounded['A'] * $dimensions_rounded['B'];

		return $total_area;
	}

	public function calculateLShape($cart_units)
	{
		$dimensions =  $this->getABCDValues($cart_units);

		$dimensions_rounded = $this->round($dimensions);

		$first_area = $dimensions_rounded['A'] * $dimensions_rounded['B'];

		$second_area = ($dimensions_rounded['A'] - $dimensions_rounded['C']) * ($dimensions_rounded['B'] - $dimensions_rounded['D']);

		$total_area = $first_area - $second_area;

		return $total_area;
	}

	public function calculateBayWindow($cart_units)
	{
		$dimensions =  $this->getABCDValues($cart_units);

		$dimensions_rounded = $this->round($dimensions);

		$total_area = $dimensions_rounded['A'] * $dimensions_rounded['B'];

		return $total_area;
	}

	public function hookPpbsPriceCalculation($params)
	{

		static $address = null;
		static $context = null;

		/* set up tax caculator */
		if ($address === null)
			$address = new Address();
		$address->id_country = $params['id_country'];
		$address->id_state = $params['id_state'];
		$address->postcode = $params['zipcode'];

		$tax_manager = TaxManagerFactory::getManager($address, Product::getIdTaxRulesGroupByIdProduct((int)$params['id_product'], $context));
		$product_tax_calculator = $tax_manager->getTaxCalculator();

		$total_area = 0;
		$product = new Product($params['id_product'], null, null, $params['id_shop']);
		$price_base = $product->price;

		$attribute_price = PPBSModel::getProductAttributePrice($params['id_product'], $params['id_shop'], $params['id_product_attribute']);
		$attribute_price = (float)$attribute_price['attribute_price'];
		
		$price_inc_attribute = $params['price'];
		if ($params['use_tax'] == '')
			$price_inc_attribute = $product_tax_calculator->addTaxes($price_inc_attribute);
		
		/* Add tax if necessary */
		/*if ($params['use_tax'] == '1')
			$price_base = $product_tax_calculator->addTaxes($price_base);*/

		if (isset($params['specific_price']))
		{
			$specific_price = $params['specific_price'];
	
			if (isset($specific_price['reduction']) && isset($specific_price['reduction_type']))
				$price_base = $this->_applyDiscount($price_base, $specific_price['reduction'], $specific_price['reduction_type']);
		}

		$cart_unit_collection = PPBSModel::get_cart_product_units(
				$params['id_product'],
				$params['id_cart'],
				$params['id_product_attribute'],
				$params['id_shop']
		);

		$ppbs_product = new PPBSProduct();
		$ppbs_product->getByProduct($params['id_product'], $params['id_shop']);

		if (empty($ppbs_product->id) || $ppbs_product->enabled == 0) return $params['price'];

		$unit_total = 0.00;
		if (!is_array($cart_unit_collection)) return $params['price'];

		
		/*
		 *  Calculate price based on room shape
		 *  KK requested the Lshape calc be changed to Square
		 */
		foreach ($cart_unit_collection as $cart_units)
		{

			$line_quantity = $cart_units['quantity'];
			$room_shape = $cart_units['ppbs_roomshape'];
			$cart_units = Tools::jsonDecode($cart_units['ppbs_dimensions']);
			$multiplier = 1;
			$total_area = 0;


			if (is_array($cart_units))
			{

				switch ($room_shape) {
					case 'square':			
						$total_area = $this->calculateSquare($cart_units);
						break;
					case 'rectangle':
						$total_area = $this->calculateSquare($cart_units);
						break;
					case 'lshape':
						$total_area = $this->calculateSquare($cart_units);
						break;
					case 'baywindow':
						$total_area = $this->calculateBayWindow($cart_units);
						break;				
					default:
						$total_area = $this->calculateSquare($cart_units);
						break;
				}

				//$this->log($total_area);
				$multiplier = $multiplier * $total_area;

				// foreach ($cart_units as $key => $unit)
				// {
				// 	$unit->value = $this->_removeNumberFormatting($unit->value);
				// 	// if ($ppbs_product->front_conversion_enabled == 1)
				// 	// {
				// 	// 	switch ($ppbs_product->front_conversion_operator)
				// 	// 	{
				// 	// 		case '/':
				// 	// 			if ($ppbs_product->front_conversion_value == 0) $ppbs_product->front_conversion_value = 1;
				// 	// 			$unit->value = $unit->value / $ppbs_product->front_conversion_value;
				// 	// 			break;
				// 	// 		case '*':
				// 	// 			if ($ppbs_product->front_conversion_value == 0) $ppbs_product->front_conversion_value = 1;
				// 	// 			$unit->value = $unit->value * $ppbs_product->front_conversion_value;
				// 	// 			break;
				// 	// 	}
				// 	// }
				// 	if ((float)$unit->value > 0)
				// 	{
				// 		$total_area = $total_area * $unit->value;
				// 		$multiplier = $multiplier * $unit->value;
				// 	}
				// 	if ($total_area == 0) $total_area = $unit->value;
				// }

				/* use base price to calculate area, and add attributes price to that total */
				if (!$ppbs_product->attribute_price_as_area_price)
					$adjustedPrice = $price_base;
				else
					$adjustedPrice = $params['price'];

				/* Adjust price based on area */
				if ($total_area > 0)
				{
					$areaPrice = PPBSModel::getAreaPriceByArea((int)$params['id_product'], $total_area, (int)$params['id_shop']);
					if ($areaPrice)
					{
						if ($areaPrice->impact == '-') $adjustedPrice = $adjustedPrice - (float)$areaPrice->price;
						if ($areaPrice->impact == '+') $adjustedPrice = $adjustedPrice + (float)$areaPrice->price;
						if ($areaPrice->impact == '=') $adjustedPrice = (float)$areaPrice->price;
						
						if ($params['use_tax'] == '1')
							$adjustedPrice = $product_tax_calculator->addTaxes($adjustedPrice);
							
						if (isset($params['specific_price']))
							if (isset($specific_price['reduction']) && isset($specific_price['reduction_type']))
								$adjustedPrice = $this->_applyDiscount($adjustedPrice, $specific_price['reduction'], $specific_price['reduction_type']);

					}
				}

				if (isset($areaPrice->impact) && $areaPrice->impact == '=')
				{
					//$unit_total += $product_tax_calculator->addTaxes($adjustedPrice + $attribute_price) * $line_quantity;
					$unit_total += ($adjustedPrice + $attribute_price) * $line_quantity;
				}
				else
				{
					if (!$ppbs_product->attribute_price_as_area_price)
						//$product_tax_calculator->addTaxes($unit_total += (($adjustedPrice * $multiplier) + $attribute_price) * $line_quantity);
						//$unit_total += $product_tax_calculator->addTaxes((($adjustedPrice * $multiplier) + $attribute_price) * $line_quantity);
						$unit_total += (($adjustedPrice * $multiplier) + $attribute_price) * $line_quantity;
					else
					{
						$customisation_total = ($adjustedPrice * $multiplier);
						//print $customisation_total."<br>";
						if ($customisation_total < $product_tax_calculator->addTaxes($ppbs_product->min_price))
							$unit_total += $product_tax_calculator->addTaxes($ppbs_product->min_price) * $line_quantity;
						else
							$unit_total += ($adjustedPrice * $multiplier) * $line_quantity;
						//$unit_total = $unit_total * $line_quantity;
					}
				}
			}
		}

		// /* Add setup fees set */
		// if ($ppbs_product->setup_fee > 0)
		// 	$unit_total = $unit_total + $ppbs_product->setup_fee;

		if ($params['use_tax'] == '1')
			$unit_total = $product_tax_calculator->addTaxes($unit_total);
		

		/* Impose min price if set */
		/*if ($unit_total < $product_tax_calculator->addTaxes($ppbs_product->min_price))
			$unit_total = $product_tax_calculator->addTaxes($ppbs_product->min_price);*/


		return $unit_total;
	}


	private function updateCartQty()
	{
		$result = array();
		$result['cart_customization_qty_updated'] = false;
		$id_cart = Context::getContext()->cart->id;
		$product_customization = PPBSModel::getProductCustomization(Tools::getValue('id_product'), Tools::getValue('ipa'), $id_cart, Tools::getValue('id_customization'));
		if ($product_customization->ppbs)
		{
			$product = new Product(Tools::getValue('id_product'));
			$minimal_quantity = (Tools::getValue('ipa')) ? Attribute::getAttributeMinimalQty(Tools::getValue('ipa')) : $product->minimal_quantity;
			if ((int)Tools::getValue('qty') < $minimal_quantity)
				$result['cart_customization_qty_updated'] = false;
				//$this->errors[] = sprintf(Tools::displayError('You must add %d minimum quantity', !Tools::getValue('ajax')), $minimal_quantity);
			else
				{
					PPBSModel::updatCartProductQty(Tools::getValue('id_product'), $id_cart, Tools::getValue('ipa'), Tools::getValue('id_customization'), Tools::getValue('id_address_delivery'), Tools::getValue('qty'));
					$result['cart_customization_qty_updated'] = true;
				}
		}
		else
			$result['cart_customization_qty_updated'] = false;
		return $result;
	}
	
	/**
	 * Return cart weight
	 * @return float Cart weight
	 */
	public function getTotalPPBSProductWeight($products = null)
	{
		$total_weight = 0;
		foreach ($products as $product)
		{
			$unit_weight = $product['weight'];
			$cart_ppbs_unit = PPBSModel::get_cart_product_units($product['id_product'], Context::getContext()->cart->id, $product['id_product_attribute'], Context::getContext()->shop->id);
			if (!empty($cart_ppbs_unit))
			{
				foreach ($cart_ppbs_unit as $cart_unit)
				{
					$ppbs_product = PPBSModel::load_ppbs_product($product['id_product'], Context::getContext()->shop->id);
					$ppbs_dimensions = Tools::jsonDecode($cart_unit['ppbs_dimensions']);
					$total_dimensions = 1;
					if (!empty($ppbs_dimensions))
						foreach ($ppbs_dimensions as $dimension)
						{
							if ($ppbs_product->front_conversion_enabled == 1)
							{
								switch ($ppbs_product->front_conversion_operator)
								{
									case '/':
										if ($ppbs_product->front_conversion_value == 0) $ppbs_product->front_conversion_value = 1;
										$dimension->value = $dimension->value / $ppbs_product->front_conversion_value;
										break;
									case '*':
										if ($ppbs_product->front_conversion_value == 0) $ppbs_product->front_conversion_value = 1;
										$dimension->value = $dimension->value * $ppbs_product->front_conversion_value;
										break;
								}
							}
							$total_dimensions = $total_dimensions * $dimension->value;
						}
					$areaPrice = PPBSModel::getAreaPriceByArea((int)$product['id_product'], $total_dimensions, Context::getContext()->shop->id);
					if (!empty($areaPrice->weight))
						$total_weight += $areaPrice->weight * $cart_unit['quantity'];
					else
						$total_weight += ($unit_weight * $total_dimensions) * $cart_unit['quantity'];
				}
			}
			else
			{
				/* original code from getTotalWeight function in classes/Cart.php */
				$total_weight = 0;
				foreach ($products as $product)
				{
					if (!isset($product['weight_attribute']) || is_null($product['weight_attribute']))
						$total_weight += $product['weight'] * $product['cart_quantity'];
					else
						$total_weight += $product['weight_attribute'] * $product['cart_quantity'];
				}
				return $total_weight;
			}
		}
		return $total_weight;
	}

	public function route()
	{
		switch (Tools::getValue('do'))
		{
			case 'updatePPBSQty':
				return Tools::jsonEncode($this->updateCartQty());

		}
		return false;
	}

}