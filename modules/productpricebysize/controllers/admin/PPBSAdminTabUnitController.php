<?php
class PPBSAdminTabUnitController extends PPBSControllerCore
{
	/** @var ProductPriceBySize */
	protected $sibling;

	protected $key_tab = 'ModuleProductpricebysize';

	public function __construct($sibling)
	{
		$this->sibling = $sibling;
	}

	private function renderEditForm()
	{
		$product_unit = PPBSModel::getProductUnitSettings(Tools::getValue('id_ppbs_unit'), Tools::getValue('id_product'), Context::getContext()->shop->id);
		$unit = PPBSUnit::get_unit_definition($product_unit->id_ppbs_unit, Configuration::get('PS_LANG_DEFAULT'));

		$fields = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Product Unit Settings ('.$unit['display_name'].')'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Minimum Value Required'),
						'name' => 'min',
						'class' => 'fixed-width-md',
						'size' => 32
					),
					array(
						'type' => 'text',
						'label' => $this->l('Max Value Required'),
						'name' => 'max',
						'class' => 'fixed-width-md',
						'size' => 32
					),
					array(
						'type' => 'text',
						'label' => $this->l('Default value'),
						'name' => 'default',
						'class' => 'fixed-width-md',
						'size' => 32
					),
					array(
						'type' => 'hidden',
						'name' => 'visible'
					),
					array(
						'type' => 'hidden',
						'name' => 'id_ppbs_unit'
					),
					array(
						'type' => 'hidden',
						'name' => 'id_product'
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submit_config_translations',
				)
			),
		);

		$helper = new HelperForm();
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this->sibling;
		$helper->currentIndex = $this->sibling->context->link->getAdminLink('AdminProducts', false).'&id_product='.Tools::getValue('id_product').'&updateproduct&route=admin_tab_unit&do=process_form';
		$helper->token = Tools::getAdminTokenLite('AdminProducts');


		$helper->tpl_vars['fields_value']['id_ppbs_unit'] = $product_unit->id_ppbs_unit;
		$helper->tpl_vars['fields_value']['id_product'] = $product_unit->id_product;
		$helper->tpl_vars['fields_value']['min'] = $product_unit->min;
		$helper->tpl_vars['fields_value']['max'] = $product_unit->max;
		$helper->tpl_vars['fields_value']['default'] = $product_unit->default;
		$helper->tpl_vars['fields_value']['position'] = $product_unit->position;
		$helper->tpl_vars['fields_value']['visible'] = $product_unit->visible;

		return $helper->generateForm(array($fields));
	}

	private function _getDropdownConfigHtml()
	{
		$ddconfigwidget = new PPBSUnitDropdownConfigWidget($this->sibling);
		return $ddconfigwidget->render();
	}

	protected function renderDropDownForm()
	{
		$fields = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Option Values'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'name' => 'input_type',
						'type' => 'select',
						'label' => 'Input type',
						'class' => 'fixed-width-xs',
						'options' => array(
							'query' => array(
								array(
									'id_option' => 'textbox',
									'name' => 'Textbox'
								),
								array(
									'id_option' => 'dropdown',
									'name' => 'Dropdown'
								)
							),
							'id' => 'id_option',
							'name' => 'name'
						)
					),

					array(
						'label' => 'Dropdown values',
						'type' => 'html',
						'name' => 'configurator',
						'html_content' => $this->_getDropdownConfigHtml(),
					),

					array(
						'type' => 'hidden',
						'name' => 'id_product'
					),

					array(
						'type' => 'hidden',
						'name' => 'id_ppbs_unit'
					),

					array(
						'type' => 'hidden',
						'name' => 'ppbs_unit_options_preload'
					),

				),
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submit_config_translations',
				)
			),
		);

		$helper = new HelperForm();
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this->sibling;
		$helper->currentIndex = $this->sibling->context->link->getAdminLink('AdminProducts', false).'&id_product='.Tools::getValue('id_product').'&updateproduct&route=admin_tab_unit&do=process_ddform';
		$helper->token = Tools::getAdminTokenLite('AdminProducts');

		$product_unit = PPBSModel::getProductUnitSettings(Tools::getValue('id_ppbs_unit'), Tools::getValue('id_product'), Context::getContext()->shop->id);
		$options = PPBSProductUnitOption::getUnitOptions(Tools::getValue('id_ppbs_unit'));

		$helper->tpl_vars['fields_value']['input_type'] = $product_unit->input_type;
		$helper->tpl_vars['fields_value']['id_product'] = Tools::getValue('id_product');
		$helper->tpl_vars['fields_value']['id_ppbs_unit'] = Tools::getValue('id_ppbs_unit');
		$helper->tpl_vars['fields_value']['ppbs_unit_options_preload'] = Tools::jsonEncode($options);

		return $helper->generateForm(array($fields));
	}

	private function processForm()
	{
		if (Tools::getValue('id_product') == '' || Tools::getValue('id_ppbs_unit') == '') return false;
		$ppbs_unit = new TPPBSProductUnit();
		$ppbs_unit->id_ppbs_unit = Tools::getValue('id_ppbs_unit');
		$ppbs_unit->id_product = Tools::getValue('id_product');
		$ppbs_unit->min = Tools::getValue('min');
		$ppbs_unit->max = Tools::getValue('max');
		$ppbs_unit->default = Tools::getValue('default');
		$ppbs_unit->position = Tools::getValue('position');
		$ppbs_unit->id_shop = Context::getContext()->shop->id;
		$ppbs_unit->visible = Tools::getValue('visible');
		PPBSModel::saveProductUnitSettings($ppbs_unit);
	}

	public function processDDForm()
	{
		/*$ppbs_product_unit = new PPBSProductUnit((int)Tools::getValue('id_ppbs_unit'));

		if (Tools::getValue('input_type') == 'textbox')
		{
			$ppbs_product_unit->input_type = 'textbox';
			$ppbs_product_unit->save(false, false);
			return true;
		}

		$ppbs_product_unit->input_type = 'dropdown';
		$ppbs_product_unit->save(false, false);

		$options = Tools::jsonDecode(Tools::getValue('dropdown_options'));
		if (count($options) > 0)
		{
			PPBSProductUnitOption::deleteAllByPPBSUnitID(Tools::getValue('id_ppbs_unit'));

			foreach ($options as $option)
			{
				$option_model = new PPBSProductUnitOption();
				$option_model->id_ppbs_unit = (int)Tools::getValue('id_ppbs_unit');
				$option_model->text = pSQL($option->text);
				$option_model->value = (float)$option->value;
				$option_model->add(false);
			}
		}*/
	}

	public function route()
	{
		$return = '';
		switch (Tools::getValue('do'))
		{
			case 'process_form' :
				$this->processForm();
				$this->redirectProductTab('key_tab='.$this->key_tab);
				break;
			case 'process_ddform' :
				$this->processDDForm();
				$this->redirectProductTab('key_tab='.$this->key_tab);
				break;
			default:
				$return .= $this->renderEmptyForm();
				$return .= $this->renderEditForm();
				$return .= $this->renderDropDownForm();
		}
		return $return;
	}


}