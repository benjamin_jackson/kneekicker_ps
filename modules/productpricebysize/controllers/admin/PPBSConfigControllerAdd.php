<?php

class PPBSConfigControllerAdd extends Module
{
	public $table_dimension = 'ppbs_dimension';
	public $sibling;

	public function __construct($sibling)
	{
		$this->sibling = $sibling;
	}

	public function renderForm()
	{
		$fields_form_1 = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Add Dimension'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'name' => 'id_ppbs_dimension',
						'type' => 'hidden'
					),
					array(
						'name' => 'name',
						'type' => 'text',
						'label' => $this->l('Dimension Name (e.g height)'),
						'class' => 'fixed-width-xl',
						'required' => true,
						'size' => 32,
						'maxlength' => 32
					),
					array(
						'type' => 'text',
						'label' => $this->l('Dimension Display Name (e.g Height)'),
						'name' => 'display_name',
						'class' => 'fixed-width-xl',
						'lang' => true,
						'required' => true,
						'size' => 32,
						'maxlength' => 32
					)
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submitConfig',
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = 'ppbs_dimension';
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this->sibling;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->sibling->identifier;
		$helper->submit_action = 'submitPPBSConfiguration';
		$helper->currentIndex = $this->sibling->context->link->getAdminLink('AdminModules', false).'&configure='.$this->sibling->name.'&tab_module='.$this->sibling->tab.'&module_name='.$this->sibling->name.'&do=form_add_process';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => array(
				'name' => '',
				'display_name' => array(
					'1' => '',
					'2' => ''
				),
				'suffix' => array(
					'1' => '',
					'2' => ''
				)
			),
			'languages' => $this->sibling->context->controller->getLanguages(),
			'id_language' => $this->sibling->context->language->id
		);

		/* For updating existing PPBS Dimension */
		if (Tools::getValue('id_ppbs_dimension') != '')
		{
			$languages = $this->sibling->context->controller->getLanguages();
			$dimension = new PPBSDimension((int)Tools::getValue('id_ppbs_dimension'));
			$helper->tpl_vars['fields_value']['id_ppbs_dimension'] = (int)Tools::getValue('id_ppbs_dimension');
			$helper->tpl_vars['fields_value']['name'] = $dimension->name;
			foreach ($languages as $key => $language)
			{
				if (isset($dimension->display_name[$language{'id_lang'}]))
					$helper->tpl_vars['fields_value']['display_name'][$language{'id_lang'}] = $dimension->display_name[$language{'id_lang'}];
				else
					$helper->tpl_vars['fields_value']['display_name'][$language{'id_lang'}] = '';

				if (isset($dimension->suffix[$language{'id_lang'}]))
					$helper->tpl_vars['fields_value']['suffix'][$language{'id_lang'}] = $dimension->suffix[$language{'id_lang'}];
				else
					$helper->tpl_vars['fields_value']['suffix'][$language{'id_lang'}] = '';
			}
		}
		else
			$helper->tpl_vars['fields_value']['id_ppbs_dimension'] = '';

		return $helper->generateForm(array($fields_form_1));
	}

	public function formAddProcess()
	{
		$languages = Language::getLanguages(false);

		if (Tools::getValue('id_ppbs_dimension') != '')
			$dimension = new PPBSDimension(Tools::getValue('id_ppbs_dimension'));
		else
			$dimension = new PPBSDimension();

		$dimension->name = Tools::getValue('name');

		foreach ($languages as $key => $language)
			$dimension->display_name[$language{'id_lang'}] = Tools::getValue('display_name_'.$language{'id_lang'});

		$dimension->position = 0;
		$dimension->id_shop = (int)Context::getContext()->shop->id;
		$dimension->save();
	}

}