<?php

class PPBSAdminTabAreaRangesController extends PPBSControllerCore
{

	public function renderList()
	{
		$area_prices = PPBSAreaPrice::getCollectionByProduct(Tools::getValue('id_product'), Context::getContext()->shop->id);
		Context::getContext()->smarty->assign(array(
			'area_prices' => $area_prices,
			'id_product' => Tools::getValue('id_product'),
			'module_tab_url' => $this->module_tab_url
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/arearanges.tpl');
	}

	public function renderAddForm()
	{
		if (Tools::getValue('id_area_price') != '')
			$area_price = new PPBSAreaPrice(Tools::getValue('id_area_price'));

		if (!empty($area_price))
			Context::getContext()->smarty->assign(array('area_price' => $area_price));

		Context::getContext()->smarty->assign(array(
			'id_product' => Tools::getValue('id_product')
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/arearanges_add.tpl');
	}

	public function processAddForm()
	{
		$area_range = new PPBSAreaPrice(Tools::getValue('id_area_price'));
		$area_range->id_product = (int)Tools::getValue('id_product');
		$area_range->id_shop = Context::getContext()->shop->id;
		$area_range->area_low = (float)Tools::getValue('area_low');
		$area_range->area_high = (float)Tools::getValue('area_high');
		$area_range->price = (float)Tools::getValue('price');
		$area_range->weight = (float)Tools::getValue('weight');
		$area_range->impact = pSQL(Tools::getValue('impact'));
		$area_range->save();
	}

	public function processDelete()
	{
		if (Tools::getValue('id_area_price') != '')
		{
			$field = new PPBSAreaPrice(Tools::getValue('id_area_price'));
			$field->delete();
		}
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderaddform' :
				die($this->renderAddForm());
				break;

			case 'processaddform' :
				die($this->processAddForm());
				break;

			case 'processdelete' :
				die($this->processDelete());
				break;

			case 'renderlist':
				die($this->renderList());
				break;
		}

		$this->renderList();
	}

}