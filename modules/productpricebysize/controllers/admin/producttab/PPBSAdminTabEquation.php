<?php

class PPBSAdminTabEquationController extends PPBSControllerCore
{

	public function render()
	{
		$combinations = PPBSProductHelper::getCombinations(Tools::getValue('id_product'), Context::getContext()->language->id);

		Context::getContext()->smarty->assign(array(
			'id_product' => Tools::getValue('id_product'),
			'module_tab_url' => $this->module_tab_url,
			'combinations' => $combinations
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/equation.tpl');
	}

	public function renderForm()
	{
		$ppbs_equation = new PPBSEquation();
		$ppbs_equation->getByProduct(Tools::getValue('id_product'), (int)Tools::getValue('ipa'));

		$equation_tokens = array();

		/* tokenize the equation for rendering in the equation display */
		if (!empty($ppbs_equation->equation))
		{
			preg_match_all('/\[([\p{L}0-9_]+?)\]|[0-9\.\*\-\+\/\(\)]/u', $ppbs_equation->equation, $matches, PREG_SET_ORDER);
			if (!empty($matches))
				foreach ($matches as $match)
					if (!empty($match[0]) || $match[0] == '0')
					{
						$token = array();
						$token['value'] = $match[0];
						if (!empty($match[1])) $token['text'] = $match[1];
						else $token['text'] = $match[0];

						// determine type for rendering properties
						if (Tools::strlen($token['value']) == 1 && is_numeric($token['value'])) $token['type'] = '';
						if (Tools::strlen($token['value']) == 1 && in_array($token['value'], array('*', '/', '-', '+'))) $token['type'] = 'operator';
						if (Tools::strlen($token['value']) == 1 && in_array($token['value'], array('(', ')'))) $token['type'] = 'parenthesis';
						if (Tools::strlen($token['value']) > 1)
							if (strpos($token['value'], ']') !== false) $token['type'] = 'variable';

						$equation_tokens[] = $token;
					}
		}

		$fields = PPBSProductField::getCollectionByProduct(Tools::getValue('id_product'), Context::getContext()->shop->id, Context::getContext()->language->id);
		Context::getContext()->smarty->assign(array(
			'fields' => $fields,
			'equation_tokens' => $equation_tokens
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/equation_editor.tpl');
	}

	public function processForm()
	{
		if (Tools::getValue('id_product') == '') return false;
		$ppbs_equation = new PPBSEquation();
		$ppbs_equation->getByProduct(Tools::getValue('id_product'), Tools::getValue('ipa'));
		$ppbs_equation->ipa = (int)Tools::getValue('ipa');
		$ppbs_equation->id_product = (int)Tools::getValue('id_product');
		$ppbs_equation->equation = Tools::getValue('equation');
		$ppbs_equation->save();
	}

	public function processEquationState()
	{
		$ppbs_product = new PPBSProduct();
		$ppbs_product->getByProduct(Tools::getValue('id_product'), Context::getContext()->shop->id);

		$ppbs_product->equation_enabled = Tools::getValue('equation_enabled');
		$ppbs_product->id_product = Tools::getValue('id_product');
		$ppbs_product->id_shop = Context::getContext()->shop->id;
		$ppbs_product->save();
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderform' :
				die($this->renderForm());

			case 'processform' :
				die($this->processForm());

			case 'processequationstate' :
				die($this->processEquationState());

			case 'render':
				die($this->render());
		}
	}

}