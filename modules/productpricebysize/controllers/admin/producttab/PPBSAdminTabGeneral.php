<?php

class PPBSAdminTabGeneralController extends PPBSControllerCore
{

	public function renderForm()
	{
		$ppbs_product = new PPBSProduct();
		$ppbs_product->getByProduct(Tools::getValue('id_product'), Context::getContext()->shop->id);
		Context::getContext()->smarty->assign(array(
			'ppbs_product' => $ppbs_product,
			'module_tab_url' => $this->module_tab_url
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/general.tpl');
	}

	public function processForm()
	{
		$ppbs_product = new PPBSProduct();
		$ppbs_product->getByProduct(Tools::getValue('id_product'), Context::getContext()->shop->id);

		$ppbs_product->id_product = Tools::getValue('id_product');
		$ppbs_product->id_shop = Context::getContext()->shop->id;
		$ppbs_product->enabled = Tools::getValue('enabled');
		$ppbs_product->min_price = Tools::getValue('min_price');
		$ppbs_product->setup_fee = Tools::getValue('setup_fee');
		$ppbs_product->attribute_price_as_area_price = Tools::getValue('attribute_price_as_area_price');
		$ppbs_product->front_conversion_enabled = (int)Tools::getValue('front_conversion_enabled');
		$ppbs_product->front_conversion_operator = Tools::getValue('front_conversion_operator');
		$ppbs_product->front_conversion_value = Tools::getValue('front_conversion_value');
		$ppbs_product->save();

		/* Create / Update Product Customization data for PPBS */
		if ($ppbs_product->enabled)
			PPBSModel::createProductCustomization(Tools::getValue('id_product'), true, $ppbs_product->id_shop);
		else
			PPBSModel::createProductCustomization(Tools::getValue('id_product'), false, $ppbs_product->id_shop);

	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'processform' :
				die($this->processForm());
		}
	}

}