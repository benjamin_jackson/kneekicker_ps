<?php

class PPBSAdminTabFieldsController extends PPBSControllerCore
{

	public function renderList()
	{
		$fields = PPBSProductField::getCollectionByProduct(Tools::getValue('id_product'), Context::getContext()->shop->id, Context::getContext()->language->id);
		Context::getContext()->smarty->assign(array(
			'fields' => $fields,
			'id_product' => Tools::getValue('id_product'),
			'module_tab_url' => $this->module_tab_url
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/fields.tpl');
	}

	public function renderAddForm()
	{
		$dimensions = PPBSDimension::getDimensions(Context::getContext()->language->id, Context::getContext()->shop->id);
		$units = PPBSUnit::getUnits();

		if (Tools::getValue('id_ppbs_product_field') != '')
			$field = new PPBSProductField(Tools::getValue('id_ppbs_product_field'));

		// get dropdown options if available
		$field_options = array();
		if ($field->input_type == 'dropdown')
			$field_options = PPBSProductFieldOption::getFieldOptions(Tools::getValue('id_ppbs_product_field'));

		if (!empty($field))
			Context::getContext()->smarty->assign(array('field' => $field));

		Context::getContext()->smarty->assign(array(
			'id_product' => Tools::getValue('id_product'),
			'dimensions' => $dimensions,
			'field_options' => $field_options,
			'units' => $units
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/fields_add.tpl');
	}

	public function processAddForm()
	{
		$product_field = new PPBSProductField(Tools::getValue('id_ppbs_product_field'));
		$product_field->id_ppbs_unit = (int)Tools::getValue('id_ppbs_unit');
		$product_field->id_ppbs_dimension = (int)Tools::getValue('id_ppbs_dimension');
		$product_field->id_product = (int)Tools::getValue('id_product');
		$product_field->id_shop = Context::getContext()->shop->id;
		$product_field->min = (float)Tools::getValue('min');
		$product_field->max = (float)Tools::getValue('max');
		$product_field->default = (float)Tools::getValue('default');
		$product_field->visible = (Tools::getIsset('visible') ? (int)Tools::getValue('visible') : 1);
		$product_field->input_type = Tools::getValue('input_type');
		$product_field->save();

		$product_field_options_arr = Tools::jsonDecode(Tools::getValue('ppbs_product_field_options'));
		if (!empty($product_field_options_arr) && !empty($product_field->id_ppbs_product_field))
		{
			$position = 0;
			PPBSProductFieldOption::deleteAllByProductFieldID($product_field->id_ppbs_product_field);
			foreach ($product_field_options_arr as $option)
			{
				$ppbs_product_field_option = new PPBSProductFieldOption();
				$ppbs_product_field_option->id_ppbs_product_field = $product_field->id_ppbs_product_field;
				$ppbs_product_field_option->text = pSQL($option->text);
				$ppbs_product_field_option->value = (float)$option->value;
				$ppbs_product_field_option->position = (float)$position;
				$ppbs_product_field_option->add(false);
				$position++;
			}
		}
	}

	public function processPositions()
	{
		$position = 0;
		if (is_array(Tools::getValue('ids_ppbs_product_field')))
		{
			foreach (Tools::getValue('ids_ppbs_product_field') as $id_ppbs_product_field)
			{
				PPBSProductField::updatePosition($id_ppbs_product_field, $position);
				$position++;
			}
		}
	}

	public function processDelete()
	{
		if (Tools::getValue('id_ppbs_product_field') != '')
		{
			$field = new PPBSProductField(Tools::getValue('id_ppbs_product_field'));
			$field->delete();
		}
	}

	public function processVisibility()
	{
		if (Tools::getValue('id_ppbs_product_field') != '')
		{
			$field = new PPBSProductField(Tools::getValue('id_ppbs_product_field'));
			$field->visible = (int)Tools::getValue('state');
			$field->update();
		}
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderaddform' :
				die($this->renderAddForm());

			case 'processaddform' :
				$this->processAddForm();
				die;

			case 'processpositions' :
				die($this->processPositions());

			case 'processdelete' :
				die($this->processDelete());

			case 'processvisibility' :
				die($this->processVisibility());

			case 'renderlist':
				die($this->renderList());
		}

		$this->renderList();
	}

}