<?php


class PPBSAdminTabController extends PPBSControllerCore
{
	public $module_folder = 'productpricebysize';

	/** @var ProductPriceBySize */
	protected $sibling;
	protected $module_base_url;
	protected $_ps15_field_suffix;

	/** @var PPBSAdminTabUnitController */
	protected $admin_tab_unit_controller;

	protected $key_tab = 'ModuleProductpricebysize';
	protected $base_url = '';

	public function __construct($sibling)
	{
		$this->sibling = $sibling;
		$this->set_module_base_admin_url();
		//this->base_url = _PS_BASE_URL_.__PS_BASE_URI__;
		$this->base_url = Tools::getShopProtocol().Tools::getShopDomain().__PS_BASE_URI__;

		$this->_addJS();
		$this->_addCSS();

		if (_PS_VERSION_ < 1.6) $this->_ps15_field_suffix = '_on';
		else $this->_ps15_field_suffix = '';
		if (Tools::getValue('do') != '' && Tools::getValue('configure') == '') $this->route();
	}

	private function _addJS()
	{
		Context::getContext()->controller->addJS($this->base_url.'/modules/'.$this->module_folder.'/views/js/admin/lib/popup.js');
		Context::getContext()->controller->addJS($this->base_url.'/modules/'.$this->module_folder.'/views/js/admin/lib/tools.js');
		Context::getContext()->controller->addJS($this->base_url.'/modules/'.$this->module_folder.'/views/js/admin/producttab/general.js');
		Context::getContext()->controller->addJS($this->base_url.'/modules/'.$this->module_folder.'/views/js/admin/producttab/fields.js');
		Context::getContext()->controller->addJS($this->base_url.'/modules/'.$this->module_folder.'/views/js/admin/producttab/arearanges.js');
		Context::getContext()->controller->addJS($this->base_url.'/modules/'.$this->module_folder.'/views/js/admin/producttab/equation.js');
	}

	private function _addCSS()
	{
		Context::getContext()->controller->addCSS($this->base_url.'/modules/'.$this->module_folder.'/views/css/admin/producttab/tools.css');
		Context::getContext()->controller->addCSS($this->base_url.'/modules/'.$this->module_folder.'/views/css/admin/producttab/popup.css');
		Context::getContext()->controller->addCSS($this->base_url.'/modules/'.$this->module_folder.'/views/css/admin/producttab/fields_add.css');
		Context::getContext()->controller->addCSS($this->base_url.'/modules/'.$this->module_folder.'/views/css/admin/producttab/equation.css');
	}

	/* Protected Methods */

	protected function set_module_base_admin_url()
	{
		if (!defined('_PS_ADMIN_DIR_'))
		{
			$this->admin_mode = false;
			return false;
		}
		else
		{
			$this->admin_mode = true;
			$arr_temp = explode('/', _PS_ADMIN_DIR_);
			if (count($arr_temp) <= 1) $arr_temp = explode('\\', _PS_ADMIN_DIR_);
			$dir_name = end($arr_temp);
			$this->module_base_url = $dir_name.'/index.php?controller=AdminProducts&id_product='.Tools::getValue('id_product').'&updateproduct&token='.Tools::getAdminTokenLite('AdminProducts');
		}
	}

	public function hookActionProductSave($params)
	{
		$ppbs = PPBSModel::Load_ppbs_product(Tools::getValue('id_product'), Context::getContext()->shop->id);
		if (!empty($ppbs->enabled) && $ppbs->enabled == 1)
			PPBSModel::createProductCustomization(Tools::getValue('id_product'), true, Context::getContext()->shop->id);
		else
			PPBSModel::createProductCustomization(Tools::getValue('id_product'), false, Context::getContext()->shop->id);
	}

	public function route()
	{
		switch (Tools::getValue('route'))
		{
			case 'admin_tab_unit' :
				$this->admin_tab_unit_controller = new PPBSAdminTabUnitController($this->sibling);
				return $this->admin_tab_unit_controller->route();
				break;

			case 'ppbsadmintabfields' :
				$fields_controller = new PPBSAdminTabFieldsController($this->sibling);
				return $fields_controller->route();
				break;

			case 'ppbsadmintabarearanges' :
				$ranges_controller = new PPBSAdminTabAreaRangesController($this->sibling);
				return $ranges_controller->route();
				break;

			case 'ppbsadmintabgeneral' :
				$general_controller = new PPBSAdminTabGeneralController($this->sibling);
				return $general_controller->route();
				break;

			case 'ppbsadmintabequation' :
				$equation_controller = new PPBSAdminTabEquationController($this->sibling);
				return $equation_controller->route();
				break;

		}

		$general_controller = new PPBSAdminTabGeneralController($this->sibling);
		$fields_controller = new PPBSAdminTabFieldsController($this->sibling);
		$equation_controller = new PPBSAdminTabEquationController($this->sibling);
		$ranges_controller = new PPBSAdminTabAreaRangesController($this->sibling);

		$ret = '';
		$ret .= $general_controller->renderForm();
		$ret .= $fields_controller->renderList();
		$ret .= $equation_controller->render();
		$ret .= $ranges_controller->renderList();

		return $ret;
	}

}