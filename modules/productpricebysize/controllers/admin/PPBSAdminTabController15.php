<?php

class PPBSAdminTabController15 extends PPBSAdminTabController
{
	protected $helper;

	public function renderMinMaxForm()
	{
		$product_units = PPBSModel::getProductUnits(Tools::getValue('id_product'), Context::getContext()->language->id, true, Context::getContext()->shop->id);
		$fields = array();
		$fields[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Minimum and Maximum Values users can enter'),
				'icon' => 'icon-cogs'
			)
		);
		foreach ($product_units as $key => $unit)
		{
			$input_min = array();
			$input_max = array();

			$input_min['label'] = 'Min '.$unit->name.' :';
			$input_min['name'] = 'unit_min_'.$unit->id_ppbs_unit;
			$input_min['type'] = 'text';
			$input_min['class'] = 'fixed-width-xs';

			$input_max['label'] = 'Max '.$unit->name.' :';
			$input_max['name'] = 'unit_max_'.$unit->id_ppbs_unit;
			$input_max['type'] = 'text';
			$input_max['class'] = 'fixed-width-xs';

			$fields['0']['form']['input'][] = $input_min;
			$fields['0']['form']['input'][] = $input_max;
		}

		$fields['0']['form']['submit'] = array(
			'title' => $this->l('Save'),
			'class' => 'btn btn-default pull-right',
			'name' => 'submit_'
		);

		/* Populate fields */
		foreach ($product_units as $key => $unit)
		{
			$this->helper->tpl_vars['fields_value']['unit_min_'.$unit->id_ppbs_unit] = $unit->min;
			$this->helper->tpl_vars['fields_value']['unit_max_'.$unit->id_ppbs_unit] = $unit->max;
		}

		$this->helper->currentIndex = $this->sibling->context->link->getAdminLink('AdminProducts', false).'&id_product='.Tools::getValue('id_product').'&updateproduct&do=form_ppbs_restraints_process';
		return $fields;
	}

	public function renderAreaPriceForm()
	{
		$fields = array();
		$fields['0']['form'] = array(
			'legend' => array(
				'title' => $this->l('Add Area Based Price'),
				'icon' => 'icon-calculator'
			),
			'input' => array(
				array(
					'name' => 'id_product',
					'type' => 'hidden'
				),
				array(
					'name' => 'area_low',
					'type' => 'text',
					'label' => 'From Area',
					'class' => 'fixed-width-xs',
				),
				array(
					'name' => 'area_high',
					'type' => 'text',
					'label' => 'To Area',
					'class' => 'fixed-width-xs'
				),
				array(
					'name' => 'impact',
					'type' => 'select',
					'label' => 'Impact on price',
					'class' => 'fixed-width-xs',
					'options' => array(
						'query' => array(
							array(
								'id_option' => '+',
								'name' => '+'
							),
							array(
								'id_option' => '-',
								'name' => '-'
							),
							array(
								'id_option' => '=',
								'name' => '='
							)
						),
						'id' => 'id_option',
						'name' => 'name'
					)
				),
				array(
					'name' => 'price',
					'type' => 'text',
					'label' => 'Price',
					'class' => 'fixed-width-xs'
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'btn btn-default pull-right',
				'name' => 'submit_',
			)
		);
		//$this->helper->show_toolbar = true;
		$this->helper->title = 'Area Based Prices';

		$this->helper->submit_action = 'submit_areaprices';
		$this->helper->currentIndex = $this->sibling->context->link->getAdminLink('AdminProducts', false).'&id_product='.Tools::getValue('id_product').'&updateproduct&do=form_ppbs_areaprices_process';

		/* Populate the form */
		$this->helper->tpl_vars['fields_value']['id_product'] = (int)Tools::getValue('id_product');
		$this->helper->tpl_vars['fields_value']['area_low'] = 0;
		$this->helper->tpl_vars['fields_value']['area_high'] = 0;
		$this->helper->tpl_vars['fields_value']['impact'] = '-';
		$this->helper->tpl_vars['fields_value']['price'] = '0.00';
		return $fields;
	}

	public function renderForm()
	{
		$ppbs = PPBSModel::Load_ppbs_product(Tools::getValue('id_product'), Context::getContext()->shop->id);
		$units = PPBSUnit::getUnitsList(Context::getContext()->language->id, Context::getContext()->shop->id);

		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		$fields_form = array();

		$fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Product Price By Size Options'),
				'icon' => 'icon-cogs'
			),
			'input' => array(
				array(
					'name'  => 'enabled',
					'type'  => 'checkbox',
					'is_bool' => 'true',
					'values'    => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('Enabled for this product'),
								'val' => '1',
								'checked' => 'checked'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
				),
				array(
					'name'  => 'attribute_price_as_area_price',
					'type'  => 'checkbox',
					'is_bool' => 'true',
					'hint' => 'If enable, the attribute price will be included in the total area price. If disabled attribute price will simply be added on the area price',
					'values'    => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('Use attribute price in area calculation?'),
								'val' => '1',
								'checked' => 'checked'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
				),
				array(
					'name' => 'min_price',
					'label' => 'Charge Minimum price (Ex Tax):',
					'type' => 'text'
				),
				array(
					'name' => 'id_product',
					'type' => 'hidden'
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'btn btn-default pull-right',
				'name' => 'submit_',
			)
		);

		/* Output a list of all available units for display */
		foreach ($units as $unit)
		{
			$input = array();

			$input['name'] = 'unit_'.$unit->id_ppbs_unit;
			$input['type'] = 'checkbox';
			$input['is_bool'] = 'true';
			$input['label'] = '';
			$input['values'] = array(
				'query' => array(
					array(
						'id' => 'on',
						'name' => $unit->display_name,
						'val' => '1',
						'checked' => 'checked'
					),
				),
				'id' => 'id',
				'name' => 'name'
			);
			$fields_form['0']['form']['input'][] = $input;
		}


		$this->helper = new HelperForm();
		$this->helper->module = $this->sibling;
		$this->helper->identifier = $this->identifier;
		foreach (Language::getLanguages(false) as $lang)
			$this->helper->languages[] = array(
				'id_lang' => $lang['id_lang'],
				'iso_code' => $lang['iso_code'],
				'name' => $lang['name'],
				'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
			);
		$this->helper->currentIndex = $this->sibling->context->link->getAdminLink('AdminProducts', false).'&id_product='.Tools::getValue('id_product').'&updateproduct&do=form_ppbs_process';
		$this->helper->default_form_language = $default_lang;
		$this->helper->allow_employee_form_lang = $default_lang;
		$this->helper->toolbar_scroll = false;
		$this->helper->show_toolbar = false;
		$this->helper->title = $this->displayName;
		$this->helper->submit_action = 'submit_generic';
		$this->helper->token = Tools::getAdminTokenLite('AdminProducts');
		$this->helper->table = 'carriers';

		if (isset($ppbs->enabled)) $enabled = $ppbs->enabled;
		else $enabled = 0;

		$ps15_suffix = '_on';

		$this->helper->tpl_vars = array(
			'fields_value' => array(
				'enabled'.$ps15_suffix => $enabled,
				'min_price' => $ppbs->min_price,
				'id_product' => Tools::getValue('id_product'),
			)
		);

		/* Populate the enabled units */
		$product_units = PPBSModel::getProductUnits(Tools::getValue('id_product'), Context::getContext()->language->id, true, Context::getContext()->shop->id);
		foreach ($product_units as $key => $unit)
			$this->helper->tpl_vars['fields_value']['unit_'.$unit->id_ppbs_unit.$ps15_suffix] = $unit->visible;

		/* Other forms */
		$tab_content = $this->helper->generateForm($fields_form);

		return $tab_content;
	}

}