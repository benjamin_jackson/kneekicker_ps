<?php

class PPBSConfigControllerUnits extends PPBSControllerCore
{
	public $table_dimension = 'ppbs_unit';
	public $sibling;

	public function renderUnitsList()
	{
		$unit = new PPBSUnit();
		$units = $unit->getUnits();

		$fields_list = array(
			'name' => array(
				'title' => $this->l('Name'),
				'type' => 'text',
			),
			'symbol' => array(
				'title' => $this->l('Symbol'),
				'type' => 'text'
			),
		);

		$this->setupHelperList('Units');

		$this->helper_list->identifier = 'id_ppbs_unit';
		$this->helper_list->table = 'ppbs_unit';
		$this->helper_list->show_toolbar = true;
		$this->helper_list->simple_header = false;

		$this->helper_list->currentIndex .= '&route=ppbsconfigconrtollerunits';

		$this->helper_list->toolbar_btn = array(
			'new' => array(
				'desc' => $this->l('Create a new Unit'),
				'href' => $this->helper_list->currentIndex.'&action=renderaddform&token='.$this->helper_list->token,
			)
		);

		return $this->helper_list->generateList($units, $fields_list);
	}

	public function renderAddForm()
	{
		$this->setupHelperForm();
		$fields = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Add / Edit Unit'),
					'icon' => 'icon-list'
				),
				'input' => array(
					array(
						'name' => 'id_ppbs_unit',
						'type' => 'hidden'
					),
					array(
						'label' => 'Name',
						'type' => 'text',
						'name' => 'name',
						'lang' => false,
						'required' => true
					),
					array(
						'label' => 'Symbol',
						'type' => 'text',
						'name' => 'symbol',
						'lang' => true,
						'required' => true
					)
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-left',
					'icon' => 'icon-list',
					'name' => 'submit_',
				)
			),
		);
		$this->helper_form->currentIndex .= '&route=ppbsconfigconrtollerunits&action=processaddform';

		if (Tools::getValue('id_ppbs_unit') != '')
			$unit = new PPBSUnit(Tools::getValue('id_ppbs_unit'));

		/* Populate the form */
		if (!empty($unit->id))
		{
			$this->helper_form->fields_value['id_ppbs_unit'] = $unit->id_ppbs_unit;
			$this->helper_form->fields_value['name'] = $unit->name;
			$this->helper_form->fields_value['symbol'] = $unit->symbol;
		}
		else
		{
			$this->helper_form->fields_value['id_ppbs_unit'] = '';
			$this->helper_form->fields_value['name'] = '';
			$this->helper_form->fields_value['symbol'] = '';
		}
		return $this->helper_form->generateForm(array($fields));
	}

	public function processAddForm()
	{
		$unit = new PPBSUnit(Tools::getValue('id_ppbs_unit'));
		$languages = $this->sibling->context->controller->getLanguages();
		$unit->name = pSQL(Tools::getValue('name'));

		foreach ($languages as $key => $language)
			$unit->symbol[$language{'id_lang'}] = Tools::getValue('symbol_'.$language{'id_lang'});

		$unit->save(false, false);
	}

	public function processDelete()
	{
		DB::getInstance()->delete('ppbs_unit', 'id_ppbs_unit='.(int)Tools::getValue('id_ppbs_unit'));
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderaddform' :
				return $this->renderAddForm();
				break;
			case 'processaddform' :
				$this->processAddForm();
				$this->redirect();
				break;
		}

		if (Tools::getIsset('updateppbs_unit'))
			return $this->renderAddForm();

		if (Tools::getIsset('deleteppbs_unit'))
		{
			$this->processDelete();
			$this->redirect();
		}
	}

}