<?php

class PPBSConfigController extends PPBSControllerCore
{
	public $table_unit = 'ppbs_unit';
	public $sibling;

	public function route()
	{
		$return = '';

		$form_add = new PPBSConfigControllerAdd($this->sibling);

		if (Tools::getValue('route') != '')
		{
			switch (Tools::getValue('route'))
			{
				case 'ppbsconfigconrtollerunits' :
					$unit_controller = new PPBSConfigControllerUnits($this->sibling);
					return $unit_controller->route();
					break;
			}
		}

		if (Tools::isSubmit('deleteppbs_dimension')) $this->deleteDimension();
		if (Tools::isSubmit('updateppbs_dimension')) return $form_add->renderForm();

		switch (Tools::getValue('do'))
		{
			case 'form_add':
				return $form_add->renderForm();
			case 'form_add_process' :
				$form_add->formAddProcess();
				return $this->renderDimensionList();
			case 'save_translations':
				$this->save_translations();
			default:
				$unit_controller = new PPBSConfigControllerUnits($this->sibling);
				$return .= $this->renderDimensionList();
				$return .= $unit_controller->renderUnitsList();
				$return .= $this->render_translations_form();
				return $return;
		}
	}

	public function deleteDimension()
	{
		$id_ppbs_dimension = (int)Tools::getValue('id_ppbs_dimension');
		$dimension = new PPBSDimension();
		$dimension->id = $id_ppbs_dimension;
		$dimension->delete();
		/* @todo: delete all relationships to this entity */
	}


	private function _parse_translation_from_form($name, &$ppbs_translation_collection)
	{
		$ppbs_translation = new TPPBSTranslation();
		$ppbs_translation->name = $name;
		$ppbs_translation_collection[$name] = $ppbs_translation;
		$languages = $this->sibling->context->controller->getLanguages();

		foreach ($languages as $value)
		{
			$id_lang = $value['id_lang'];
			$ppbs_translation->text[$id_lang] = Tools::getValue($name.'_'.$id_lang);
		}
	}

	public function save_translations()
	{
		$ppbs_translation_collection = array();
		$this->_parse_translation_from_form('min_max_error', $ppbs_translation_collection);
		$this->_parse_translation_from_form('generic_error', $ppbs_translation_collection);
		$this->_parse_translation_from_form('unit_price_suffix', $ppbs_translation_collection);
		PPBSModel::save_translations($ppbs_translation_collection, Context::getContext()->shop->id);
	}

	public function render_translations_form()
	{
		$fields = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('translations'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Min/Max Error'),
						'name' => 'min_max_error',
						'lang' => true,
						'required' => true,
						'size' => '',
						'col' => ''
					),
					array(
						'type' => 'text',
						'label' => $this->l('Generic Error'),
						'name' => 'generic_error',
						'lang' => true,
						'col' => '',
						'size' => 128,
						'required' => true
					),
					array(
						'type' => 'text',
						'label' => $this->l('Price / Unit Text'),
						'name' => 'unit_price_suffix',
						'lang' => true,
						'required' => true,
						'size' => 128,
						'col' => ''
					)
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submit_config_translations',
				)
			),
		);

		$helper = new HelperForm();
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this->sibling;
		//$helper->submit_action = 'submit_restraints';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->sibling->name.'&do=save_translations';

		$languages = $this->sibling->context->controller->getLanguages();

		$helper->tpl_vars = array(
			'languages' => $languages,
			'id_language' => $this->sibling->context->language->id
		);

		$translations = PPBSModel::load_translations(Context::getContext()->shop->id);

		foreach ($languages as $language)
		{
			if (isset($translations['min_max_error'][$language{'id_lang'}]))
				$helper->tpl_vars['fields_value']['min_max_error'][$language{'id_lang'}] = $translations['min_max_error'][$language{'id_lang'}]->text;
			else
				$helper->tpl_vars['fields_value']['min_max_error'][$language{'id_lang'}] = '';

			if (isset($translations['generic_error'][$language{'id_lang'}]))
				$helper->tpl_vars['fields_value']['generic_error'][$language{'id_lang'}] = $translations['generic_error'][$language{'id_lang'}]->text;
			else
				$helper->tpl_vars['fields_value']['generic_error'][$language{'id_lang'}] = '';

			if (isset($translations['unit_price_suffix'][$language{'id_lang'}]))
				$helper->tpl_vars['fields_value']['unit_price_suffix'][$language{'id_lang'}] = $translations['unit_price_suffix'][$language{'id_lang'}]->text;
			else
				$helper->tpl_vars['fields_value']['unit_price_suffix'][$language{'id_lang'}] = '';

		}
		return $helper->generateForm(array($fields));
	}

	public function renderDimensionList()
	{
		$this->initPageHeaderToolbar();
		$id_lang = Context::getContext()->language->id;
		$dimensions = PPBSDimension::getDimensions($id_lang, Context::getContext()->shop->id);

		$this->fields_list = array(
			'name' => array(
				'title' => $this->l('Internal Name'),
				'width' => 140,
				'type' => 'text',
			),
			'display_name' => array(
				'title' => $this->l('Display Name'),
				'width' => 140,
				'type' => 'text',
			)
		);
		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->simple_header = false;

		$helper->show_toolbar = true;
		$helper->toolbar_btn['new'] = array(
			'href' => $this->sibling->context->link->getAdminLink('AdminModules').'&configure='.$this->sibling->name.'&module_name='.$this->sibling->name.'&do=form_add',
			'desc' => $this->l('Add New Unit', null, null, false)
		);

		// Actions to be displayed in the "Actions" column
		$helper->actions = array('edit', 'delete');

		$helper->identifier = 'id_ppbs_dimension';
		$helper->show_toolbar = true;
		$helper->title = 'Dimension fields';
		$helper->table = 'ppbs_dimension';

		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->sibling->name;
		$return = $helper->generateList($dimensions, $this->fields_list);
		return $return;
	}

	public function initPageHeaderToolbar()
	{
		/*if (empty($this->sibling->display))
			$this->sibling->page_header_toolbar_btn['new_language'] = array(
				'href' => AdminController::$currentIndex.'&addlang&token='.Tools::getAdminTokenLite('AdminModules'),
				'desc' => $this->l('Add new language', null, null, false),
				'icon' => 'process-icon-new'
			);*/
	}

}