<?php

class PPBSUnitDropdownConfigWidget
{
	private $sibling;
	private $module_folder = 'productpricebysize';

	public function __construct($sibling)
	{
		$this->sibling = $sibling;
	}

	public function render()
	{
		Context::getContext()->controller->addJS(_PS_BASE_URL_.'/modules/'.$this->module_folder.'/views/js/admin/form/dropdownconfigwidget.js');
		Context::getContext()->controller->addCSS(_PS_BASE_URL_.'/modules/'.$this->module_folder.'/views/css/admin/form/dropdownconfigwidget.css');

		Context::getContext()->smarty->assign(array(
			'test' => 'test is ok',
		));
		return $this->sibling->display(_PS_MODULE_DIR_.$this->module_folder, 'views/templates/admin/form/dropdownconfigwidget.tpl');
	}

}