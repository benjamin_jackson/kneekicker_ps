<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
require_once(_PS_MODULE_DIR_.'/productpricebysize/lib/bootstrap.php');

/* route to the correct controller */
if (Tools::strtolower(Tools::getValue('controller')) == 'ppbscartcontroller')
{
	$ppbs_cart_controller = new PPBSCartController();
	print $ppbs_cart_controller->route();
	die;
}


$null = null;
$ppbs_front = new PPBSFrontController($null);

if (Tools::getValue('action') == 'get_cart_totals')
{
	print Tools::jsonEncode($ppbs_front->getCartTotals());
	die;
}
else $ppbs_front->addDimensionsToCart();