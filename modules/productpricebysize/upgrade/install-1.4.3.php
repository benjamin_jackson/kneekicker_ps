<?php
if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/productpricebysize/lib/bootstrap.php');

function upgrade_module_1_4_3($object)
{
	$return = true;
	$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ppbs_unit_lang` (
			`id_ppbs_unit` int(10) unsigned NOT NULL,
			`id_lang` smallint(5) unsigned NOT NULL,
			`symbol` varchar(32) NOT NULL,
			PRIMARY KEY (`id_ppbs_unit`,`id_lang`)
		)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');
	PPBSInstall::addColumn('ppbs_product', 'setup_fee', 'decimal(10,6) unsigned DEFAULT "0.000000"');
	return true;
}