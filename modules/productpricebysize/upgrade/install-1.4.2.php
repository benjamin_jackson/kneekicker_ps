<?php
if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/productpricebysize/lib/bootstrap.php');

function upgrade_module_1_4_2($object)
{
	$return = true;
	$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ppbs_equation` (
			`id_equation` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
			`id_product` int(10) unsigned NOT NULL,
			`ipa` int(10) unsigned NOT NULL,
			`equation` varchar(512) NOT NULL,
			PRIMARY KEY (`id_equation`)
		)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');
	return true;
}