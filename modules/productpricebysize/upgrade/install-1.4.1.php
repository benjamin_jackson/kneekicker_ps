<?php
if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/productpricebysize/lib/bootstrap.php');

function upgrade_module_1_4_1($object)
{
	PPBSInstall::addColumn('ppbs_product', 'equation_enabled', 'TINYINT UNSIGNED DEFAULT 0');
	PPBSInstall::addColumn('ppbs_product', 'equation', 'VARCHAR(512) NOT NULL');
	return true;
}