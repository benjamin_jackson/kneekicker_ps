<?php

class Cart extends CartCore
{
	public function deleteProduct($id_product, $id_product_attribute = null, $id_customization = null, $id_address_delivery = 0)
	{
		if (!Module::isEnabled('ProductPriceBySize'))
			parent::deleteProduct($id_product, $id_product_attribute = null, $id_customization = null, $id_address_delivery = 0);

		$result = Hook::exec('ppbsDeleteCartProduct', array(
				'id_product' => $id_product,
				'id_product_attribute' => $id_product_attribute,
				'id_customization' => $id_customization,
				'id_address_delivery' => $id_address_delivery,
			),
			null, false);
		if ($result == false)
			parent::deleteProduct($id_product, $id_product_attribute = null, $id_customization = null, $id_address_delivery = 0);
	}

	public function getProducts($refresh = false, $id_product = false, $id_country = null)
	{
		$products = parent::getProducts($refresh, $id_product, $id_country);
		if (!Module::isEnabled('ProductPriceBySize')) return $products;

		if (_PS_VERSION_ >= 1.6)
		{
			$params = Hook::exec('ppbsGetProducts', array('products'=>$products), null, true);
			if (is_array($params) && isset($params['productpricebysize']['products']))
				return $params['productpricebysize']['products'];
			else
				return $products;
		}
		else
		{
			$params = Hook::exec('ppbsGetProducts', array('products'=>$products), null);
			$params = Tools::jsonDecode($params, true);
			if (isset($params['products']))
				return $params['products'];
			else
				return $products;
		}
	}

	public function getTotalWeight($products = null)
	{
		if (!Module::isEnabled('ProductPriceBySize')) return parent::getTotalWeight($products);

		if (!is_null($products))
		{
			$cart_controller = new PPBSCartController($this);
			return $cart_controller->getTotalPPBSProductWeight($products);
		}
		else
			return parent::getTotalWeight($products);
	}

	public function duplicate()
	{
		$parent = parent::duplicate();
		if (Module::isEnabled('ProductPriceBySize')) PPBSModel::duplicateForReorder($parent, $this->id);
		return $parent;
	}


}