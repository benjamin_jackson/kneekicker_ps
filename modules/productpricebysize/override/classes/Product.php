<?php

class Product extends ProductCore
{
	public static function priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency, $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, &$specific_price, $use_group_reduction, $id_customer = 0, $use_customer_price = true, $id_cart = 0, $real_quantity = 0)
	{
		include_once(_PS_MODULE_DIR_.'/productpricebysize/lib/bootstrap.php');

		$null = null;
		$ppbs_front_controller = new PPBSFrontController($null);

		if ($ppbs_front_controller->shouldShowAreaPrice())
			$decimals = 6;

		$ppbs_product = PPBSModel::load_ppbs_product($id_product, $id_shop);
		if ((!isset($ppbs_product->enabled) || $ppbs_product->enabled == 0) || !Module::isEnabled('ProductPriceBySize'))
			return parent::priceCalculation(
				$id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
				$id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, $specific_price, $use_group_reduction,
				$id_customer, $use_customer_price, $id_cart, $real_quantity
			);

		if (!isset($ppbs_product->attribute_price_as_area_price)) $use_reduc = true;
		else
			if (!$ppbs_product->attribute_price_as_area_price)
				$use_reduc = false;

		$price = parent::priceCalculation(
			$id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
			$id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, $specific_price, $use_group_reduction,
			$id_customer, $use_customer_price, $id_cart, $real_quantity
		);

		if (!$ppbs_front_controller->shouldShowAreaPrice()) return $price;

		if (!$id_cart && isset(Context::getContext()->cart->id)) $id_cart = Context::getContext()->cart->id;

		if ($id_cart)
		{
			$price = Hook::exec('ppbsPriceCalculation', array(
				'price' => $price,
				'id_product' => $id_product,
				'id_product_attribute' => $id_product_attribute,
				'id_cart' => $id_cart,
				'id_shop' => $id_shop,
				'specific_price' => $specific_price,
				'id_country' => $id_country,
				'id_state' => $id_state,
				'zipcode' => $zipcode,
				'use_tax' => $use_tax
			));
		}

		return (float)$price;
	}

	public static function getProductsProperties($id_lang, $query_result)
	{
		static $address = null;
		static $context = null;

		include_once(_PS_MODULE_DIR_.'/productpricebysize/lib/bootstrap.php');
		$results_array = parent::getProductsProperties($id_lang, $query_result);
		$translations_obj = PPBSModel::load_translations();
		$unit_price_suffix = $translations_obj['unit_price_suffix'][Context::getContext()->language->id]->text;

		foreach ($results_array as &$product)
		{
			$ppbs_product = PPBSModel::Load_ppbs_product($product['id_product'], context::getContext()->shop->id);

			if (Module::isEnabled('ProductPriceBySize'))
			{
				/* set up tax caculator */
				if ($address === null)
					$address = new Address();
				$address->id_country = Context::getContext()->country->id;

				if (isset($ppbs_product->enabled) && $ppbs_product->enabled)
				{
					$tax_manager = TaxManagerFactory::getManager($address, Product::getIdTaxRulesGroupByIdProduct((int)$product['id_product'], $context));
					$product_tax_calculator = $tax_manager->getTaxCalculator();

					$product['ppbs_enabled'] = 1;
					$product['ppc_price_suffix'] = $unit_price_suffix;
					$product['ppbs_list_price'] = Tools::displayPrice($product_tax_calculator->addTaxes($product['attribute_price']));
					$product['ppbs_list_price_display'] = Tools::displayPrice($product_tax_calculator->addTaxes($product['attribute_price'])).' '.$unit_price_suffix;
				}
				else
				{
					$product['ppbs_enabled'] = 0;
					$product['ppc_price_suffix'] = '';
				}
			}
			else
			{
				$product['ppbs_enabled'] = 0;
				$product['ppc_price_suffix'] = '';
			}
		}

		return $results_array;
	}
}