<?php

/*
 *
 * Custom Calculator class written for KneeKicker. This class serves the custom calculator page  
 * tpl file.
 *
 * Predict Marketing
 * Ben Jackson
 */

class kneeKickerCalculatorCalculatorModuleFrontController extends ModuleFrontController {
	
	
	/**
	 * Lets set up out template
	 * @return  void
	 */
	public function initContent()
	{
		parent::initContent();
		$this->setTemplate('kneekickercalculator.tpl');
	}

	public function setMedia()
	{
		parent::setMedia();
		$this->addCSS(__PS_BASE_URI__.'modules/'.$this->module->name.'/css/'.$this->module->name.'.css');
		$this->addJS(__PS_BASE_URI__.'modules/'.$this->module->name.'/js/'.$this->module->name.'.js');
	}

}