/*
 * Set up the page
 */
$(window).load(function() {
	$("#l_room_dimensions").hide();
	$("#bay_window_dimensions").hide();
	$("#room_measurements_c").hide();
	$("#room_measurements_d").hide();
	$('.roomDoorways').hide();
});

$(document).ready(function($) {
	
		// Defaults
		var roomShape = 'square';
		var flooringCost = '0';
		var gripper_rods ='no';
		var room_doorways ='1';
		var door_bars = 'no';
		var room_measurements_a = '1';
		var room_measurements_b = '1';
		var room_measurements_c = '1';
		var room_measurements_d = '1';
		var underlay = 'carpet_underlay';


		/*
		 * Listen for the choice of room shape and update the form
		 * 
		 */

		$("#rectangle_room_choice").click(function() {			
			if($('#rectangle_room_choice').is(':checked')){ deSelectStairs(); selectRectangle(); }
		});

		$("#square_room_choice").click(function() {			
			if($('#square_room_choice').is(':checked')) { deSelectStairs(); selectSquare(); }
		});

		$("#l_shaped_room_choice").click(function() {
			if($('#l_shaped_room_choice').is(':checked')) { deSelectStairs(); selectLShaped(); }
		});

		$("#bay_window_room_choice").click(function() {
			if($('#bay_window_room_choice').is(':checked')) { deSelectStairs(); selectBayWindow(); }
		});

		$("#hall_stairs_landing_room_choice").click(function() {
			if($('#hall_stairs_landing_room_choice').is(':checked')) { selectStairs(); selectHallStairs(); }
		});

		$("#stairs_landing_room_choice").click(function() {
			if($('#stairs_landing_room_choice').is(':checked')) { selectStairs(); selectStairsLanding(); }
		});

		/*
		 * Add doorway number choice if Doorbars is selected true
		 */
		
		$("input[name=door_bars]").change(function(){
			if($('#door_bar_yes').is(':checked')) { showDoorways(); }
				else hideDoorways();
		});

		function showDoorways() {
			$('.roomDoorways').show();
		}

		function hideDoorways() {
			$('.roomDoorways').hide();
		}

		
		/*
		 * Remove selected class from images if switched
		 */
		
		function removeActiveClassFromImages() {
			$('#rectangle_image').removeClass('selected');
			$('#square_image').removeClass('selected');
			$('#l_image').removeClass('selected');
			$('#bay_image').removeClass('selected');
			$('#hall_image').removeClass('selected');
			$('#landing_image').removeClass('selected');
			$('#hall_image').removeClass('selected');
			$('#landing_image').removeClass('selected');
		}

		/*
		 * Display message if Stairs or landing are chosen
		 */
		
		function selectStairs() {
			$('.flooringCost').hide();
			$('.gripperRods').hide();
			$('.roomDoorways').hide();
			$('.doorBars').hide();
			$('.roomMeasurements').hide();
			$('.roomUnderlay').hide();
			$('#calculate_submit').hide();
			$('.displayMessage').show();
		}

		/*
		 * Make sure the main form content is displayed again
		 *  if the stairs are chosen and then switched back to a
		 *  main shape
		 */
		
		function deSelectStairs()
		{
			$('.flooringCost').show();
			$('.gripperRods').show();
			$('.roomDoorways').show();
			$('.doorBars').show();
			$('.roomMeasurements').show();
			$('.roomUnderlay').show();
			$('#calculate_submit').show();
			$('.displayMessage').hide();
		}


		/*
		 * Update room measurements option based on room type selection
		 */
		function selectSquare() {
			removeActiveClassFromImages();
			$('#square_image').addClass('selected');
			$("#square_room_dimensions").show();
			$("#l_room_dimensions").hide();
			$("#bay_window_dimensions").hide();
			$("#room_measurements_c").hide();
			$("#room_measurements_d").hide();
			roomShape = 'square';
		}

		function selectRectangle() {
			removeActiveClassFromImages();
			$('#rectangle_image').addClass('selected');
			$("#square_room_dimensions").show();
			$("#l_room_dimensions").hide();
			$("#bay_window_dimensions").hide();
			$("#room_measurements_c").hide();
			$("#room_measurements_d").hide();
			roomShape = 'rectangle';
		}

		function selectLShaped() {
			removeActiveClassFromImages();
			$('#l_image').addClass('selected');
			$("#square_room_dimensions").hide();
			$("#l_room_dimensions").show();
			$("#bay_window_dimensions").hide();
			$("#room_measurements_c").show();
			$("#room_measurements_d").show();
			roomShape = 'l_shaped';
		}

		function selectBayWindow() {
			removeActiveClassFromImages();
			$('#bay_image').addClass('selected');
			$("#square_room_dimensions").hide();
			$("#l_room_dimensions").hide();
			$("#bay_window_dimensions").show();
			$("#room_measurements_c").show();
			$("#room_measurements_d").show();
			roomShape = 'bay_window';
		}

		function selectHallStairs() {
			removeActiveClassFromImages();
			$('#hall_image').addClass('selected');
		}

		function selectStairsLanding() {
			removeActiveClassFromImages();
			$('#landing_image').addClass('selected');
		}

		function getFlooringCost() {
			flooringCost = $('#flooring_cost').val();
		}

		function getGripperRodChoice() {
			if($('#gripper_yes').is(':checked')) { gripper_rods = 'yes'}
				else gripper_rods = 'no';
		}
		function getDoorwayNumber() {
			room_doorways = '1';
			if($('#room_doorway_2').is(':checked')) { room_doorways = '2' }
			if($('#room_doorway_3').is(':checked')) { room_doorways = '3' }
		}
		function getDoorBars() {
			if($('#door_bar_yes').is(':checked')) { door_bars = 'yes'}
				else door_bars = 'no';
		}
		function getRoomMeasurements() {
			room_measurements_a = $('#room_measurement_a').val();
			room_measurements_b = $('#room_measurement_b').val();
			room_measurements_c = $('#room_measurement_c').val();
			room_measurements_d = $('#room_measurement_d').val();
		}

		function getUnderlay() {
			underlay = $('#room_underlay').val();
		}

		/**
		 * Prevent default submit action
		 * Call our calculate method to get the price
		 */
		
		$('form').submit(function(e){
			e.preventDefault();
			getFormChoices();
			Calculate();
		})

		/**
		 * Get Form choices
		 */
		
		function getFormChoices() {
			getFlooringCost();
			getGripperRodChoice();
			getDoorwayNumber();
			getDoorBars();
			getRoomMeasurements();
			getUnderlay();
		}

		/**
		 * Function to call our calculator class and get our price
		 * Update the form with the price asynchronously
		 */
		
		function Calculate() {
			$.ajax({
					type: 'GET',
					url: baseDir + 'modules/kneekickercalculator/ajax.php',
					headers: { "cache-control": "no-cache" },
					async: true,
					cache: false,
					data: 'method=calculate&room_shape=' + roomShape + '&flooring_cost=' + flooringCost + '&gripper_rods=' + gripper_rods + '&room_doorways=' 
					+ room_doorways + '&door_bars=' + door_bars + '&room_measurements_a=' + room_measurements_a + '&room_measurements_b=' + room_measurements_b +
					'&room_measurements_c=' + room_measurements_c + '&room_measurements_d=' + room_measurements_d + '&underlay=' + underlay,
					success: function(data)
					{
						console.log(data);
						var numbers = jQuery.parseJSON(data);
						$('#underlay_final_cost').text(numbers.underlayCost);
						$('#gripper_rods_final_cost').text(numbers.gripperRods);
						$('#door_bars_final_cost').text(numbers.doorBars);
						$('#flooring_final_cost').text(numbers.flooringCost);
						$('#total_final_size').text(numbers.floorArea);
						$('#total_final_price').text(numbers.totalCost);							
					}
				});
			
		}

});

