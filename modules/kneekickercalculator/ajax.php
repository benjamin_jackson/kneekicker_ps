<?php
require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');

switch (Tools::getValue('method')) {
  case 'calculate' :
    die(calculatePrice());
    break;
  default:
    exit;
}
exit;
/*
 * Prestashop Documentation recommends calling php functions
 * using ajax with this procedural approach.
 * 
 */
function calculatePrice()
{
		$roomShape = Tools::getValue('room_shape');
		$flooringCost = Tools::getValue('flooring_cost');
		$gripperRods = Tools::getValue('gripper_rods');
		$roomDoorways = Tools::getValue('room_doorways');
		$doorbars = Tools::getValue('door_bars');
		$roomMeasurementA = Tools::getValue('room_measurements_a');
		$roomMeasurementB = Tools::getValue('room_measurements_b');
		$roomMeasurementC = Tools::getValue('room_measurements_c');
		$roomMeasurementD = Tools::getValue('room_measurements_d');
		$underlay = Tools::getValue('room_underlay');
			
		/*
		 * Rounding up the smallest input to 4/5
		 * Number stays the same if over
		 */
		
		$minValue = min($roomMeasurementA, $roomMeasurementB);

		if ($minValue == $roomMeasurementA)
		{
			if( $roomMeasurementA < 4)
			{
				$roomMeasurementA = 4;
			}
			if ($roomMeasurementA > 4 && $roomMeasurementA < 5)
			{
				$roomMeasurementA = 5;
			}
		}
		
		else if ($minValue == $roomMeasurementB)
		{
			if( $roomMeasurementB < 4)
			{
				$roomMeasurementB = 4;
			}
			if ($roomMeasurementB > 4 && $roomMeasurementB < 5)
			{
				$roomMeasurementB = 5;
			}
		}


		/*
		 * Initial flooring cost = Area * flooring Cost
		 */
		
		$floorArea = 0;

		if ($roomShape == "rectangle" || $roomShape == "square" || $roomShape == "bay_window")
		{
			$floorArea = $roomMeasurementA * $roomMeasurementB;
		}

		if ($roomShape == "l_shaped" )
		{
			$squareArea = $roomMeasurementA * $roomMeasurementB;
			$minusArea = ($roomMeasurementA - $roomMeasurementC) * ($roomMeasurementB - $roomMeasurementD);
			$floorArea = $squareArea - $minusArea;
		}

		$initialFlooringCost = $floorArea * $flooringCost;

		/*
		 * Gripper rods are £1 per 1m, and wrap the perimeter
		 * Add perimeter £ value to cost 
		 */
		$perimeter = 0;

		if ($gripperRods == "yes")
		{
			$perimeter = ($roomMeasurementA * 2) + ($roomMeasurementB * 2);
		}

		$costWithGripperRods = $initialFlooringCost + $perimeter;

		/*
		 * Door bars are £3.99 per door * x number of doors
		 */
		$doorbarCost = 0;
		
		if ($doorbars == "yes")
		{
			$doorbarCost = $roomDoorways * 3.99;
		}

		$costWithDoorBars = $costWithGripperRods + $doorbarCost;

		/*
		 * Add the underlay cost
		 */
		
		$budgetUnderlay = 3;
		$goodUnderlay = 4;
		$betterUnderlay = 5;
		$bestUnderlay = 7;
		$underlayCost = 0;
		
		switch ($underlay) {
			case 'budget_underlay':
				$underlayCost = $floorArea * $budgetUnderlay;
				break;
			case 'good_underlay':
				$underlayCost = $floorArea * $goodUnderlay;
				break;
			case 'better_underlay':
				$underlayCost = $floorArea * $betterUnderlay;
				break;
			case 'best_underlay':
				$underlayCost = $floorArea * $bestUnderlay;
				break;			
			default:
				$underlayCost = $floorArea * $budgetUnderlay; // default to budget
				break;
		}

		$totalCost = $costWithDoorBars + $underlayCost;

		$finalCost = array('floorArea' => $floorArea, 'underlayCost' => $underlayCost, 'gripperRods' => $perimeter, 'doorBars' => $doorbarCost, 'flooringCost' => $initialFlooringCost, 'totalCost' => $totalCost);
		
		return Tools::jsonEncode($finalCost);

}