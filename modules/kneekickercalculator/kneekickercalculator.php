<?php
/**
 *  The Calculator module for KneeKicker
 *
 *  Predict Marketing 
 *  Ben Jackson
 */

if (!defined('_PS_VERSION_'))
	exit;

class kneekickercalculator extends Module
{	
	public function __construct()
	{
		$this->name = 'kneekickercalculator';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Predict Marketing';
		$this->need_instance = 0;
		$this->bootstrap = true;
		
		parent::__construct();

		$this->displayName = $this->l('KneeKicker Calculator');
		$this->description = $this->l('Adds a calculator to products and a dedicated page.');
		$this->confirmUninstall = $this->l('Are you sure you want to delete this module?');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	
	public function install()
	{
		if (!parent::install())
			return false;
		return true;
	}
	
	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}

}