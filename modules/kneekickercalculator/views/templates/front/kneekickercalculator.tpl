<!--
*  Calculator Template File
*  Predict Marketing
*
-->
<div class="flooringCalculator col-md-10">
	<div class="calculatorHeader">
		<div class="calculatorHeaderContent">
			<h1>Flooring Calculator</h1>
			<p>Need to work out how much carpet to order? Try our handy floor calculator: Just select your room shape, enter the dimensions and select any underlay options to get an estimate.</p>
		</div>
	</div>
</div>
<div class="flooringCalculator col-md-10">
	<div class="calculatorForm">
		<form action="#">
			<div class="form-group roomShape">
				<label class="control-label" for="room_shape"><h3>1. Choose your room shape:</h3></label>
				<label class="radio roomLabel inline" for="rectangle_room_choice">
					<input class="form-control" type="radio" id="rectangle_room_choice" value="rectangle" name="room_shape" />
					<img id="rectangle_image" src="{$tpl_uri}modules/kneekickercalculator/img/shape-rectangle-182.jpg" title="Rectangle Floor" />
					Rectangle
				</label>
				<label class="radio roomLabel inline" for="square_room_choice">
					<input class="form-control" type="radio" id="square_room_choice" value="square" name="room_shape" />
					<img id="square_image" src="{$tpl_uri}modules/kneekickercalculator/img/shape-square-182.jpg" title="Square Floor" />
					Square
				</label>
				<label class="radio roomLabel inline" for="l_shaped_room_choice">
					<input class="form-control" type="radio" id="l_shaped_room_choice" value="l_shaped" name="room_shape" />
					<img id="l_image" src="{$tpl_uri}modules/kneekickercalculator/img/shape-l-shape-r-182.jpg" title="L Shaped Floor" />
					L Shaped
				</label>
				<label class="radio roomLabel inline" for="bay_window_room_choice">
					<input class="form-control" type="radio" id="bay_window_room_choice" value="bay_window" name="room_shape" />
					<img id="bay_image" src="{$tpl_uri}modules/kneekickercalculator/img/shape-bay-window-182.jpg" title="Bay Window" />
					Bay Window
				</label>
				<label class="radio roomLabel inline" for="hall_stairs_landing_room_choice">					
					<input class="form-control" type="radio" id="hall_stairs_landing_room_choice" value="hall_stairs_landing" name="room_shape" />
					<img id="hall_image" src="{$tpl_uri}modules/kneekickercalculator/img/shape-hall-stair-landing-l-182.jpg" title="Hall, Stairs and Landing" />
					Hall, Stairs &amp; Landing
				</label>
				<label class="radio roomLabel inline" for="stairs_landing_room_choice">					
					<input class="form-control" type="radio" id="stairs_landing_room_choice" value="stairs_landing" name="room_shape" />
					<img id="landing_image" src="{$tpl_uri}modules/kneekickercalculator/img/shape-stairs-landing-l-182.jpg" title="Stairs and landing" />
					Stairs &amp; Landing
				</label>
				<hr>
			</div>
			
			<div class="form-group flooringCost">
				<label class="control-label" for="flooring_cost"><h3>2. Enter cost of flooring:</h3></label>				
				<div class="input-group">
				  <span class="input-group-addon">£</span>
				  <input class="form-control" type="number" step="any" min="0" id="flooring_cost" name="flooring_cost" placeholder="0.00" required />
				  <span class="input-group-addon">per m²</span>
				</div>
				<hr>
			</div>
			
			<div class="form-group gripperRods">
				<label class="control-label" for="gripper_rods"><h3>3. Do you require gripper rods?</h3></label>
				<label for="gripper_yes">
					<input class="form-control" type="radio" id="gripper_yes" value="yes" name="gripper_rods" />
					Yes
				</label>
				<label for="gripper_no">
					<input class="form-control" type="radio" id="gripper_no" value="no" name="gripper_rods" />
					No
				</label>
				<hr>
			</div>
				
			<div class="form-group doorBars">
				<label class="control-label" for="door_bars"><h3>5. Do you need door bars?</h3></label>
				<label for="door_bar_yes">
					<input class="form-control" type="radio" id="door_bar_yes" value="yes" name="door_bars" />
					Yes
				</label>
				<label>
					<input class="form-control" type="radio" id="door_bar_no" value="no" name="door_bars" />
					No
				</label>
			<hr>
			</div>

			<div class="form-group roomDoorways">
				<label class="control-label" for="room_doorways"><h3>4. How many doorways does it have?</h3></label>
				<label for="room_doorway_1">
					<input class="form-control" type="radio" id="room_doorway_1" value="1" name="room_doorways" />
					1
				</label>
				<label for="room_doorway_2">
					<input class="form-control" type="radio" id="room_doorway_2" value="2" name="room_doorways" />
					2
				</label>
				<label for="room_doorway_3">
					<input class="form-control" type="radio" id="room_doorway_3" value="3" name="room_doorways" />
					3
				</label>
				<hr>
			</div>
			
			<div class="form-group roomMeasurements">
				<label class="control-label" for="room_measurements"><h3>6. Enter your Room Measurements:</h3></label>						
						<div class="col-md-6">
							<img id="square_room_dimensions" src="{$tpl_uri}modules/kneekickercalculator/img/rect_room_size.png" title="Square Room Dimensions" />
							<img id="l_room_dimensions" src="{$tpl_uri}modules/kneekickercalculator/img/shape-l-dimensions.jpg" title="L Shaped Room Dimensions" />
							<img id="bay_window_dimensions" src="{$tpl_uri}modules/kneekickercalculator/img/shape-bay-window-dimensions-320.jpg" title="Bay Window Dimensions" />
						</div>
						<div class="col-md-6">
								<div class="input-group col-md-6" id="room_measurements_a">
								  <span class="input-group-addon">A</span>
								  <input class="form-control" type="number" step="any" min="0" id="room_measurement_a" name="room_measurements_a" placeholder="0" required />
								  <span class="input-group-addon">m</span>
								</div>
								<div class="input-group col-md-6" id="room_measurements_b">
								  <span class="input-group-addon">B</span>
								  <input class="form-control" type="number" step="any" min="0" id="room_measurement_b" name="room_measurements_b" placeholder="0" required />
								  <span class="input-group-addon">m</span>
								</div>
								<div class="input-group col-md-6" id="room_measurements_c">
								  <span class="input-group-addon">C</span>
								  <input class="form-control" type="number" step="any" min="0" id="room_measurement_c" name="room_measurements_c" placeholder="0" />
								  <span class="input-group-addon">m</span>
								</div>
								<div class="input-group col-md-6" id="room_measurements_d">
								  <span class="input-group-addon">D</span>
								  <input class="form-control" type="number" step="any" min="0" id="room_measurement_d" name="room_measurements_d" placeholder="0" />
								  <span class="input-group-addon">m</span>
								</div>
						</div>
					<hr>	
			</div>
			
			<div class="form-group roomUnderlay">
				<label class="control-label" for="room_underlay"><h3>7. Choose your underlay:</h3></label>
				<select class="form-control col-md-6" name="room_underlay" id="room_underlay" selected="selected">
					<option value="budget_underlay">Budget Underlay</option>
					<option value="good_underlay">Good Underlay</option>
					<option value="better_underlay">Better Underlay</option>
					<option value="best_underlay">Best Underlay</option>
				</select>
			</div>
			<div class="form-group displayMessage">
				<label class="control-label">Tips on Measuring Stairs &amp; Landings</label>				
<p><img style="width:15px;" src="{$tpl_uri}modules/kneekickercalculator/img/logo.png" /> - For stairs ordering a 2 metres x 4 metres piece will cover 99% of stair cases, even those with a twist or curve - as the carpet fitter will cut the piece down for the stairs as it's getting fitted.</p>
<p><img style="width:15px;" src="{$tpl_uri}modules/kneekickercalculator/img/logo.png" /> - If you are ordering a striped carpet for stairs it will be best to order 2.5 metres x 4 metres to give a bit extra as you will lose quite a bit of carpet to make sure the pattern flows down.</p>
<p><img style="width:15px;" src="{$tpl_uri}modules/kneekickercalculator/img/logo.png" /> - For your landing you need to use the above guide for standard rooms but add on enough to cover the first step at the top of the stairs.</p>
<p><img style="width:15px;" src="{$tpl_uri}modules/kneekickercalculator/img/logo.png" /> - Please Contact KneeKicker for Pricing - <span id="phone_number">02380 334555</span>.</p>
			</div>
			<hr>
			<button type="submit" id="calculate_submit" class="btn btn-success">Recalculate</button>
		</form>
		<hr>

	</div>
</div>
<div class="flooringCalculator col-md-10">
	<div class="calculatorForm">
		<div class="calculatorFooter">
		<div class="calculatorFooterContent">
			<h3>Your Final Price</h3>
			<div class="col-md-6">
				<table class="table">
				  <tbody>
				  	<tr><td>Underlay</td><td>£<span class="label" id="underlay_final_cost">0.00</span></td></tr>
				  	<tr><td>Gripper Rods</td><td>£<span class="label" id="gripper_rods_final_cost">0.00</span></td></tr>
				  	<tr><td>Door Bars</td><td>£<span class="label" id="door_bars_final_cost">0.00</span></td></tr>
				  	<tr><td>Flooring</td><td>£<span class="label" id="flooring_final_cost">0.00</span></td></tr>
				  </tbody>
				</table>
			</div>
			<div class="col-md-6">
				<h4>Size: <span class="label" id="total_final_size">0</span>m²</h4>
				<h3>Total Cost:</h3>
				<h2>£<span class="label" id="total_final_price">0.00</span></h2>
			</div>			
		</div>
	</div>
	</div>
</div>