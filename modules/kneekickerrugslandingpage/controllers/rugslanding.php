<?php

/*
 *
 * Custom Landing Page class written for KneeKicker.
 *
 * Predict Marketing
 * Ben Jackson
 */

class kneeKickerRugsLandingPageRugslandingModuleFrontController extends ModuleFrontController {


        /**
         * Lets set up out template
         *
         */
        public function initContent()
        {
                parent::initContent();
                $this->setTemplate('kneekickerrugslandingpage.tpl');
        }

        public function setMedia()
        {
                parent::setMedia();
                $this->addCSS(__PS_BASE_URI__.'modules/'.$this->module->name.'/css/'.$this->module->name.'.css');
        }



}