<div class="landingPage col-md-12">
	<div class="landingPageHeader">
		<div class="landingPageHeaderContent">
			<h1>Carpets for marvellous interior design</h1>
			<p><strong>Do you want to have the most stylish living room to show off to your friends?</strong></p>
		</div>
	</div>
</div>
<div class="landingPage col-md-12">
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickerrugslandingpage/img/a-new-rug-aura-2060D.jpg" title="Aura Rug" />
		</div>
		<div class="col-md-6 landingPageContent">
			<h3>Style for your home</h3>
			<p>Finding the right style for your floor has never been easier. With a range of carpet colours and designs available, you can transform your space into a centre of style and comfort.</p>
<p>Kneekicker can offer you a variety of carpets designs to match the interior of your home or office and make it look it’s best. Bringing new life into your lounge, meeting room or your hallway!</p>
<p>By choosing Kneekicker, you will be assured to work with experienced and knowledgeable professionals who can you find the perfect carpet for your floor. If you’re looking for an interior design that sends out a message of trend and style, put your faith in us to transform your space.</p>

		</div>
	</div>
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageContent">
			<h3>Why you should be choosing carpet</h3>
			<p>There are many reasons to love carpet as it is the ideal flooring for comfort, especially if you pick a thick underlay. Here are some more reasons to indulge:</p>
<ul>
<li>Choices are only limited by your imagination. You have lots of options available - with a broad variety of colours, textures and designs.</li>
<li>Carpet is a more affordable option, compared to other floor covering options like wood.</li>
<li>Carpet is soft and comfortable. Perfect for young families.</li>
<li>The fabric in carpet acts a natural insulator against the cold, reducing the cost of heating in your home and acting as an energy conservation benefit.</li>
<li>Carpet can reduce noise from room to room because the fibre absorbs sound.</li></ul>

		</div>
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickerrugslandingpage/img/a-new-rug-owb-slide.jpg" title="Aura Rug" />
		</div>
	</div>
	<div class="row landingPageBlocks">
		<div class="col-md-6 landingPageImage">
			<img src="{$tpl_uri}modules/kneekickerrugslandingpage/img/a-new-rug-owb-slide5.jpg" title="Aura Rug" />
		</div>		
		<div class="col-md-6 landingPageContent">
			<h3>Size &amp; Installation </h3>
			<p>The installation of carpet can be done by yourself, see our [carpet fitting video](), or you can get a professional to help you. Either way, at KneeKicker, we’ll be happy to help and advise you along the way.Your carpet roll will need to be cut to the size of your room, so measurements will have to be taken. You can also use our floor calculator to help you determine the measurements required and the overall cost.</p>

<p>Carpets are installed with tackless strips which are positioned around a ½ foot away from the wall. The carpet is then rolled out and cut away where the tackless strip lies. A knee kicker closes the gap to hold down the carpet to the floor.</p>

<p>If you’re unsure about carpet installation, our team at KneeKicker have a variety of knowledge and experience with many types of flooring installations. Call us today on [number] and we can help you get to grips with carpet installation.</p>

		</div>
	</div>
</div>
<div class="landingPage col-md-12">
	<div class="landingPageFooter">
		<div class="landingPageFooterContent">
			<h1>What our customers think</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, numquam, dolore. Sit est fuga provident totam debitis veniam placeat in dolore illo, vitae nesciunt. Neque ut incidunt officia, laboriosam aliquam.</p>
		</div>
	</div>
</div>