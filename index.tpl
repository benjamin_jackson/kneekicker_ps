{if isset($HOOK_HOME_TAB_CONTENT) && $HOOK_HOME_TAB_CONTENT|trim}
    {if isset($HOOK_HOME_TAB) && $HOOK_HOME_TAB|trim}
        <ul id="home-page-tabs" class="nav nav-tabs clearfix">
			{$HOOK_HOME_TAB}
		</ul>
	{/if}
	<div class="tab-content">{$HOOK_HOME_TAB_CONTENT}</div>
{/if}
{if isset($HOOK_HOME) && $HOOK_HOME|trim}
	<div class="clearfix">{$HOOK_HOME}</div>
{/if}

<!-- KneeKicker Help & Inspiration  -->
<div class="jumbotron" id="help_inspiration">
	<div class="row">
		<h3>Help &amp; Inspiration</h3>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="thumbnail">
		      <img class="help_inspiration_img" src="{$tpl_uri}img/calculator.svg" title="Calculator" />
		      <div class="caption">
		        <h3>Floor Calculator</h3>
		        <p>Use our tool to work out the cost of your flooring.</p>
		        <p><a href="/floor_calculator">Try it Out</a></p>
		      </div>
		    </div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="thumbnail">
		      <img class="help_inspiration_img" src="{$tpl_uri}img/lightbulb.svg" title="Lightbulb" />
		      <div class="caption">
		        <h3>Be inspired</h3>
		        <p>By the latest flooring trends in our huge image gallery.</p>
		        <p><a href="/content/6-gallery">View Gallery</a></p>
		      </div>
		    </div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="thumbnail">
		      <img class="help_inspiration_img" src="{$tpl_uri}img/samples.svg" title="Samples" />
		      <div class="caption">
		        <h3>Carpet News</h3>
		        <p>Catch up on the latest carpet news</p>
		        <p><a href="/blog">Read Now</a></p>
		      </div>
		    </div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="thumbnail">
		      <img class="help_inspiration_img" src="{$tpl_uri}img/guides.svg" title="Guides" />
		      <div class="caption">
		        <h3>Buying Guides</h3>
		        <p>Any trouble purchasing your new flooring?</p>
		        <p><a href="/blog/buying-guides">Learn More</a></p>
		      </div>
		    </div>
		</div>
	</div>
</div>